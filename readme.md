# STORE CLONE
git clone git@bitbucket.org:tezarius/demo-ocstore-3.git


## next step:
```
cd ./storage

find ./ -type d -exec chmod 755 "{}" \;
find ./ -type f -exec chmod 644 "{}" \;

--- OR ---

find ./ -type d -exec chmod 777 "{}" \;
find ./ -type f -exec chmod 666 "{}" \;
```

## next step:
```
cd ../public/image/

find ./ -type d -exec chmod 755 "{}" \;
find ./ -type f -exec chmod 644 "{}" \;

--- OR ---

find ./ -type d -exec chmod 777 "{}" \;
find ./ -type f -exec chmod 666 "{}" \;
```

## next step:
```
cd ../
cp ./config-dist.php ./config.php
nano ./config.php
```
> replace `http://site.name/` on you `site name`  
> replace `/path/to/site/public` on you `folder path`  
> `setting` connect to `db`  


## next step:
```
cd ./admin
cp ./config-dist.php ./config.php
nano ./config.php
```
> replace `http://site.name/` on you `site name`  
> replace `/path/to/site/public` on you `folder path`  
> `setting` connect to `db`  

## next step:
```
cd ../install
mysql -u username -p database_name < tezarius_store.sql
```

## next step:
```
cd ../
npm i
CUR_PATH=`pwd` && \
ln -s $CUR_PATH/node_modules $CUR_PATH/public/catalog/view/libraries
```

# Custom Themes
```
http://forum.opencart-russia.ru/threads/rukovodstvo-dizajnera.16/
https://webformyself.com/sozdanie-polzovatelskoj-temy-v-opencart-chast-2/
```