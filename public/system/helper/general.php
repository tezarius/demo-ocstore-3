<?php
function token($length = 32) {
	// Create random token
	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	
	$max = strlen($string) - 1;
	
	$token = '';
	
	for ($i = 0; $i < $length; $i++) {
		$token .= $string[mt_rand(0, $max)];
	}	
	
	return $token;
}

/**
 * Backwards support for timing safe hash string comparisons
 * 
 * http://php.net/manual/en/function.hash-equals.php
 */

if(!function_exists('hash_equals')) {
	function hash_equals($known_string, $user_string) {
		$known_string = (string)$known_string;
		$user_string = (string)$user_string;

		if(strlen($known_string) != strlen($user_string)) {
			return false;
		} else {
			$res = $known_string ^ $user_string;
			$ret = 0;

			for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);

			return !$ret;
		}
	}
}


/** Получение данных из массива */
function get($array, $key, $default = '', $checkEmpty = FALSE)
{
    if( gettype($array)=='object' ) return isset($array->{$key}) ? $array->{$key} : $default;
    if( $array instanceof \ArrayObject ) return $array->offsetExists($key) ? $array->offsetGet($key) : $default;
    else return ( isset($array[$key]) && ( !$checkEmpty || ($checkEmpty&&!empty($array[$key])) ) ) ? $array[$key] : $default;
}
function property($obj, $name, $default = ''){
    if( gettype($obj)=='object' ) return ( isset($obj->{$name}) && !empty($obj->{$name}) ) ? $obj->{$name} : $default;
    else if( gettype($obj)=='array' ) return get($obj, $name, $default);
    return $default;
}
function cut(&$obj, $name, $default = ''){
    $r = $default;
    if( gettype($obj)=='object' ){
        if( isset($obj->{$name}) ){ $r = $obj->{$name}; unset($obj->{$name}); }else{ $r = $default; }
    }
    else if( gettype($obj)=='array' ){
        if( isset($obj[$name]) ){ $r = $obj[$name]; unset($obj[$name]); }else{ $r = $default; }
    }
    return $r;
}
/** Get IP Address Client */
function ip($forwarded_only=FALSE){
    if ( !$forwarded_only && getenv ('REMOTE_ADDR')){$user_ip = getenv ('REMOTE_ADDR');}
    elseif ( getenv ('HTTP_FORWARDED_FOR'))   {$user_ip = getenv ('HTTP_FORWARDED_FOR');}
    elseif ( getenv ('HTTP_X_FORWARDED_FOR')) {$user_ip = getenv ('HTTP_X_FORWARDED_FOR');}
    elseif ( getenv ('HTTP_X_COMING_FROM'))   {$user_ip = getenv ('HTTP_X_COMING_FROM');}
    elseif ( getenv ('HTTP_VIA'))             {$user_ip = getenv ('HTTP_VIA');}
    elseif ( getenv ('HTTP_XROXY_CONNECTION')){$user_ip = getenv ('HTTP_XROXY_CONNECTION');}
    elseif ( getenv ('HTTP_CLIENT_IP'))       {$user_ip = getenv ('HTTP_CLIENT_IP');}
    else {$user_ip='unknown';}
    if (15 < strlen ($user_ip)){
        $ar = split (', ', $user_ip);
        for ($i= sizeof ($ar)-1; $i> 0; $i--){
            if ($ar[$i]!='' and !preg_match ('/[a-zA-Zа-яА-Я]/', $ar[$i])){
                $user_ip = $ar[$i];
                break;
            }
            if ($i== sizeof ($ar)-1){$user_ip = 'unknown';}
        }
    }
    if ( preg_match ('/[a-zA-Zа-яА-я]/', $user_ip)){$user_ip = 'unknown';}
    return $user_ip;
}
/** This is Bot? */
function isBot(){
    $bots = array(
        'bot', 'stackrambler', 'aportworm', 'isearch', 'yandex', 'yandexblog', 'mail.ru', 'adsbot-google', 'ia_archiver', 'check_http',
        'scooter/', 'ask jeeves', 'baiduspider+(', 'exabot/', 'fast enterprise crawler', 'fast-webcrawler/', 'http://www.neomo.de/', 'gigabot/',
        'mediapartners-google', 'google desktop', 'feedfetcher-google', 'googlebot', 'heise-it-markt-crawler', 'heritrix/1.', 'ibm.com/cs/crawler',
        'iccrawler - icjobs', 'ichiro/2', 'mj12bot/', 'metagerbot/', 'msnbot-newsblogs/', 'msnbot/', 'msnbot-media/', 'ng-search/',
        'http://lucene.apache.org/nutch/', 'nutchcvs/', 'omniexplorer_bot/', 'online link validator', 'psbot/0', 'seekbot/', 'sensis web crawler',
        'seo search crawler/', 'seoma [seo crawler]', 'seosearch/', 'snappy/1.1 ( http://www.urltrends.com/ )', 'http://www.tkl.iis.u-tokyo.ac.jp/~crawler/',
        'synoobot/', 'crawleradmin.t-info@telekom.de', 'turnitinbot/', 'voyager/1.0', 'w3 sitesearch crawler', 'w3c-checklink/', 'w3c_*validator',
        'http://www.wisenutbot.com', 'yacybot', 'yahoo-mmcrawler/', 'yahoo! de slurp', 'yahoo! slurp', 'yahooseeker/'
    );
    $user_agent = strtolower( get($_SERVER,'HTTP_USER_AGENT') );
    //$this->e($user_agent);
    if( $user_agent == "" ){ return true; }
    foreach( $bots as $bot ){
        if( stripos($user_agent, $bot) !== false ){ return true; }
    }
    return false;
}
/** Получение переменной из POST или GET */
function rcv($name,$df=""){
    $dt = @$_POST[$name];
    if( !$dt ){
        $POST = file_get_contents("php://input");
        $POST = json_decode($POST);
        $dt = @$POST->{$name};
    }
    if( !$dt ) $dt = @$_GET[$name];

    if( gettype($dt)=='string' ){
        $dt = htmlEntityEncode($dt,ENT_QUOTES,"UTF-8");
        $dt = trim($dt);
    }
    else if( gettype($dt)=='array' ){
        array_walk_recursive($dt,function(&$value){
            $value = htmlentities($value,ENT_QUOTES,"UTF-8");
            $value = trim($value);
        });
    }
    return ( $dt ) ? $dt : $df;
}
function htmlEntityDecode($str='')
{
    return html_entity_decode($str);
}
function htmlEntityEncode($str='',$style=ENT_QUOTES,$charset='UTF-8')
{
    return htmlentities($str,$style,$charset);
}
/** Check on Ajax */
function isAjax(){
    if( !isset( $_SERVER[ "HTTP_X_REQUESTED_WITH" ] ) || $_SERVER[ "HTTP_X_REQUESTED_WITH" ] != "XMLHttpRequest" ) return FALSE;
    return TRUE;
}
/**
 * метод проверяет, была ли отправлена форма по наличию имени кнопки в массиве $_POST
 * кроме того происходит проверка referrer
 * Возвращает TRUE при выполнении условий и значение, если оно есть, в объект
 */
function buttonPressed($buttonName){
    $matches = NULL;
    if( !isset($_POST[$buttonName]) ) return FALSE;
    if( !isset($_SERVER['HTTP_REFERER']) ) return TRUE; // проверка referrer
    if( !preg_match('/^https?\:\/\/([^\/]+)\/.*$/i',$_SERVER['HTTP_REFERER'],$matches) ) return FALSE;
    if( $matches[1] != $_SERVER['HTTP_HOST'] ) return FALSE;
    ///$this->bttn = $_POST[$buttonName];
    return TRUE;
}
/** Echo Json String */
function jEcho($msg,$exit=TRUE){
    if( !(is_string($msg) && is_object(json_decode($msg)) && (json_last_error() == JSON_ERROR_NONE) ? true : false) ) $msg = json_encode($msg);
    echo $msg;
    if( $exit ) exit;
}
/** Request, based on cURL*/
function request($url,$params=array()){
    $body = get($params,'body');
    $auth = get($params,'auth');
    $post = get($params,'post');
    $header = get($params,'header');
    $headers = get($params,'headers');
    $uAgent  = get($params,'uAgent',"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1");
    $uaForce = get($params,'uaForce');
    $timeouts = get($params,'timeouts','15:25');
    /// Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1
    /// Mozilla/5.0 (Windows; U; Windows NT 6.1; ru; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5
    /// Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36
    $userAgent = ($ua=@$_SERVER['HTTP_USER_AGENT']) ?$ua :$uAgent;
    if( $uaForce ) $userAgent = $uAgent;
    /// TimeOuts
    $arrTimeouts = explode(':',$timeouts);
    $toConn = get($arrTimeouts,0,15);
    $toExec = get($arrTimeouts,1,25);
    /////////////////////////////////////////////////////////
    /// http://php.net/manual/ru/function.curl-setopt.php ///
    $ch = curl_init($url);                                /// Замена $ch = curl_init(); curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, $header);                   /// Возвращает заголовки?
    ///curl_setopt($ch, CURLOPT_NOBODY, 1);                             /// Читать ТОЛЬКО заголовок без тела
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);           /// Возвращает веб-страницу
    curl_setopt($ch, CURLOPT_USERAGENT,$userAgent);             /// ЮзерАгент
    curl_setopt($ch, CURLOPT_REFERER, "api://yulsun.ru"); /// Рефер
    ///curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);                      /// Переходит по редиректам
    ///curl_setopt($ch, CURLOPT_MAXREDIRS, 10);                         /// останавливаться после 10-ого редиректа
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,$toConn);            /// Таймаут на соединение
    curl_setopt($ch, CURLOPT_TIMEOUT,$toExec);                   /// Таймайт на выполнение
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);        /// DEF 120sec
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);       /// Отключаем проверку сертификата
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);           /// Отключаем проверку сертификата
    /////////////////////////////////////////////////////////
    if( $body ){
        ///$body = array('name' => 'Foo', 'file' => '@/home/user/test.png');
        $body = (is_array($body)) ? http_build_query($body) : $body;
        curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
    }///else curl_setopt($ch, CURLOPT_POST,1);
    if( $auth ){
        /// $auth="login:password";
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
    }
    if( $headers ) curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
    if( $post ) curl_setopt($ch, CURLOPT_POST,1);///Помоему лишняя опция [2016.12.29]
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
/** Encryption */
function arrEncryption($array,$method=0){
    $method = (int) $method;
    switch($method){
        case 1:
            $jArr  = json_encode($array);
            $rjArr = strrev($jArr);
            $sArr  = serialize($rjArr);
            $rsArr = strrev($sArr);
            $bArr  = base64_encode($rsArr);
            $rbArr = strrev($bArr);
            $enArr = str_replace("=","$",$rbArr);
            break;
        default:
            $jArr  = json_encode($array);
            $rjArr = strrev($jArr);
            $sArr  = serialize($rjArr);
            $rsArr = strrev($sArr);
            $bArr  = base64_encode($rsArr);
            $rbArr = strrev($bArr);
            $enArr = str_replace("=","@",$rbArr);
            $enArr = substr($enArr,0,7).'0'.substr($enArr,7,strlen($enArr));
            $enArr = substr($enArr,0,24).'1'.substr($enArr,24,strlen($enArr));
    }
    return $enArr;
}
/** Decryption */
function arrDecryption($string,$method=0){
    $method = (int) $method;
    switch($method){
        case 1:
            $rbArr = str_replace("$","=",$string);
            $bArr  = strrev($rbArr);
            $rsArr = base64_decode($bArr);
            $sArr  = strrev($rsArr);
            $rjArr = unserialize($sArr);
            $jArr  = strrev($rjArr);
            $deArr = json_decode($jArr);
            break;
        default:
            $string = cutSymbol($string,[7,24])->string;
            $rbArr = str_replace("@","=",$string);
            $bArr  = strrev($rbArr);
            $rsArr = base64_decode($bArr);
            $sArr  = strrev($rsArr);
            $rjArr = unserialize($sArr);
            $jArr  = strrev($rjArr);
            $deArr = json_decode($jArr);
    }
    return $deArr;
}
/**
 * Convert WINDOWS1251 from UTF-8
 *
 * @param mixed $text
 * @return mixed
 */
function getTextCP1251($text){
    if( is_array($text) || is_object($text) ){
        foreach( $text as &$value ){ $value = getTextCP1251($value); }
        return $text;
    }
    if( mb_check_encoding($text, "UTF-8") ){
        $text = mb_convert_encoding( $text, "WINDOWS-1251", "UTF-8" );
    }
    return $text;
}
/**
 * Convert WINDOWS-1251 to UTF-8
 *
 * @param mixed $text
 * @return mixed
 */
function getTextUTF8($text){
    if( is_array($text) || is_object($text) ){
        foreach( $text AS &$value ){ $value = getTextUTF8($value); }
        return $text;
    }
    if( mb_check_encoding( $text, "WINDOWS-1251" ) ){
        $text = mb_convert_encoding( $text, "UTF-8", "WINDOWS-1251" );
    }
    return $text;
}
/** HTML Special Chars */
function entities($text){
    $text = htmlentities($text,ENT_QUOTES,'UTF-8',true);
    return $text;
}
/** Russian Weekday */
function russianWeekday($mn)
{
    switch ($mn) {
        case 1: $m = "Понедельник"; break;
        case 2: $m = "Вторник"; break;
        case 3: $m = "Среда"; break;
        case 4: $m = "Четверг"; break;
        case 5: $m = "Пятница"; break;
        case 6: $m = "Суббота"; break;
        case 7: $m = "Воскресенье"; break;
        default: $m = "Восьмидельник";
    }
    return $m;
}
/**
 * Cut Symbols from String on the Array with Positions
 *
 *      $oHash = Core::cutSymbol($hash,[5,13]);
 *
 * @param   string  $str    input string
 * @param   array   $pos    array with symbol position
 * @return  object
 */
function cutSymbol($str=NULL,$pos=NULL)
{
    if( $str===NULL || $pos===NULL ) return json_decode(json_encode(["symbol"=>'',"symbols"=>'',"string"=>'']));
    $symbols = array(); $symbol = FALSE;
    if( is_array($pos) )
    {
        $i = -1; foreach( $pos AS $p ){ ++$i;
        $p = $p-$i;
        if( $p>strlen($str)+1 ) return json_decode(json_encode(["symbol"=>'',"symbols"=>'',"string"=>'']));
        $symbol .= $str{$p};
        $symbols[] = $str{$p};
        $str = substr_replace($str,'',$p,1);
    }
        $string = $str;
    }
    else{
        if( $pos>strlen($str)+1 ) return json_decode(json_encode(["symbol"=>'',"symbols"=>'',"string"=>'']));
        $symbol .= $str{$pos};
        $symbols[] = $symbol;
        $string = substr_replace($str,'',$pos,1);
    }
    return json_decode( json_encode( array("symbol"=>$symbol,"symbols"=>$symbols,"string"=>$string) ) );
}
/** Debug Function */
function pre()
{
    $stack = [];
    foreach( func_get_args() as $val ){
        array_push( $stack, $val );
    }
    print'<pre>';print_r($stack);print'</pre>';exit;
}
function pretty($arr, $level=0){
    $tabs = "";
    for($i=0;$i<$level; $i++){
        $tabs .= "    ";
    }
    foreach($arr as $key=>$val){
        if( is_array($val) ) {
            print ($tabs . $key . " : " . "\n");
            pretty($val, $level + 1);
        } else {
            if($val && $val !== 0){
                print ($tabs . $key . " : " . $val . "\n");
            }
        }
    }
}
function debug($object, $title=null, $plain=false, $limit=6, $level=0)
{
    if (defined('DEBUG') && ((isset($_SERVER['REMOTE_ADDR']) && !(debug==$_SERVER['REMOTE_ADDR'] || strpos(debug,$_SERVER['REMOTE_ADDR'].',')===0 || strpos(debug,','.$_SERVER['REMOTE_ADDR'].',')!==false || strpos(debug,','.$_SERVER['REMOTE_ADDR'])===strlen(debug)-strlen($_SERVER['REMOTE_ADDR'])-1)) || (!isset($_SERVER['REMOTE_ADDR']) && debug===false))
    )
    {
        return;
    }
    if ($plain===false && php_sapi_name()==='cli')
    {
        $plain = true;
    }
    if (is_string($plain))
    {
        $color_black = "";
        $color_black_light = "";
        $color_red = "";
        $color_red_light = "";
        $color_green = "";
        $color_green_light = "";
        $color_yellow = "";
        $color_yellow_light = "";
        $color_blue = "";
        $color_blue_light = "";
        $color_purple = "";
        $color_purple_light = "";
        $color_cyan = "";
        $color_cyan_light = "";
        $color_gray = "";
        $color_gray_light = "";
        $color_clear = "";
    }
    else if ($plain===true)
    {
        $color_black = "\33[0;30m";
        $color_black_light = "\33[1;30m";
        $color_red = "\33[0;31m";
        $color_red_light = "\33[1;31m";
        $color_green = "\33[0;32m";
        $color_green_light = "\33[1;32m";
        $color_yellow = "\33[1;33m";
        $color_yellow_light = "\33[0;33m";
        $color_blue = "\33[0;34m";
        $color_blue_light = "\33[1;34m";
        $color_purple = "\33[0;35m";
        $color_purple_light = "\33[1;35m";
        $color_cyan = "\33[0;36m";
        $color_cyan_light = "\33[1;36m";
        $color_gray = "\33[0;37m";
        $color_gray_light = "\33[1;37m";
        $color_clear = "\033[0;39m";
    }
    $result = '';
    static $charset;
    if ($charset===null && !$plain)
    {
        echo "<meta charset=\"utf-8\">\n";
        $charset = true;
    }
    if ($level>$limit)
    {
        if ($plain)
        {
            return str_repeat(' ', 4*$level)."...\n";
        }
        else
        {
            return "...";
        }
    }
    if (is_object($object) || (is_array($object) && count($object)>0))
    {
        foreach ($object as $key => $value)
        {
            if ($plain)
            {
                $result .= str_repeat(' ', 4*$level).$key;
            }
            else
            {
                if (is_array($object))
                {
                    $result .= "<span style='background:silver;padding-left:2px;padding-right:2px;'>".$key.'</span>';
                }
                else
                {
                    $result .= '<span>'.$key.'</span>';
                }
            }
            if (is_object($value) || (is_array($value) && count($value)>0))
            {
                if (is_object($value))
                {
                    if ($plain)
                    {
                        $result .= " (".get_class($value).")";
                    }
                    else
                    {
                        $result .= "<span style='color:grey'> ".get_class($value)."</span>";
                    }
                }
                else
                {
                    if ($plain)
                    {
                        $result .= " (array)";
                    }
                    else
                    {
                        $result .= "<span style='color:grey'> array</span>";
                    }
                }
                if ($plain)
                {
                    $result .= "\n";
                }
                else
                {
                    $collapse = !($level+1<=1 || $title===true || (is_string($title) && substr($title,0,1)=='*'));
                    $result .= "\n<div style='".($collapse?'height:12px':'').";margin-top:2px;margin-bottom:2px;border:1px solid grey;padding:3px;margin-left:15px;background-color:rgb(".(255-($level*10)).",".(255-($level*10)).",".(255-($level*10)).");overflow:hidden;'>\n<div id='tree' style='background-color:silver;font-size:9px;cursor:pointer;float:right;border:1px solid black;width:10px;height:10px;text-align:center;padding:0px;overflow:hidden;' onclick=\"if(this.parentNode.style.height=='12px'){this.parentNode.style.height='';this.innerHTML='-'}else{this.parentNode.style.height='12px';this.innerHTML='+'}\">".($collapse?'+':'-')."</div>";
                }
                $result .= debug ($value, $title, $plain, $limit, $level+1);
                if ($plain)
                {
                    $result .= "";
                }
                else
                {
                    $result .= "</div>\n";
                }
            }
            else
            {
                $result .= " : ";
                if ($value===null)
                {
                    if ($plain)
                    {
                        $result .= $color_cyan."null".$color_clear;
                    }
                    else
                    {
                        $result .= "<span style='color:blue'>null</span>";
                    }
                }
                else if (is_bool($value))
                {
                    if ($value)
                    {
                        if ($plain)
                        {
                            $result .= $color_purple_light."true".$color_clear;
                        }
                        else
                        {
                            $result .= "<span style='color:green'>true</span>";
                        }
                    }
                    else
                    {
                        if ($plain)
                        {
                            $result .= $color_purple_light."false".$color_clear;
                        }
                        else
                        {
                            $result .= "<span style='color:red'>false</span>";
                        }
                    }
                }
                else if (is_integer($value) || is_float($value))
                {
                    if ($plain)
                    {
                        $result .= $color_red_light.$value.$color_clear;
                    }
                    else
                    {
                        $result .= "<span style='color:maroon'>".$value."</span>";
                    }
                }
                else if (is_array($value))
                {
                    if ($plain)
                    {
                        $result .= "[]";
                    }
                    else
                    {
                        $result .= "<span style='color:red'>[]</span>";
                    }
                }
                else
                {
                    if ($plain)
                    {
                        $result .= $color_green_light."\"".$value."\"".$color_clear;
                    }
                    else
                    {
                        $result .= "\"".htmlspecialchars ($value,ENT_NOQUOTES,'UTF-8')."\"";
                    }
                }
                if ($plain)
                {
                    $result .= "\n";
                }
                else
                {
                    $result .= "<br>\n";
                }
            }
        }
        if ($level>0)
        {
            return $result;
        }
    }
    else
    {
        if ($object===null)
        {
            if ($plain)
            {
                $result = $color_cyan."null".$color_clear;
            }
            else
            {
                $result = "<span style='color:blue'>null</span>";
            }
        }
        else if (is_bool($object))
        {
            if ($object)
            {
                if ($plain)
                {
                    $result = $color_purple_light."true".$color_clear;
                }
                else
                {
                    $result = "<span style='color:green'>true</span>";
                }
            }
            else
            {
                if ($plain)
                {
                    $result = $color_purple_light."false".$color_clear;
                }
                else
                {
                    $result = "<span style='color:red'>false</span>";
                }
            }
        }
        else if (is_integer($object) || is_float($object))
        {
            if ($plain)
            {
                $result = $color_red_light.$object.$color_clear;
            }
            else
            {
                $result = "<span style='color:maroon'>".$object."</span>";
            }
        }
        else if (is_array($object))
        {
            if ($plain)
            {
                $result = "[]";
            }
            else
            {
                $result = "<span style='color:red'>[]</span>";
            }
        }
        else
        {
            if ($plain)
            {
                $result = $color_green_light."\"".$object."\"".$color_clear;
            }
            else
            {
                $result = "\"".htmlspecialchars ($object,ENT_NOQUOTES,'UTF-8')."\"";
            }
        }
        if ($plain)
        {
            $result .= "\n";
        }
        else
        {
            $result .= "<br>\n";
        }
    }
    if (is_null($object))
    {
        $type = 'null';
    }
    else if (is_bool($object))
    {
        $type = 'boolean';
    }
    else if (is_object($object))
    {
        $type = get_class($object);
    }
    else if (is_array($object))
    {
        $type = 'array';
    }
    else if (is_int($object))
    {
        $type = 'integer';
    }
    else if (is_float($object))
    {
        $type = 'float';
    }
    else
    {
        $type = 'string';
    }
    if ($plain)
    {
        $header = '';
        if ($title)
        {
            $header = $color_cyan;
            $header .= "--------------------------------------\n";
            $header .= (is_bool($title) || $title===null)?$type:$title;
            $header .= "\n--------------------------------------\n";
            $header .= $color_clear;
        }
        if (is_string($plain))
        {
            if ($plain=='error_log')
            {
                error_log ("\n".$header.$result);
            }
            else
            {
                file_put_contents ($plain, $header.$result, FILE_APPEND);
            }
        }
        else
        {
            echo $header.$result;
        }
    }
    else
    {
        $trace = debug_backtrace();
        $node = reset ($trace);
        $file = basename($node['file']).":".$node['line'];
        $debug = "<div>";
        foreach ($trace as $key => $value)
        {
            if (isset($value['file']) && $value['line'])
            {
                $debug .= "<a href='subl://".str_replace('\\','/',$value['file']).":".$value['line']."' style='color:black;text-decoration:none;'>".$value['file']."</a> [".$value['line']."] <font color=maroon>".$value['function']."</font><br>";
            }
            else
            {
                $debug .= "<font color=maroon>".$value['function']."</font><br>";
            }
        }
        $debug .= "</div>";
        echo "<div style='box-shadow: 5px 5px 5px #888888;background:#f1f1f1;z-index:9999;position:relative;font-family:dejavu sans mono;font-size:12px;line-height:normal!important;/*width:550px;*/border:1px solid grey;padding:3px;margin:10px;'>\n";
        echo "<div style='background-color:grey;padding:1px;margin-bottom:5px;border:1px solid black;height:14px;overflow:hidden;'>".(($title && !is_bool($title))?$title:$type)."<div id='tree' style='background-color:silver;font-size:9px;cursor:pointer;float:right;border:1px solid black;width:10px;height:10px;text-align:center;padding:0px;overflow:hidden;margin-left:5px' onclick=\"if(this.parentNode.style.height=='14px'){this.parentNode.style.height='';this.innerHTML='-'}else{this.parentNode.style.height='14px';this.innerHTML='+'}\">+</div><a href='subl://".str_replace('\\','/',$node['file']).":".$node['line']."' style='color:silver;text-decoration:none;float:right'>".$file."</a>".$debug."</div>\n";
        echo $result;
        echo "</div>\n";
    }
}
function dd($object, $title=null, $plain=false, $limit=6, $level=0)
{
    debug($object,$title,$plain,$limit,$level);
    exit;
}
function pp($object, $title=null, $plain=false, $limit=6, $level=0)
{
    debug($object,$title,$plain,$limit,$level);
}
/**
 * https://stackoverflow.com/questions/4196157/create-array-tree-from-array-list
 *
 * Example:
 * $flat = [
 *      ['id'=>100, 'parentID'=>0, 'name'=>'a'],
 *      ['id'=>101, 'parentID'=>100, 'name'=>'a'],
 *      ['id'=>102, 'parentID'=>101, 'name'=>'a'],
 *      ['id'=>103, 'parentID'=>101, 'name'=>'a'],
 * ];
 *
 * $tree = buildTree($flat, 'parentID', 'id');
 * print_r($tree);
 */
function buildTree($flat, $pidKey, $idKey = null)
{
    $grouped = array();
    foreach ($flat as $sub){
        $grouped[$sub[$pidKey]][] = $sub;
    }

    $fnBuilder = function($siblings) use (&$fnBuilder, $grouped, $idKey) {
        foreach ($siblings as $k => $sibling) {
            $id = $sibling[$idKey];
            if(isset($grouped[$id])) {
                $sibling['children'] = $fnBuilder($grouped[$id]);
            }
            $siblings[$k] = $sibling;
        }

        return $siblings;
    };

    $tree = $fnBuilder($grouped[0]);

    return $tree;
}