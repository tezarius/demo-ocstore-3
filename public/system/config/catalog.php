<?php
// Site
$_['site_url']           = HTTP_SERVER;
$_['site_ssl']           = HTTPS_SERVER;

// Url
$_['url_autostart']      = false;

// Database
$_['db_autostart']       = true;
$_['db_engine']          = DB_DRIVER; // mpdo, mssql, mysql, mysqli or postgre
$_['db_hostname']        = DB_HOSTNAME;
$_['db_username']        = DB_USERNAME;
$_['db_password']        = DB_PASSWORD;
$_['db_database']        = DB_DATABASE;
$_['db_port']            = DB_PORT;
// Database Tezarius
$_['dbt_autostart']       = true;
$_['dbt_engine']          = DBT_DRIVER; // mpdo, mssql, mysql, mysqli or postgre
$_['dbt_hostname']        = DBT_HOSTNAME;
$_['dbt_username']        = DBT_USERNAME;
$_['dbt_password']        = DBT_PASSWORD;
$_['dbt_database']        = DBT_DATABASE;
$_['dbt_port']            = DBT_PORT;

// Session
$_['session_autostart']  = true;
$_['session_engine']     = 'db';
$_['session_name']       = 'OCSESSID';

// Template
$_['template_engine']    = 'twig';
$_['template_directory'] = '';
$_['template_cache']     = true;

// Autoload Configs
$_['config_autoload'] = array();

// Autoload Libraries
$_['library_autoload'] = array(
	'openbay'
);

// Autoload Libraries
$_['model_autoload'] = array(
    'setting/tezarius'
);

// Actions
$_['action_pre_action']  = array(
	'startup/session',
	'startup/startup',
	'startup/error',
	'startup/event',
	'startup/maintenance',
	'startup/seo_url',
	'startup/tezarius',
);

// Action Events
$_['action_event'] = array(
	'controller/*/before' => array(
		'event/language/before'
	),
	'controller/*/after' => array(
		'event/language/after'
	),	
	'view/*/before' => array(
		500  => 'event/theme/override',
		998  => 'event/language',
		1000 => 'event/theme'
	),
	'language/*/after' => array(
		'event/translation'
	),
	//'view/*/before' => array(
	//	1000  => 'event/debug/before'
	//),
	'controller/*/after'  => array(
		'event/debug/after'
	)
);