<?php
namespace Cart;
class Tezarius {
	private $data = [];
	public function __construct($registry) {
        $this->data = new \stdClass();
        $this->data->config   = $registry->get('config');
        $this->data->customer = $registry->get('customer');
        $this->data->request  = $registry->get('request');
        $this->data->session  = $registry->get('session');
        $this->data->db       = $registry->get('db');
        $this->data->dbt      = $registry->get('dbt');
	}
	public function setAccessStocks($offices) {
        $this->data->offices = $offices;
    }
	public function getAccessStocks() {
        return get($this->data,'offices',[]);
    }
	public function setCurrentStock($office) {
        $this->data->office = $office;
    }
	public function getCurrentStock() {
        return get($this->data,'office',[]);
    }
    public function getStockPhones() {
        $_phones = [];
        $_office = $this->getCurrentStock();
        if( $_phone = get($_office,'phone1') ) array_push($_phones,$_phone);
        if( $_phone = get($_office,'phone2') ) array_push($_phones,$_phone);
        return $_phones;
    }
}