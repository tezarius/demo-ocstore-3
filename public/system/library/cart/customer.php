<?php
namespace Cart;
class Customer
{
    public $isTezarius = TRUE;

    private $data = []; /// Общий массив
    private $customer_id;
    private $discountCardID;

    private $price_lid; /// Уровень цены клиента
    private $org_name;
    private $org_inn;
    private $org_addr;

    private $manager_id;
    private $source_id;
    private $stock_id;
    private $stock_addr;
    private $stock_name;
    private $firm_id;
    private $stock_phone_1;
    private $stock_phone_2;

    private $firstname;
    private $lastname;
    private $patronymic;
    private $group_id; /// customer_group_id => group_id
    private $email;
    private $telephone;
    private $newsletter;
    private $address_id;
    private $country_id;

    public function __construct($registry)
    {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->dbt = $registry->get('dbt');
        $this->request = $registry->get('request');
        $this->session = $registry->get('session');
        $this->tezarius = $registry->get('tezarius');

        $this->data = get($this->session->data,'customer',[]);

        if( $this->dbt && $this->data ) /// В админке карточка пользователя не нужна, нахер он ее туда подрубает...
        {
            ///get($this->session->data,'CounterpartsID');
            $hashAuth = get($this->data,'hashAuth'); /// - Всегда есть (ДОЛЖЕН БЫТЬ) когда есть объект CUSTOMER в SESSION как и CounterpartsID
            $hashAuth = arrDecryption($hashAuth);
            ///dd($hashAuth);
            if( $hashAuth && is_object($hashAuth) )
            {
                $login = get($hashAuth,'login');
                $passwd = get($hashAuth,'passwd');
                $query = $this->dbt->query("CALL pRB_get('rbCustomers',JSON_OBJECT('filter','Authorization','login','$login','pass','$passwd'),'ru',1,0,1,'')");
                $this->dbt->clear();
                //
                if( $query->num_rows )
                {
                    $this->data = $query->row;
                    $this->data['hashAuth'] = arrEncryption([
                        'login' => $login,
                        'passwd' => $passwd,
                    ]);
                    $this->session->data['customer'] = $this->data;

                    $this->customer_id = (int) get($query->row,'id_rbCounterparts');
                    $this->discountCardID = (int) get($query->row,'DiscountCardID');

                    $this->price_lid = (int) get($query->row,'id_rbPriceLevels');
                    $this->org_name = get($query->row,'name_full');
                    $this->org_inn  = get($query->row,'inn');
                    $this->org_addr = get($query->row,'address');

                    $this->manager_id = (int) get($query->row,'id_rbUsers_manager');
                    $this->source_id  = (int) get($query->row,'id_rbCustomersSource');
                    $this->stock_id   = (int) get($query->row,'id_rbStock');
                    $this->stock_addr = get($query->row,'StockAddress');
                    $this->stock_name = get($query->row,'rbStock_id_rbStock');
                    $this->session->data['selectedStockID'] = $this->stock_id;
                    $this->firm_id    = (int) get($query->row,'id_rbFirms');
                    $this->stock_phone_1 = get($query->row,'StockPhone1');
                    $this->stock_phone_2 = get($query->row,'StockPhone2');

                    $this->firstname   = get($query->row,'name_i');
                    $this->lastname    = get($query->row,'name_f');
                    $this->patronymic  = get($query->row,'name_o');
                    $this->group_id    = get($query->row,'group_id');
                    $this->email       = get($query->row,'email');
                    $this->telephone   = get($query->row,'phone_formated');
                    $this->newsletter  = get($query->row,'newsletter');
                    $this->address_id  = get($query->row,'rbStock_id_rbStock');
                    $this->country_id  = 176;

                    $ip = ip();
                    $this->db->query("UPDATE ".DB_PREFIX."customer SET language_id = '".(int)$this->config->get('config_language_id')."', ip = '".$this->dbt->escape($ip)."' WHERE customer_id = '".(int)$this->customer_id."'");
                    $query = $this->db->query("SELECT * FROM ".DB_PREFIX."customer_ip WHERE customer_id = '".$this->customer_id."' AND ip = '".$this->dbt->escape($ip)."'");
                    if( !$query->num_rows )
                    {
                        $this->db->query("INSERT INTO ".DB_PREFIX."customer_ip SET customer_id = '".$this->customer_id."', ip = '".$this->dbt->escape($ip)."', date_added = NOW()");
                    }
                }
                else $this->logout();
            }
        }
    }

    public function login($login, $passwd, $override = false)
    {
        ///return TRUE;
        $login = $this->dbt->escape($login);
        $passwd = $this->dbt->escape($passwd);

        ///$sql = "SELECT * FROM ".DB_PREFIX."customer WHERE LOWER(email) = '".$this->dbt->escape(utf8_strtolower($email))."' AND status = '1'";
        ///if( !$override ) $sql .= " AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('".$password."'))))) OR password = '".md5($password)."') ";
        ///$query = $this->dbt->query($sql);

        $query = $this->dbt->query("CALL pRB_get('rbCustomers',JSON_OBJECT('filter','Authorization','login','$login','pass','$passwd'),'ru',1,0,1,'')");
        $this->dbt->clear();
        if( $query->num_rows )
        {
            $this->data = $query->row;
            $this->data['hashAuth'] = arrEncryption([
                'login' => $login,
                'passwd' => $passwd,
            ]);
            $this->session->data['customer'] = $this->data;

            $this->customer_id = (int) get($query->row,'id_rbCounterparts');
            $this->discountCardID = (int) get($query->row,'DiscountCardID');

            $this->price_lid = (int) get($query->row,'id_rbPriceLevels');
            $this->org_name = get($query->row,'name_full');
            $this->org_inn  = get($query->row,'inn');
            $this->org_addr = get($query->row,'address');

            $this->manager_id = (int) get($query->row,'id_rbUsers_manager');
            $this->source_id  = (int) get($query->row,'id_rbCustomersSource');
            $this->stock_id   = (int) get($query->row,'id_rbStock');
            $this->stock_addr = get($query->row,'StockAddress');
            $this->stock_name = get($query->row,'rbStock_id_rbStock');
            $this->session->data['selectedStockID'] = $this->stock_id;
            $this->firm_id    = (int) get($query->row,'id_rbFirms');
            $this->stock_phone_1 = get($query->row,'StockPhone1');
            $this->stock_phone_2 = get($query->row,'StockPhone2');

            $this->firstname   = get($query->row,'name_i');
            $this->lastname    = get($query->row,'name_f');
            $this->patronymic  = get($query->row,'patronymic');
            $this->group_id    = get($query->row,'group_id');
            $this->email       = get($query->row,'email');
            $this->telephone   = get($query->row,'phone_formated');
            $this->newsletter  = get($query->row,'newsletter');
            $this->address_id  = get($query->row,'rbStock_id_rbStock');
            $this->country_id  = 176;

            $ip = ip();
            $this->db->query("UPDATE ".DB_PREFIX."customer SET language_id = '".(int)$this->config->get('config_language_id')."', ip='".$this->dbt->escape($ip)."' WHERE customer_id='".$this->customer_id."'");

            return TRUE;
        }
        return FALSE;
    }

    public function logout()
    {
        unset($this->session->data['customer']);

        $this->data        = NULL;
        $this->customer_id = NULL;
        $this->discountCardID = NULL;

        $this->price_lid  = NULL;
        $this->manager_id = NULL;
        $this->source_id  = NULL;
        $this->stock_id   = NULL;
        $this->firm_id    = NULL;
        $this->org_name   = NULL;
        $this->org_inn    = NULL;
        $this->org_addr   = NULL;

        $this->firstname   = NULL;
        $this->lastname    = NULL;
        $this->group_id    = NULL;
        $this->email       = NULL;
        $this->telephone   = NULL;
        $this->newsletter  = NULL;
        $this->address_id  = NULL;
        $this->country_id  = NULL;

        $this->stock_phone_1 = NULL;
        $this->stock_phone_2 = NULL;

        /**
         * Вот не понятно какого х это вне этого метода было
         * Каждый раз делается logout и вот эта партянка ниже
         */
        /// Наши добавленные
        ///unset($this->session->data['selectedStockID']); /// Выход не влияет на удаление выбранного офиса из сессии
        /// Родные
        unset($this->session->data['shipping_address']);
        unset($this->session->data['shipping_method']);
        unset($this->session->data['shipping_methods']);
        unset($this->session->data['payment_address']);
        unset($this->session->data['payment_method']);
        unset($this->session->data['payment_methods']);
        unset($this->session->data['comment']);
        unset($this->session->data['order_id']);
        unset($this->session->data['coupon']);
        unset($this->session->data['reward']);
        unset($this->session->data['voucher']);
        unset($this->session->data['vouchers']);
    }

    public function isLogged() {
        return ($this->customer_id);
    }

    public function getId() {
        return (int) $this->customer_id;
    }

    public function getManagerID() {
        return (int) $this->manager_id;
    }
    public function getSourceID() {
        return (int) $this->source_id;
    }
    public function getStockId() {
        return (int) $this->stock_id;
    }
    public function getStockIDGlobal() {
        return (int) ( $this->stock_id ? $this->stock_id : get($this->session->data,'selectedStockID') );
    }
    public function getStockAddr() {
        return $this->stock_addr;
    }
    public function getStockName() {
        return $this->stock_name;
    }
    public function getStockPhones() {
        $_phones = [];
        if( $this->stock_phone_1 ) array_push($_phones,$this->stock_phone_1);                        /// С базы 1
        if( $this->stock_phone_2 ) array_push($_phones,$this->stock_phone_2);                        /// С базы 2
        if( empty($_phones) ) $this->tezarius->getStockPhones();                                            /// Если пользователь не авторизован берем телефоны активного склада
        if( $_cfg_phone = $this->config->get('config_telephone') ) array_push($_phones,$_cfg_phone); /// Из настроек админки
        return $_phones;
    }
    public function getFirmId() {
        return (int) $this->firm_id;
    }


    public function getPriceLID() {
        return (int) $this->price_lid;
    }
    public function getDiscountCardID() {
        return (int) $this->discountCardID;
    }

    public function getFirstName() {
        return $this->firstname;
    }

    public function getLastName() {
        return $this->lastname;
    }

    public function getGroupId() {
        return $this->group_id;
    }

    public function getEmail() {
        return $this->email;
    }

    /**
     * Альтернатива из public/catalog/model/account/customer.php
     * Нет смыслда бегать в базу, если тут есть все данные
     * Лучше добавить пару полей, чем добзапрос
    */
    public function getCustomer()
    {
        return [
            'firstname'  => $this->firstname,
            'lastname'   => $this->lastname,
            'patronymic' => $this->patronymic,
            'email'      => $this->email,
            'telephone'  => $this->telephone,
            'org_name'   => $this->org_name,
            'org_inn'    => $this->org_inn,
            'org_addr'   => $this->org_addr,
        ];
    }

    public function getData() {
        return $this->data;
    }

    public function setHashAuth($login,$passwd)
    {
        $hashAuth = arrEncryption([
            'login' => $login,
            'passwd' => $passwd,
        ]);
        $this->data['hashAuth'] = $hashAuth;
        $this->session->data['customer'] = $this->data;
        return $hashAuth;
    }
    public function getHashAuth() {
        $hashAuth = get($this->data,'hashAuth');
        return $hashAuth;
    }
    public function getLogin() {
        $hashAuth = $this->getHashAuth();
        $hashAuth = arrDecryption($hashAuth);
        $login = get($hashAuth,'login');
        return $login;
    }
    public function getPasswd() {
        $hashAuth = $this->getHashAuth();
        $hashAuth = arrDecryption($hashAuth);
        $passwd = get($hashAuth,'passwd');
        return $passwd;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    /*/ /// Поидеи, нужно не всегда, перенесено пока в public/catalog/model/account/customer.php
    public function getNewsletter() {
        return $this->newsletter;
    }
    //*/

    public function getAddressId() {
        return $this->address_id;
    }

    public function getProperty($name) {
        return get($this->data,$name);
    }

    public function getBalance()
    {
        ///$query = $this->db->query("SELECT SUM(amount) AS total FROM ".DB_PREFIX."customer_transaction WHERE customer_id = '".(int)$this->customer_id."'");
        ///return $query->row['total'];
        /**
         * onBalance - на балансе по выданным
         * onActiveOrders - активных, невыданных заказов
         * onDebtOverdue - долг просрочено
         * onCredit - сумма отсрочки по договору поставки
        */
        if( !$this->customer_id ) return NULL;
        $sql = "CALL pFinanceCounterpartsInfo(JSON_OBJECT('type','Balance','id_rbCounterparts',{$this->customer_id}))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row['onBalance'];
    }

    public function getRewardPoints()
    {
        if( !$this->discountCardID ) return NULL;
        $query = $this->dbt->query("CALL `pRB_get`('rbDiscount',JSON_OBJECT('filter','id','id',{$this->discountCardID}),'ru',1,0,1,'')");
        $this->dbt->clear();
        ///dd($query->row);
        return get($query->row,'onBonus');
    }
}
/*
id : "74"
id_rbCounterparts : "151"
id_rbUsers : "3"
data : "2019-09-17 14:24:00"
isMarked : "N"
name : "Роман Трошков"
name_f : "Роман"
name_i : "Трошков"
name_o : null
name_full : null
inn : null
kpp : null
ogrn : null
id_rbCounterparts_group : null
id_rbGroups : "31"
id_rbPriceLevels : "1"
email : "borodatych222@demka.org"
id_rbCountry_phone : null
phone : null
phone_formated : null
phones : "795055303732"
comment : null
address_delivery : null
address : null
sms_allow : null
birthDay : null
id_rbStock : "2"
id_rbUsers_manager : null
isNeedFullPayOrderWork : null
isDelivery : null
login : "borodatych222@demka.org"
pass : "795055303732"
id_rbCustomersSource : null
isWholesaler : null
isDeliveryNoBell : null
PercentPrepayToWorkOrder : "10"
rbStock_id_rbStock : "Филиал Люберцы"
StockAddress : "г.Моква, ул. Спортивная 78"
StockPhone1 : ""
StockPhone2 : ""
rbCounterparts_group : null
isSendPriceOn : "0"
rbCounterparts_id_rbCounterparts_group : null
rbCountry_id_rbCountry_phone : null
rbCustomersSource_id_rbCustomersSource : null
rbGroups_id_rbGroups : "1. Новые с сайта"
rbPriceLevels_id_rbPriceLevels : "Розница"
rbUsers_id_rbUsers : "Бурда Василий Олегович"
rbUsers_id_rbUsers_manager : null
DiscountCard : null
DiscountCardID : null
DiscountCardType : null
hashAuth : "@@wc6UT0O6ISfiIzM3MDMzUT1NwUTO3IiOiQ2dzNXYwJCLicmcv5SYr1WZkBkMyIDajlHdhR2by9mYio
*/