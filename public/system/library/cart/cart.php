<?php
namespace Cart;
class Cart
{
    public $isTezarius = TRUE;

    private $tabs = [];
    private $products = [];
    public function __construct($registry)
    {
        $this->config   = $registry->get('config');
        $this->customer = $registry->get('customer');
        $this->request  = $registry->get('request');
        $this->session  = $registry->get('session');
        $this->db       = $registry->get('db');
        $this->dbt      = $registry->get('dbt');
        $this->tax      = $registry->get('tax');
        $this->weight   = $registry->get('weight');

        // Remove all the expired carts with no customer ID
        $this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE (api_id > '0' OR customer_id = '0') AND date_added < DATE_SUB(NOW(), INTERVAL 1 HOUR)");

        if( $this->customer->getId() )
        {
            // We want to change the session ID on all the old items in the customers cart
            $this->db->query("UPDATE " . DB_PREFIX . "cart SET session_id = '" . $this->db->escape($this->session->getId()) . "' WHERE api_id = '0' AND customer_id = '" . (int)$this->customer->getId() . "'");

            // Once the customer is logged in we want to update the customers cart
            $cart_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cart WHERE api_id = '0' AND customer_id = '0' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
            foreach( $cart_query->rows as $cart )
            {
                $this->db->query("DELETE FROM " . DB_PREFIX . "cart WHERE cart_id = '" . (int)$cart['cart_id'] . "'");

                // The advantage of using $this->add is that it will check if the products already exist and increaser the quantity if necessary.
                $this->add($cart['product_id'], $cart['quantity'], json_decode($cart['option']), $cart['recurring_id']);
            }
        }
    }

    public function loadTabs($ShopID,$CounterPartsID,$SessionId)
    {
        $sql = "CALL pCart('TabsInfo',JSON_OBJECT('isWebCart',1,'ShopID',{$ShopID},'CounterPartsID',{$CounterPartsID},'SessionId','{$SessionId}','LanguageCode','ru'))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->rows['sql'] = $sql;
        $this->tabs = $query->rows;
        return $this->tabs;
    }
    public function getTabs()
    {
        if( !$this->tabs )
        {
            $ShopID = (int) $this->customer->getStockIDGlobal();
            $UserID = (int) $this->customer->getId();
            $SessID = (string) $this->session->getId();
            $this->tabs = $this->loadTabs($ShopID,$UserID,$SessID);
        }
        return $this->tabs;

    }
    public function hasProducts()
    {
        if( !$this->tabs )
        {
            $ShopID = (int) $this->customer->getStockIDGlobal();
            $UserID = (int) $this->customer->getId();
            $SessID = (string) $this->session->getId();
            $this->tabs = $this->loadTabs($ShopID,$UserID,$SessID);
        }
        return (bool)($this->tabs);
    }
    public function loadBasket($ShopID,$TabID,$UserID,$SessID,$ByRecord)
    {
        $sql = "call pCart('Get',JSON_OBJECT('isWebCart',1,'ShopID',{$ShopID},'tabID',{$TabID},'CounterPartsID',{$UserID},'SessionId','$SessID','ByRecord',{$ByRecord},'LanguageCode','ru'))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        $this->products = $query->rows;
        ///$query->rows['sql'] = $sql;
        return $this->products;
    }
    public function getProducts()
    {
        if( $this->products ) return $this->products;
        $ShopID = (int) $this->customer->getStockIDGlobal();
        $UserID = (int) $this->customer->getId();
        $SessID = (string) $this->session->getId();
        $TabID = (int) get($this->request->cookie,'basketTabActive',0);
        $ByRecord = 0;
        $arr = $this->loadBasket($ShopID,$TabID,$UserID,$SessID,$ByRecord);
        return $arr;
    }
    public function add($rbg,$tzp,$qty,$std,$pld,$ShopID,$UserID,$PriceLID,$SessID,$TabID)
    {
        $GoodsTempID = 0;
        if( !$rbg )
        {
            $sql = "CALL pGoodsSupplierAdd(JSON_OBJECT('Source','price','StockID',{$std},'TZP','{$tzp}'),@id,1)";
            $query = $this->dbt->query($sql);
            $this->dbt->clear();
            if( get($query->row,'isError ') ) return $query->row;
            $query->row['sql'] = $sql;
            ///return $query->row;
            $GoodsTempID = get($query->row,'id');
        }
        $sql = "call pCart('Add',JSON_OBJECT(
                         'isWebCart',1,
                         'ShopID',{$ShopID},
                         'tabID',{$TabID},
                         'CounterPartsID',{$UserID},
                         'SessionId','{$SessID}',
                         'id_rbGoods',{$rbg},
                         'GoodsTempID',{$GoodsTempID},
                         'qty',{$qty},
                         'PriceLevelID',{$PriceLID},
                         'StockID',{$std},
                         'PlaceID',{$pld},
                         'note','',
                         'isHideArticleCode',0,
                        'LanguageCode','ru'))"
        ;
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }

    public function update($id,$ShopID,$TabID,$UserID,$SessID,$qty,$note,$PriceLID)
    {
        $sql = "call pCart('Upd',JSON_OBJECT('isWebCart',1,'id',{$id},'ShopID',{$ShopID},'tabID',{$TabID},'CounterPartsID',{$UserID},'SessionId','$SessID',
        'qty',{$qty},'note','{$note}','PriceLevelID',{$PriceLID},'LanguageCode','ru'));";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }

    public function remove($cart_id)
    {
        $sql = "call pCart('Upd',JSON_OBJECT('qty',0,'id',{$cart_id},'LanguageCode','ru'))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }

    public function clear()
    {
        $ShopID = (int) $this->customer->getStockIDGlobal();
        $UserID = (int) $this->customer->getId();
        $SessID = (string) $this->session->getId();
        $TabID = (int) get($this->request->cookie,'basketTabActive',0);

        $sql = "call pCart('TabDelete',JSON_OBJECT( 'isWebCart',1,'ShopID',{$ShopID},'CounterPartsID',{$UserID},'SessionId','{$SessID}','tabID',{$TabID},'LanguageCode','ru'))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }

    public function getRecurringProducts()
    {
        $product_data = [];
        foreach( $this->getProducts() as $value ) if( get($value,'recurring') ) $product_data[] = $value;
        return $product_data;
    }

    public function getWeight()
    {
        $weight = 0;
        return $weight; /// Пока не учитывается
        foreach( $this->getProducts() as $product ){
            if( $product['shipping'] ){
                $weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
            }
        }
        return $weight;
    }

    public function getSubTotal() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            $total += $product['total'];
        }

        return $total;
    }

    public function getTaxes() {
        $tax_data = array();
        return $tax_data;

        foreach ($this->getProducts() as $product) {
            if ($product['TaxSystemCode']) {
                $tax_rates = $this->tax->getRates($product['cost'], $product['TaxSystemCode']);

                foreach ($tax_rates as $tax_rate) {
                    if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
                        $tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['qty']);
                    } else {
                        $tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['qty']);
                    }
                }
            }
        }

        return $tax_data;
    }

    public function getTotal() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            $total += $this->tax->calculate($product['cost'], $product['TaxSystemCode'], $this->config->get('config_tax')) * $product['qty'];
        }

        return $total;
    }

    public function countProducts()
    {
        $product_total = 0;
        $products = $this->getProducts();
        foreach( $products as $product ) $product_total += $product['qty'];
        return $product_total;
    }

    public function hasRecurringProducts() {
        return count($this->getRecurringProducts());
    }

    /// У нас в корзине это проверяется, пока всегда TRUE возвращаем
    public function hasStock()
    {
        ///foreach ( $this->getProducts() as $product ) if( !$product['stock'] ) return FALSE;
        return TRUE;
    }
    /// Пока нет доставки
    public function hasShipping()
    {
        ///foreach( $this->getProducts() as $product ) if( $product['shipping'] ) return TRUE;
        return FALSE;
    }
    /// Пока нет данных скачивания
    public function hasDownload()
    {
        ///foreach( $this->getProducts() as $product ) if( $product['download'] ) return TRUE;
        return FALSE;
    }
}
/*
name : "Стойка амортизационная - Excel-G | перед прав | 333114"
properties : " "
code : "333114"
code_display : "333114"
brand : "KAYABA"
id_rbArticles : null
id_rbBrands : null
id : "780"
data_ins : "2019-09-25 16:17:19"
data : "2019-09-25 16:17:19"
id_rbUsers : "3"
tabID : "0"
id_rbCounterparts : null
isWebCart : "1"
id_rbStock_shop : "5"
id_rbStock : "6"
id_rbStockStoragePlace : null
id_rbGoods : null
id_rbGoods_Temp : "308"
qty : "1.000"
cost : "3540.00"
total : "3540.00"
cost_first : "3540.00"
discount : "0.00"
discount_detail : null
cost_in : "2281.90"
note : ""
isHideArticleCode : "0"
sess_id : "da81c316c44e46cfa657c8da69"
margin : "1258.10000"
unitName : ""
StorPlace : null
StockName : "Башавтоком"
qty_free : "100.000"
GoodsType : null
dscntGift : null
dscntBonus : null
dscntCard : null
dscntPromo : null
TaxSystemCode : ""
dlMin : "2019-10-02"
dlMax : "2019-10-09"
dlvr_period : "02 Октябрь- 09 Октябрь "
StockLogo : "DRM"
part : "1"
*/
