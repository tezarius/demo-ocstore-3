<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCommonFlash extends ControllerStartupTezarius
{
	public function index($flashers=[])
    {
        if( !$flashers ) $flashers = $this->flashers;
		return $this->load->view('common/flash',$flashers);
	}
}
