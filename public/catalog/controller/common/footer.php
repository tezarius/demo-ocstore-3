<?php



class ControllerCommonFooter extends ControllerStartupTezarius
{
	public function index()
    {
		$this->load->language('common/footer');

		$data['informations'] = [];
		$this->load->model('catalog/information');
		foreach( $this->model_catalog_information->getInformations() as $result )
		{
			if( $result['bottom'] )
			{
				$data['informations'][] = [
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				];
			}
		}

		$data['contact']      = $this->url->link('information/contact');
		$data['return']       = $this->url->link('account/return/add', '', true);
		$data['sitemap']      = $this->url->link('information/sitemap');
		$data['tracking']     = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher']      = $this->url->link('account/voucher', '', true);
		$data['affiliate']    = $this->url->link('affiliate/login', '', true);
		$data['special']      = $this->url->link('product/special');
		$data['account']      = $this->url->link('account/account', '', true);
		$data['order']        = $this->url->link('account/order', '', true);
		$data['wishlist']     = $this->url->link('account/wishlist', '', true);
		$data['newsletter']   = $this->url->link('account/newsletter', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if( $this->config->get('config_customer_online') )
		{
			$this->load->model('tool/online');

			$ip = get($this->request->server,'REMOTE_ADDR');

			$HTTP_HOST = get($this->request->server,'HTTP_HOST');
			$REQUEST_URI = get($this->request->server,'REQUEST_URI');
            $url = ( $HTTP_HOST && $REQUEST_URI )
                ? ( get($this->request->server,'HTTPS')?'https://':'http://' ).$HTTP_HOST.$REQUEST_URI
                : ''
            ;

            $referer = get($this->request->server,'HTTP_REFERER');

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		$data['scripts'] = $this->document->getScripts('footer');
		
		return $this->load->view('common/footer', $data);
	}
}
