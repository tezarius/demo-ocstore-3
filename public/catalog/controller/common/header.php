<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCommonHeader extends ControllerStartupTezarius
{
    public function __construct($registry)
    {
        parent::__construct($registry);
        /// Пока сделаем так: забрали из public/catalog/controller/common/header.php
        $this->data['office_menu'] = $this->load->controller('common/offices_list');
        ///pp($this->data['office_menu']);
    }

    public function index()
    {
		// Analytics
		$this->load->model('setting/extension');

		$this->data['analytics'] = array();

		$analytics = $this->model_setting_extension->getExtensions('analytics');

		foreach( $analytics as $analytic )
		{
			if ($this->config->get('analytics_'.$analytic['code'].'_status')) {
				$this->data['analytics'][] = $this->load->controller('extension/analytics/'.$analytic['code'], $this->config->get('analytics_'.$analytic['code'].'_status'));
			}
		}

        $_cfg = ($this->request->server['HTTPS']) ? 'config_ssl' : 'config_url';
        $server = $this->config->get($_cfg);

		if (is_file(DIR_IMAGE.$this->config->get('config_icon'))) {
			$this->document->addLink($server.'image/'.$this->config->get('config_icon'), 'icon');
		}

		$this->data['base'] = $server;

		$this->data['title']       = $this->document->getTitle();
		$this->data['description'] = $this->document->getDescription();
		$this->data['keywords']    = $this->document->getKeywords();
		$this->data['links']       = $this->document->getLinks();
		$this->data['robots']      = $this->document->getRobots();
		$this->data['styles']      = $this->document->getStyles();
		$this->data['scripts']     = $this->document->getScripts('header');

		$this->data['lang']      = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');

		$this->data['name'] = $this->config->get('config_name');

        $this->data['logo'] = ( is_file(DIR_IMAGE.$this->config->get('config_logo')) )
            ? $server.'image/'.$this->config->get('config_logo')
            : ''
        ;

		$this->load->language('common/header');
		
        $host = isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')) ? HTTPS_SERVER : HTTP_SERVER;


        $this->data['og_url'] = ( $this->request->server['REQUEST_URI'] == '/' )
            ? $this->url->link('common/home')
            : $host.substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1))
        ;
		$this->data['og_image'] = $this->document->getOgImage();


		// Wishlist
		if ($this->customer->isLogged())
        {
			$this->load->model('account/wishlist');

			$this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		}
        else {
			$this->data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$this->data['text_logged'] = sprintf(
            $this->language->get('text_logged'),
            $this->url->link('account', '', true),
            $this->customer->getFirstName(),
            $this->url->link('logout', '', true)
        );

        $this->data['logged'] = $this->customer->isLogged();

        $this->data['home']          = $this->url->link('common/home');
        $this->data['garage']        = $this->url->link('account/garage', '', true);
        $this->data['wishlist']      = $this->url->link('account/wishlist', '', true);
		$this->data['account']       = $this->url->link('account/account', '', true);
		$this->data['register']      = $this->url->link('account/register', '', true);
		$this->data['login']         = $this->url->link('account/login', '', true);
		$this->data['order']         = $this->url->link('account/order', '', true);
		$this->data['transaction']   = $this->url->link('account/transaction', '', true);
		$this->data['download']      = $this->url->link('account/download', '', true);
		$this->data['logout']        = $this->url->link('account/logout', '', true);
		$this->data['shopping_cart'] = $this->url->link('checkout/cart');
		$this->data['checkout']      = $this->url->link('checkout/checkout', '', true);
		$this->data['contact']       = $this->url->link('information/contact');

		$this->data['telephone'] = $this->customer->getStockPhones()[0]; /// Если версткой предусмотренно, то лучше все выводить

		$this->data['language'] = $this->load->controller('common/language');
		$this->data['currency'] = $this->load->controller('common/currency');

        $this->data['blog_menu'] = ( $this->config->get('configblog_blog_menu') )
            ? $this->load->controller('blog/menu')
            : ''
        ;

        /// Так как этот контроллер отрабатывает перед самым рендерингом, а работа с офисами нужна до всего, то вынес к конструктор public/catalog/controller/common.php
        ///$this->data['office_menu'] = $this->office_menu;

		$this->data['search'] = $this->load->controller('common/search');
		$this->data['cart']   = $this->load->controller('common/cart');
		$this->data['menu']   = $this->load->controller('common/menu');


        $this->data['clear'] = $this->url->link('common/home/clear', '', true);
        $this->data['text_clear'] = 'Забыть меня';

		return $this->load->view('common/header', $this->data);
	}
}
