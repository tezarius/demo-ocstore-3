<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCommonOfficesList extends ControllerStartupTezarius
{
	public function index()
    {
		$this->load->language('common/offices_list');
		///
        $this->load->model('offices');
        $offices = $this->model_offices->getList();
        ///dd($offices);
        $data['offices'] = $offices;

        /// Если клиент авторизовался, то будет и getStockId и getStockAddr
        $address = ''; /// $workIF = '';
        if( $current = $this->customer->getStockId() )
        {
            $address = $this->customer->getStockAddr();
            ///$workIF = 1;
        }
        elseif( $current = get($this->session->data,'selectedStockID') )
        {
            $isCoincidence = FALSE;
            foreach( $offices AS $office ) if( get($office,'id')==$current )
            {
                $address = get($office,'address');
                $isCoincidence = TRUE;
                $this->tezarius->setCurrentStock($office);
                break;
            }
            if( !$isCoincidence )
            {
                if( count($offices)==1 ) /// Не авторизован, не выбран офис и офис всего один
                {
                    $office_1 = get($offices,0);
                    $this->tezarius->setCurrentStock($office_1);
                    $current = get($office_1,'id');
                    $address = get($office_1,'address');

                    /// Установим в сессию новый Stock ID
                    $this->session->data['selectedStockID'] = $current;
                    ///$workIF = 3.1;
                }
            }
            ///else $workIF = 2;
        }
        elseif( count($offices)==1 ) /// Не авторизован, не выбран офис и офис всего один
        {
            $office_1 = get($offices,0);
            $this->tezarius->setCurrentStock($office_1);
            $current = get($office_1,'id');
            $address = get($office_1,'address');

            /// Установим в сессию новый Stock ID
            $this->session->data['selectedStockID'] = $current;
            ///$workIF = 3.2;
        }
        ///dd([$workIF,$current,$address,$this->session->data]);

        ///$current = (int) ( ($csid = $this->customer->getStockId()) ? $csid : get($this->session->data,'selectedStockID'));
        ///$address = $this->customer->getStockAddr();
        ///if( !$address ) foreach( $offices AS $office ) if( get($office,'id')==$current ) $address = get($office,'address');
        ///
        $data['text_offices'] = ( $address )
            ? $address
            : $this->language->get('text_offices')
        ;
        ///
        $data['current'] = $current;
        $this->session->data['offices'] = $offices;
        ///
        ///dd($this->customer->getStockAddr());
		return $this->load->view('common/offices_list', $data);
	}
	public function selected()
    {
        $office = rcv('office');
        $current = $this->customer->getStockId();
        if( $office!==$current )
        {
            /// Внутри удаляется selectedStockID и сопутствующее
            $this->customer->logout();

            /// Установим в сессию новый Stock ID
            $this->session->data['selectedStockID'] = $office;

            ///
            $redirect = ( $current )
                ? $this->url->link('account/logout','',true)
                : htmlEntityDecode(get($this->request->server,'HTTP_REFERER'))
            ;

            if( isAjax() ) jEcho(['redirect'=>$redirect]);

            $this->response->redirect($this->url->link('account/logout', '', true));
        }
        jEcho([$office,$this->customer->getStockId()]);
    }
}