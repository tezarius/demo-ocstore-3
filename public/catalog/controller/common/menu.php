<?php


class ControllerCommonMenu extends ControllerStartupTezarius
{
    public function index()
    {
        $this->load->language('common/menu');

        // Menu
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');

        $data['categories'] = [];

        $categories = $this->model_catalog_category->getCategories();
        $tree = $this->buildTree($categories);
        ///pp($tree);
        $data['categories'] = $tree;
        /*/
        function buildTreeHTML(array $elements, $parentId = 0, $level = 0)
        {
            ++$level;
            $branch = array();
            foreach( $elements as $element )
            {
                if( $element['id_parent'] == $parentId )
                {
                    $element['level'] = $level;
                    $children = buildTreeHTML($elements,$element['id'],$level);
                    if( $children ) $element['children'] = $children;
                    $branch[] = $element;
                }
            }
            return $branch;
        }
        $treeHTML = buildTreeHTML($categories);
        pp($treeHTML);
        //*/

        /*/
        $this->load->model('catalog/category');
        $categories = $this->model_catalog_category->getCategories(0);
        pp($categories);
        foreach( $categories as $category )
        {
            if( @$category['top'] )
            {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach( $children as $child )
                {
                    $filter_data = array(
                        'filter_category_id'  => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                // Level 1
                $data['categories'][] = array(
                    'name'     => $category['name'],
                    'children' => $children_data,
                    'column'   => $category['column'] ? $category['column'] : 1,
                    'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }
        //*/

        return $this->load->view('common/menu', $data);
    }

    private function buildTree(array $elements, $parentId = 0, $level = 0)
    {
        ++$level;
        $branch = array();
        foreach( $elements as $element )
        {
            if( $element['id_parent'] == $parentId )
            {
                $id = $element['id'];
                $name = $element['name'];
                $hash = arrEncryption([
                    'id' => $id,
                    'name' => $name,
                ]);

                $element['level'] = $level;
                $element['column'] = 4;
                $element['href'] = $this->url->link('price/goods',"hash=$hash");
                $children = $this->buildTree($elements,$id,$level);
                if( $children ) $element['children'] = $children;
                $branch[] = $element;
            }
        }
        return $branch;
    }
}
