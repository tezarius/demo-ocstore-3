<?php

class ControllerCommonHome extends ControllerStartupTezarius
{
	public function index()
    {
        $this->load->language('common/home');
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setTitle('TEZARIUS HOME');
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if( isset($this->request->get['route']) )
		{
			$canonical = $this->url->link('common/home');
			if( $this->config->get('config_seo_pro') && !$this->config->get('config_seopro_addslash') )
			{
				$canonical = rtrim($canonical, '/');
			}
			$this->document->addLink($canonical, 'canonical');
		}

        $this->render('common/home');
	}

	/// http://demo.tezarius.lo/index.php?route=common/home/clear
    public function clear()
    {
        $this->customer->logout();
        unset($this->session->data['selectedStockID']);
        $redirect = get($this->request->server,'HTTP_REFERER',$this->url->link('account/logout', '', true));
        if( isAjax() ) jEcho(['redirect'=>$redirect]);
        $this->response->redirect($redirect);
    }
}