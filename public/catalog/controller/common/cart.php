<?php


class ControllerCommonCart extends ControllerStartupTezarius
{
    public function index()
    {
        $this->load->language('common/cart');


        // loadBasket должен идти в начале, так как потом все функцию юзают загруженные позициии
        ///$ByRecord = 0;
        $ShopID = (int) $this->getStockID();
        $UserID = (int) $this->customer->getId();
        $SessID = $this->session->getId();
        $tabActive = $this->getBasketTabActive();
        ///$products = $this->cart->loadBasket($ShopID,$tabActive,$UserID,$SessID,$ByRecord);
        $products = $this->cart->getProducts();


        $tabs = $this->cart->loadTabs($ShopID,$UserID,$SessID);
        ///dd($tabs);
        $tabInfo = [];
        foreach( $tabs AS $t ) if( (int)get($t,'tabID')===$tabActive ){ $tabInfo = $t; break; }
        $_total = $this->currency->format((int)get($tabInfo,'total'), $this->session->data['currency']);
        $_count = (int)get($tabInfo,'count');
        $data['text_items'] = sprintf( $this->language->get('text_items'), $_count, $_total );


        // Totals
        $this->load->model('setting/extension');
        //
        $totals = [];
        $taxes = $this->cart->getTaxes();
        $total = 0;
        // Because __call can not keep var references so we put them into an array.
        $total_data = array(
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total
        );
        // Display prices
        if( $this->customer->isLogged() || !$this->config->get('config_customer_price') )
        {
            $sort_order = array();
            $results = $this->model_setting_extension->getExtensions('total');
            foreach( $results as $key => $value ) $sort_order[$key] = $this->config->get('total_'.$value['code'].'_sort_order');
            array_multisort($sort_order, SORT_ASC, $results);
            foreach( $results as $result )
            {
                if( $this->config->get('total_'.$result['code'].'_status') )
                {
                    $this->load->model('extension/total/'.$result['code']);
                    // We have to put the totals in an array so that they pass by reference.
                    $this->{'model_extension_total_'.$result['code']}->getTotal($total_data);
                }
            }
            $sort_order = array(); foreach( $totals as $key => $value ) $sort_order[$key] = $value['sort_order'];
            array_multisort($sort_order, SORT_ASC, $totals);
        }
        ///dd($totals);


        $this->load->model('tool/image');
        $this->load->model('tool/upload');
        if( 34==76 ) foreach( $products as $product )
        {
            $image = ($product['image'])
                ? $this->model_tool_image->resize(
                    $product['image'],
                    $this->config->get('theme_'.$this->config->get('config_theme').'_image_cart_width'),
                    $this->config->get('theme_'.$this->config->get('config_theme').'_image_cart_height')
                )
                : ''
            ;

            $option_data = [];
            foreach( $product['option'] as $option )
            {
                if( $option['type'] != 'file' )
                {
                    $value = $option['value'];
                }
                else
                {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
                    $value = ($upload_info) ? $upload_info['name'] : '';
                }

                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20).'..' : $value),
                    'type'  => $option['type']
                );
            }

            // Display prices
            if( $this->customer->isLogged() || !$this->config->get('config_customer_price') )
            {
                $unit_price = $this->tax->calculate($product['cost'], $product['tax_class_id'], $this->config->get('config_tax'));

                $price = $this->currency->format($unit_price, $this->session->data['currency']);
                $total = $this->currency->format($unit_price * $product['qty'], $this->session->data['currency']);
            }
            else {
                $price = false;
                $total = false;
            }

            $data['products'][] = array(
                'cart_id'   => $product['cart_id'],
                'thumb'     => $image,
                'name'      => $product['name'],
                'model'     => $product['model'],
                'option'    => $option_data,
                'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
                'quantity'  => $product['qty'],
                'price'     => $price,
                'total'     => $total,
                'href'      => $this->url->link('product/product', 'product_id='.$product['product_id'])
            );
        }
        $data['products'] = $products;


        $data['totals'] = [];
        foreach( $totals as $total )
        {
            $data['totals'][] = array(
                'title' => $total['title'],
                'text'  => $this->currency->format($total['value'], $this->session->data['currency']),
            );
        }


        // Gift Voucher
        $data['vouchers'] = [];
        if( !empty($this->session->data['vouchers']) )
        {
            foreach( $this->session->data['vouchers'] as $key => $voucher )
            {
                $data['vouchers'][] = array(
                    'key'         => $key,
                    'description' => $voucher['description'],
                    'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency'])
                );
            }
        }


        $data['cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);

        return $this->load->view('common/cart', $data);
    }
    public function info()
    {
        $this->response->setOutput($this->index());
    }
}