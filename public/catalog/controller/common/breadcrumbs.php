<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCommonBreadcrumbs extends ControllerStartupTezarius
{
	public function index($breadcrumbs=[])
    {
        if( !$breadcrumbs ) $breadcrumbs = $this->breadcrumbs;
		return $this->load->view('common/breadcrumbs',$breadcrumbs);
	}
}
