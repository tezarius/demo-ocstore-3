<?php


class ControllerCommonSearch extends ControllerStartupTezarius
{
    public function index()
    {
        $this->load->language('common/search');

        $data['text_search'] = $this->language->get('text_search');

        $data['search'] = rcv('search');

        /*/
        $this->load->model('account/customer');
        $UserID = (int) $this->customer->getId();
        $arr = $this->model_account_customer->historySearch($UserID);
        $arrStack = []; $arrHistory = [];
        foreach( $arr AS $h )
        {
            $hash = crc32(get($h,'code').get($h,'brand'));
            if( !in_array($hash,$arrStack) )
            {
                array_push($arrStack,$hash);
                array_push($arrHistory,$h);
            }
        }
        dd($arrHistory);
        //*/

        return $this->load->view('common/search', $data);
    }
    /**
     * https://github.com/bassjobsen/Bootstrap-3-Typeahead
     */
    public function history()
    {
        $json = [];
        $this->load->model('account/customer');
        $UserID = (int) $this->customer->getId();
        $arr = $this->model_account_customer->historySearch($UserID);
        $arrStack = []; $arrHistory = []; $i = 0;
        foreach( $arr AS $h )
        {
            $hash = crc32(get($h,'code').get($h,'brand'));
            if( !in_array($hash,$arrStack) )
            {
                array_push($arrStack,$hash);
                array_push($arrHistory,[
                    'id' => $i++,
                    'name' => $h['code'].' '.$h['brand'],
                    'code' => $h['code'],
                    'brand' => $h['brand'],
                ]);
            }
        }
        $json['history'] = $arrHistory;
        ///dd($rr);
        $this->jEcho($json);
    }
}