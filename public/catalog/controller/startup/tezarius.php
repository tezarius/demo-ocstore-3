<?php
/**
 * @package		OpenCart -> Tezarius
 * @author		Roman Troshkov
 * @copyright	Copyright (c) 2019 - 2020, Tezarius, Ltd. (https://tezarius.ru/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
 *
 * Tezarius Common Controller class
 *
*/
class ControllerStartupTezarius extends Controller
{
	protected $data = [];
	protected $flashers = [];
	protected $breadcrumbs = [];

	public function __construct($registry)
    {
		parent::__construct($registry);
    }
    /**
     * Если что-то нужно выполнить на старпе, к примеру забрать сообщения из сессии после редиректа
     * Объявляется тут: public/system/config/catalog.php -> $_['action_pre_action'] = []
    */
    public function index()
    {
        // Flash
        if( isset($this->session->data['flashers']) )
        {
            $this->flashers = array_merge($this->flashers,$this->session->data['flashers']);
            ///unset($this->session->data['flashers']);
        }
        $this->session->data['flashers'] = [];
    }
    /**
     * Пытается выбрать офис при пепрвой загрузке страницы
     * Пока скостылил: $this->data['office_menu']
    */
    private function choosingOffice()
    {
        ///
    }
	/**
     * Рендерим все базовые элементы
     * Нахера это описывал в роднов движке при кадом чихе в душе не танцую
     */
	protected function renderCommon()
    {
        // General Sections
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Все это идет через таблицы `layout`=>`layout_route`=>`layout_module` | Tezarius
        $this->data['column_left']    = $this->load->controller('common/column_left');
        $this->data['column_right']   = $this->load->controller('common/column_right');
        $this->data['content_top']    = $this->load->controller('common/content_top');
        $this->data['content_bottom'] = $this->load->controller('common/content_bottom');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->data['header'] = $this->load->controller('common/header');
        $this->data['footer'] = $this->load->controller('common/footer');
        $this->data['flash']  = $this->load->controller('common/flash',['flashers'=>$this->flashers]);
        $this->data['breadcrumbs']  = $this->load->controller('common/breadcrumbs',['breadcrumbs'=>$this->breadcrumbs]);
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    /**
     * Итоговый ренлдеринг и отлдача контента
     * @param $name
     * @param bool $withCommon
     */
    protected function render($name,$withCommon=TRUE)
    {
        if( $withCommon ) $this->renderCommon();
        $this->response->setOutput($this->load->view($name,$this->data));
    }
    /**
     * JSON Echo For ocStore
     * @param $arr
     */
    protected function jEcho($arr=[])
    {
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($arr));
    }
    /**
     * Офис выбран ?
     * Пока только добавление flash alert
     * Возвращает либо StockID или FALSE
     * StockID в приоритете берется от привязки пользователя, только потом из сессии
     */
    protected function officeChoosed()
    {
        $current = (int) ( ($csid = $this->customer->getStockId()) ? $csid : get($this->session->data,'selectedStockID'));
        if( $current )
        {
            $isChoose = $current;
        }
        else
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => 'Для продолжения нужно выбрать офис!'
            ];
            $isChoose = FALSE;
        }
        return $isChoose;
    }
    /**
     * Получение Stock ID
     * Сперва относительно Customer
     * Потом относительно выбранного из session
    */
    protected function getStockID()
    {
        return (int) ( ($csid = $this->customer->getStockId()) ? $csid : get($this->session->data,'selectedStockID'));
    }
    protected function getStockAddress()
    {
        ///
    }
    /**
     *
     */
    protected function getBasketTabActive()
    {
        return (int) get($this->request->cookie,'basketTabActive',0);
    }
    protected function setBasketTabActive($tabActive)
    {
        ///$this->session->data['basketTabActive'] = $tabActive;
        setcookie('basketTabActive',$tabActive, time()+60*60*24*180,'/');
    }
}