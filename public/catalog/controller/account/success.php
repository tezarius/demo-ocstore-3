<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerAccountSuccess extends Controller
{
	public function index()
    {
		$this->load->language('account/success');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

		$data['breadcrumbs'] = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        ],[
            'text' => $this->language->get('text_success'),
            'href' => $this->url->link('account/success')
        ]];

		$data['text_message'] = ( $this->customer->isLogged() )
        ? sprintf($this->language->get('text_message'),$this->url->link('information/contact'))
        : sprintf($this->language->get('text_approval'),$this->config->get('config_name'), $this->url->link('information/contact'))
    ;

        $data['continue'] = ($this->cart->hasProducts())
            ? $this->url->link('checkout/cart')
            : $this->url->link('account/account', '', true)
        ;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Все это идет через таблицы `layout`=>`layout_route`=>`layout_module` | Tezarius
        $data['column_left']    = $this->load->controller('common/column_left');
        $data['column_right']   = $this->load->controller('common/column_right');
        $data['content_top']    = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}