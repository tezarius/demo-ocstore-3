<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountLogin extends ControllerStartupTezarius
{
	public function index()
    {
		$this->load->model('account/customer');

		// Login override for admin users
		if( !empty($this->request->get['token']) )
		{
			$this->customer->logout();
			$this->cart->clear();

			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

			if ($customer_info && $this->customer->login($customer_info['email'], '', true)) {
				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}

		if( $this->customer->isLogged() )
		{
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/login');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

		if( get($this->request->server,'REQUEST_METHOD')=='POST' && $this->validate() )
        {
            ///dd([$this->customer->getHashAuth(),$this->customer->getData(),$this->customer,$this->session->data]);
            ///dd([$this->request->post,$this->session]); /// look session customer
            // Unset guest
            unset($this->session->data['guest']);

            // Default Shipping Address
            /*/
            $this->load->model('account/address');

            if ($this->config->get('config_tax_customer') == 'payment') {
                $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            }

            if ($this->config->get('config_tax_customer') == 'shipping') {
                $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
            }
            //*/
            // Wishlist
            /*/
			if( isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist']) )
            {
				$this->load->model('account/wishlist');
				foreach( $this->session->data['wishlist'] as $key => $product_id )
                {
					$this->model_account_wishlist->addWishlist($product_id);
					unset($this->session->data['wishlist'][$key]);
				}
			}
            //*/
            // Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
            $redirectPost = get($this->request->post,'redirect',NULL);
            ///dd($redirectPost);
			if( $redirectPost!==NULL
                && $redirectPost != $this->url->link('account/logout', '', true)
                && (
                    strpos($redirectPost, $this->config->get('config_url')) !== FALSE
                    || strpos($redirectPost, $this->config->get('config_ssl')) !== FALSE
                )
            ){
                ///dd($redirectPost); /// look session customer
				$this->response->redirect(str_replace('&amp;','&',$redirectPost));
			}
            else
            {
                ///dd([$this->request->post,$this->session]); /// look session customer
				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}

		$this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        ],[
            'text' => $this->language->get('text_login'),
            'href' => $this->url->link('account/login', '', true)
        ]];

		$this->data['action']    = $this->url->link('account/login', '', true);
		$this->data['register']  = $this->url->link('account/register', '', true);
		$this->data['forgotten'] = $this->url->link('account/forgotten', '', true);

		/// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
        $redirectPost = get($this->request->post,'redirect',NULL);
		if(
            $redirectPost!==NULL && ( 
                strpos($redirectPost, $this->config->get('config_url')) !== FALSE
                || strpos($redirectPost, $this->config->get('config_ssl')) !== FALSE
            )
        ){
			$this->data['redirect'] = $redirectPost;
		}
        elseif( isset($this->session->data['redirect']) )
        {
			$this->data['redirect'] = $this->session->data['redirect'];
			unset($this->session->data['redirect']);
		}
        else {
			$this->data['redirect'] = '';
		}

        $this->data['email'] = get($this->request->post,'email');
        $this->data['password'] = get($this->request->post,'password');

        $this->renderCommon();
		$this->response->setOutput($this->load->view('account/login', $this->data));
	}

	protected function validate()
    {
        $isValidate = TRUE;
        ///return $isValidate;

        $email = rcv('email');
        $pass = rcv('password');

		// Check how many login attempts have been made.
        /*/
		$login_info = $this->model_account_customer->getLoginAttempts($email);
		if( $login_info
            && $login_info['total'] >= $this->config->get('config_login_attempts')
            && strtotime('-1 hour') < strtotime($login_info['date_modified'])
        ){
			$this->flashers[] = [
                'type' => 'error',
                'text' => $this->language->get('error_attempts')
            ];
            $isValidate = FALSE;
		}
        //*/
		// Check if customer has been approved.
        /*/
		$customer_info = $this->model_account_customer->getCustomerByEmail($email);
		if( $customer_info && !$customer_info['status'] )
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => $this->language->get('error_approved')
            ];
            $isValidate = FALSE;
		}
        //*/
		if( $isValidate )
        {
			if( !$this->customer->login($email,$pass) )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => $this->language->get('error_login')
                ];
                $isValidate = FALSE;

				///$this->model_account_customer->addLoginAttempt($email);
			}
            else
            {
				///$this->model_account_customer->deleteLoginAttempts($email);
			}
		}

		return $isValidate;
	}
}
