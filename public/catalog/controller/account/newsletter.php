<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountNewsletter extends ControllerStartupTezarius
{
	public function index()
    {
		if( !$this->customer->isLogged() )
		{
			$this->session->data['redirect'] = $this->url->link('account/newsletter', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/newsletter');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

		///if( $this->request->server['REQUEST_METHOD'] == 'POST' ) /// Тут метод POST только ловится
		if( buttonPressed('btnNotified') ) /// Тут именно имя кнопки и кнопкали это !
		{
		    ///dd($this->request->post);
			$this->load->model('account/customer');
            $res = $this->model_account_customer->editNewsletter($this->request->post['notified']);
            if( get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get($res,'mess'),
                ];
            }
            else
            {$this->session->data['flashers'][] = [
                    'type' => 'success',
                    'text' => $this->language->get('text_success'),
                ];
                $this->response->redirect($this->url->link('account/account', '', true));
            }
		}

		$this->breadcrumbs = [[
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
        ],[
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
        ],[
			'text' => $this->language->get('text_newsletter'),
			'href' => $this->url->link('account/newsletter', '', true)
        ]];

        $this->load->model('account/customer');
        $this->data['notices'] = $this->model_account_customer->getNewsletter();
		///$this->data['newsletter'] = $this->customer->getNewsletter();

		$this->data['action'] = $this->url->link('account/newsletter', '', true);
		$this->data['back'] = $this->url->link('account/account', '', true);

		/// https://www.bootstraptoggle.com/
        $this->document->addScript('catalog/view/libraries/bootstrap-toggle/js/bootstrap-toggle.min.js');
        $this->document->addStyle('catalog/view/libraries/bootstrap-toggle/css/bootstrap-toggle.min.css');
		$this->render('account/newsletter');
	}
}