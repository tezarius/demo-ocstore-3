<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountEdit extends ControllerStartupTezarius
{
	private $error = array();

	public function index()
    {
		if( !$this->customer->isLogged() )
		{
			$this->session->data['redirect'] = $this->url->link('account/edit', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}
		$this->load->language('account/edit');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/customer');
		if( get($this->request->server,'REQUEST_METHOD')=='POST' && $this->validate() )
		{
		    ///dd($this->request->post);
			$res = $this->model_account_customer->editCustomer($this->customer->getId(),$this->customer->getStockId(),$this->request->post);

            if( get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get($res,'mess')
                ];
                ///dd($this->flashers);
            }
            else
            {
                $this->session->data['flashers'][] = [
                    'type' => 'success',
                    'text' => $this->language->get('text_success')
                ];
                $this->response->redirect($this->url->link('account/account', '', true));
            }
		}

		$this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        ],[
            'text' => $this->language->get('text_edit'),
            'href' => $this->url->link('account/edit', '', true)
        ]];

        $this->data['error_warning']      = get($this->error,'warning');
        $this->data['error_firstname']    = get($this->error,'firstname');
        $this->data['error_lastname']     = get($this->error,'lastname');
        $this->data['error_patronymic']   = get($this->error,'patronymic');
        $this->data['error_email']        = get($this->error,'email');
        $this->data['error_telephone']    = get($this->error,'telephone');
        $this->data['error_custom_field'] = get($this->error,'custom_field');

        $this->data['error_org_name'] = get($this->error,'org_name');
        $this->data['error_org_inn']  = get($this->error,'org_inn');
        $this->data['error_org_addr'] = get($this->error,'org_addr');

		$this->data['action'] = $this->url->link('account/edit', '', true);

        $customer_info = $customer = [];
		if( $this->request->server['REQUEST_METHOD'] != 'POST' )
		{
		    ///$customer_info = $this->model_account_customer->getCustomer($this->customer->getId()); /// Какие-то кастомные поля, оставим как идею
			$customer = $this->customer->getCustomer();
		}

		$this->data['firstname']  = rcv('firstname',get($customer,'firstname'));
        $this->data['lastname']   = rcv('lastname',get($customer,'lastname'));
        $this->data['patronymic'] = rcv('patronymic',get($customer,'patronymic'));
        $this->data['email']      = rcv('email',get($customer,'email'));
        $this->data['telephone']  = rcv('telephone',get($customer,'telephone'));

        $this->data['org_name'] = rcv('org_name',get($customer,'org_name'));
        $this->data['org_inn']  = rcv('org_inn',get($customer,'org_inn'));
        $this->data['org_addr'] = rcv('org_addr',get($customer,'org_addr'));


		// Custom Fields
		$this->data['custom_fields'] = [];
		$this->load->model('account/custom_field');
		$custom_fields = $this->model_account_custom_field->getCustomFields($this->config->get('config_customer_group_id'));
		foreach( $custom_fields as $custom_field )
		{
			if( $custom_field['location'] == 'account' ) $this->data['custom_fields'][] = $custom_field;
		}

		if( isset($this->request->post['custom_field']['account']) )
		    $this->data['account_custom_field'] = $this->request->post['custom_field']['account'];
		elseif( !empty($customer_info) )
            $this->data['account_custom_field'] = json_decode(get($customer_info,'custom_field'),true);
		else
		    $this->data['account_custom_field'] = [];


		$this->data['back'] = $this->url->link('account/account', '', true);

        $this->renderCommon();
		$this->response->setOutput($this->load->view('account/edit', $this->data));
	}

	protected function validate()
    {
        $firstname = rcv('firstname');
        $firstname_l = utf8_strlen($firstname);
        if( $firstname_l < 1 || $firstname_l > 32 ) $this->error['firstname'] = $this->language->get('error_firstname');

        $lastname = rcv('lastname');
        $lastname_l = utf8_strlen($lastname);
        if( $lastname_l < 1 || $lastname_l > 32 ) $this->error['lastname'] = $this->language->get('error_lastname');

        $patronymic = rcv('patronymic');
        $patronymic_l = utf8_strlen($patronymic);
        if( $patronymic_l < 1 || $patronymic_l > 32 ) $this->error['patronymic'] = $this->language->get('error_patronymic');

        $email = rcv('email');
        if( (utf8_strlen($email) > 96) || !filter_var($email, FILTER_VALIDATE_EMAIL) )
        {
            $this->error['email'] = $this->language->get('error_email');
        }
		if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('error_exists');
		}

        $telephone = rcv('telephone');
        $telephone = preg_replace("/[^0-9]/", '', $telephone);
        if( $telephone && utf8_strlen($telephone) < 11 )
        {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        // Organization
        $org_name = rcv('org_name');
        $org_inn  = rcv('org_inn');
        $org_addr = rcv('org_addr');
        if( $org_name || $org_inn || $org_addr && (!$org_name || !$org_inn || !$org_addr) )
        {
            if( !$org_name || utf8_strlen($org_name)<4 ) $this->error['org_name'] = $this->language->get('error_org_name');
            if( !$org_inn  || utf8_strlen($org_inn)<10 ) $this->error['org_inn']  = $this->language->get('error_org_inn');
            if( !$org_addr || utf8_strlen($org_addr)<5 ) $this->error['org_addr'] = $this->language->get('error_org_addr');
        }

		// Custom field validation
		$this->load->model('account/custom_field');
		$custom_fields = $this->model_account_custom_field->getCustomFields('account', $this->config->get('config_customer_group_id'));
		foreach( $custom_fields as $custom_field )
		{
			if( $custom_field['location'] == 'account' )
			{
				if( $custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']]) )
				{
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				}
				elseif( $custom_field['type'] == 'text' && !empty($custom_field['validation'])
                    && !filter_var(
                        $this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']],
                        FILTER_VALIDATE_REGEXP,
                        array('options' => array('regexp' => $custom_field['validation']))
                    )
                ){
					$this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
				}
			}
		}

		return !$this->error;
	}
}