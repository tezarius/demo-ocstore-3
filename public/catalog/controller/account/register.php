<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountRegister extends ControllerStartupTezarius
{
	private $error = [];

	public function index()
    {
		if( $this->customer->isLogged() )
		{
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		$this->load->language('account/register');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $StockID = $this->officeChoosed();

        $this->data = $this->request->post;
        $this->data['StockID'] = $StockID;
        ///dd($this->data);

		$this->load->model('account/customer');
		if( $StockID && get($this->request->server,'REQUEST_METHOD')=='POST' && $this->validate() )
		{
		    ///dd($this->data);
			$oReg = $this->model_account_customer->addCustomer($this->data);
            ///dd($oReg);
            if( (int) get($oReg,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get($oReg,'mess'),
                ];
            }
            else
            {
                $CustomerUID = get($oReg,'AnyID');
                $this->session->data['CustomerUID'] = $CustomerUID;
                if( $CustomerUID )
                {
                    $susMsg = rcv('firstname')." ".rcv('lastname')."! ".'Congratulations! Registration completed!';
                    $this->flashers[] = [
                        'type' => 'success',
                        'text' => $susMsg,
                    ];

                    // Clear any previous login attempts for unregistered accounts.
                    $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

                    $this->customer->login(rcv('email'), rcv('password'));

                    unset($this->session->data['guest']);

                    $this->response->redirect($this->url->link('account/success'));
                }
                else
                {
                    $this->flashers[] = [
                        'type' => 'error',
                        'text' => 'Что-то пошло не так :( [tzr]',
                    ];
                }
            }
		}

		$this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        ],[
            'text' => $this->language->get('text_register'),
            'href' => $this->url->link('account/register', '', true)
        ]];

		$this->data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));

        $this->data['error_firstname']    = get($this->error,'firstname');
        $this->data['error_lastname']     = get($this->error,'lastname');
        $this->data['error_email']        = get($this->error,'email');
        $this->data['error_telephone']    = get($this->error,'telephone');
        $this->data['error_custom_field'] = get($this->error,'custom_field',[]);
        $this->data['error_password']     = get($this->error,'password');
        $this->data['error_confirm']      = get($this->error,'confirm');

        if( $_err = get($this->error,'agree') ) $this->flashers[] = [
            'type' => 'error',
            'text' => $_err,
        ];


        $this->data['action'] = $this->url->link('account/register','',true);

        $this->data['customer_group_id'] = rcv('customer_group_id');
        $this->data['firstname'] = rcv('firstname');
        $this->data['lastname']  = rcv('lastname');
        $this->data['email']     = rcv('email');
        $this->data['telephone'] = rcv('telephone');

        $this->data['password']   = rcv('password');
        $this->data['confirm']    = rcv('confirm');
        $this->data['newsletter'] = rcv('newsletter');

		// Captcha
		if( $this->config->get('captcha_'.$this->config->get('config_captcha').'_status') && in_array('register',(array)$this->config->get('config_captcha_page')) )
        {
			$this->data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
		}
        else $this->data['captcha'] = '';


        $this->data['text_agree'] = '';
		if( $this->config->get('config_account_id') )
		{
			$this->load->model('catalog/information');
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
			if( $information_info )
            {
				$this->data['text_agree'] = sprintf($this->language->get('text_agree'),
                    $this->url->link('information/information/agree', 'information_id='.$this->config->get('config_account_id'), true),
                    $information_info['title'],
                    $information_info['title']
                );
			}
		}
	    $this->data['agree'] = rcv('agree');

        ///dd($this->flashers);
        $this->renderCommon();
		$this->response->setOutput($this->load->view('account/register', $this->data));
	}

	private function validate()
    {
        $firstname = rcv('firstname');
        $firstname_l = utf8_strlen($firstname);
		if( $firstname_l < 1 || $firstname_l > 32 ) $this->error['firstname'] = $this->language->get('error_firstname');

        $lastname = rcv('lastname');
        $lastname_l = utf8_strlen($lastname);
		if( $lastname_l < 1 || $lastname_l > 32 ) $this->error['lastname'] = $this->language->get('error_lastname');

		$email = rcv('email');
		if( (utf8_strlen($email) > 96) || !filter_var($email, FILTER_VALIDATE_EMAIL) )
		{
			$this->error['email'] = $this->language->get('error_email');
		}

		$telephone = rcv('telephone');
        $telephone = preg_replace("/[^0-9]/", '', $telephone);
        if( utf8_strlen($telephone) < 11 )
        {
            $this->error['telephone'] = $this->language->get('error_telephone');
        }

        ///$password = rcv('password');
        $password = html_entity_decode(rcv('password'), ENT_QUOTES, 'UTF-8');
        $password_l = utf8_strlen($password);
		if( $password_l < 4 || $password_l > 40) $this->error['password'] = $this->language->get('error_password');
		if( rcv('confirm') != rcv('password') )
		{
			$this->error['confirm'] = $this->language->get('error_confirm');
		}

		// Captcha
		if( $this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page')) )
		{
			$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');
			if( $captcha ) $this->error['captcha'] = $captcha;

		}

		// Agree to terms
		if( $this->config->get('config_account_id') )
		{
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

			if( $information_info && !isset($this->request->post['agree']) )
			{
				$this->error['agree'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		return !$this->error;
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}