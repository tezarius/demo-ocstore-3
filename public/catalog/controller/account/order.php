<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountOrder extends ControllerStartupTezarius
{
    public function __construct($registry)
    {
        parent::__construct($registry);
        if( !$this->customer->isLogged() )
        {
            $this->session->data['redirect'] = $this->url->link('account/order', '', true);
            $this->response->redirect($this->url->link('account/login', '', true));
        }
    }

    public function index()
    {
		$this->load->language('account/order');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->document->addScript('catalog/view/javascript/orders.js');

        $page = (int) rcv('page',1);

		$url = '';
		if( $page ) $url .= "&page={$page}";


		$this->breadcrumbs = [[
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
        ],[
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
        ],[
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order', $url, true)
        ]];


		$this->data['orders'] = [];
		$this->load->model('account/order');
		///$order_total = $this->model_account_order->getTotalOrders();
		///$results = $this->model_account_order->getOrders(($page - 1) * 10, 10);

        $filter = rcv('filter','current');
        $this->data['filter_code'] = $filter;
        $this->data['filter_name'] = $this->language->get('filter_name_'.$filter);

        $results = [];
        if( buttonPressed('filter-set') )
        {
            ///pp($_POST);
            switch( $filter )
            {
                case 'period':{
                    $period = rcv('period',[]);
                    $dd1 = get($period,'dd1');
                    $dd2 = get($period,'dd2');
                    $this->data['period'] = [];
                    $this->data['period']['dd1'] = $dd1;
                    $this->data['period']['dd2'] = $dd2;
                    $results = $this->model_account_order->getOrdersByData($dd1,$dd2);
                    ///dd([$filter,$period,$results]);
                }break;
                case 'number':{
                    $number = rcv('number');
                    $this->data['number'] = $number;
                    $results = $this->model_account_order->getOrdersByNumber($number);
                }break;
                case 'search':{
                    $search = rcv('search');
                    $this->data['search'] = $search;
                    $results = $this->model_account_order->getOrdersBySearch($search);
                }break;
            }
        }
		if( empty($results) ) $results = $this->model_account_order->getOrderList();
		///dd($results);
        $order_total = count($results);
		foreach( $results as $result )
		{
			///$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			///$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

            //*/
			$this->data['orders'][] = array(
				'id'         => $result['id'],
				'bgColor'    => $result['color_web'],
				'fnColor'    => $result['color_font_web'],
				'order_id'   => $result['doc_number'],
				'part'       => $result['art_code'] . ' / ' . $result['art_brand'],
				'name'       => $result['name'],
				'status'     => get($result,'StateName'),
				'status_code'  => get($result,'StateCode'),
				'last_note'    => get($result,'last_note'),
				'date_added'   => date($this->language->get('date_format_short'),strtotime(get($result,'doc_date'))), /// || {{ order.date_added | date("m.d.Y H:i") }}
				///'products'  => ($product_total + $voucher_total), => qty
				'qty'          => get($result,'qty')*1,
				///'total'     => $this->currency->format($result['total'],get($result,'currency_code'),get($result,'currency_value')),
				'price'        => get($result,'cost',0)*1,
				'total'        => get($result,'total',0)*1,
				'view'         => $this->url->link('account/order/info', 'order_id=' . get($result,'id'), true),
			);
            //*/
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/order', 'page={page}', true);

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

		$this->data['continue'] = $this->url->link('account/account', '', true);

		$this->render('account/order/list/grid');
	}
	public function history()
    {
        $this->load->language('account/order');
        $json = [];
        $json['status'] = 0;
        //
        if( $id = (int) rcv('id') )
        {
            $this->load->model('account/order');
            $res = $this->model_account_order->getOrderHistory($id);
            if( get($res,'isError') )
            {
                $message = [
                    'type' => 'error',
                    'text' => get($res,'mess')
                ];
                $this->flashers[] = $message;
                $json['message'] = $message;
            }
            else
            {
                $json['status'] = 1;
                $json['rows'] = $res;
                $json['html'] = $this->load->view('account/order/history',['rows'=>$res]);
            }
        }
        else
        {
            $message = [
                'type' => 'error',
                'text' => $this->language->get('text_error')
            ];
            $this->flashers[] = $message;
            $json['message'] = $message;
        }

        $json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);
        //
        $this->jEcho($json);
    }
    public function withdraw()
    {
        $this->load->language('account/order');

        $json = [];

        $id = (int) rcv('id');
        $note = rcv('note');

        $this->load->model('account/order');
        $res = $this->model_account_order->reqOrderWithdraw($id,$note);
        if( get($res,'isError') )
        {
            $json['type'] = 'error';
            $json['text'] = get($res,'mess');
        }
        else
        {
            $json['type'] = 'success';
            $json['text'] = get($res,'mess',$this->language->get('order_withdraw_success'),TRUE);
        }

        $this->jEcho($json);
    }
    public function delivery()
    {
        $this->load->language('account/order');

        $json = [];

        $id = (int) rcv('id');
        $note = rcv('note');

        $this->load->model('account/order');
        $res = $this->model_account_order->getOrderDelivery($id,$note);
        if( get($res,'isError') )
        {
            $json['type'] = 'error';
            $json['text'] = get($res,'mess');
        }
        else
        {
            $json['type'] = 'success';
            $json['text'] = get($res,'mess',$this->language->get('order_delivery_success'),TRUE);
        }

        $this->jEcho($json);
    }
    public function towork()
    {
        $this->load->language('account/order');

        $json = [];

        $id = (int) rcv('id');
        $note = rcv('note');

        $this->load->model('account/order');
        $res = $this->model_account_order->getOrderToWork($id,$note);
        if( get($res,'isError') )
        {
            $json['type'] = 'error';
            $json['text'] = get($res,'mess');
        }
        else
        {
            $json['type'] = 'success';
            $json['text'] = get($res,'mess',$this->language->get('order_towork_success'),TRUE);
        }

        $this->jEcho($json);
    }

	public function info() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->data['breadcrumbs'] = array();

			$this->data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$this->data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$this->data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', $url, true)
			);

			$this->data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url, true)
			);

			if (isset($this->session->data['error'])) {
				$this->data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$this->data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$this->data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$this->data['success'] = '';
			}

			if ($order_info['invoice_no']) {
				$this->data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$this->data['invoice_no'] = '';
			}

			$this->data['order_id'] = $this->request->get['order_id'];
			$this->data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$this->data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->data['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$this->data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$this->data['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$this->data['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);

			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
				} else {
					$reorder = '';
				}

				$this->data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => $reorder,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
				);
			}

			// Voucher
			$this->data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$this->data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$this->data['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			foreach ($totals as $total) {
				$this->data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
				);
			}

			$this->data['comment'] = nl2br($order_info['comment']);

			// History
			$this->data['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

			foreach ($results as $result) {
				$this->data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}

			$this->data['continue'] = $this->url->link('account/order', '', true);

			$this->data['column_left'] = $this->load->controller('common/column_left');
			$this->data['column_right'] = $this->load->controller('common/column_right');
			$this->data['content_top'] = $this->load->controller('common/content_top');
			$this->data['content_bottom'] = $this->load->controller('common/content_bottom');
			$this->data['footer'] = $this->load->controller('common/footer');
			$this->data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/order_info', $this->data));
		} else {
			return new Action('error/not_found');
		}
	}

	public function reorder() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			if (isset($this->request->get['order_product_id'])) {
				$order_product_id = $this->request->get['order_product_id'];
			} else {
				$order_product_id = 0;
			}

			$order_product_info = $this->model_account_order->getOrderProduct($order_id, $order_product_id);

			if ($order_product_info) {
				$this->load->model('catalog/product');

				$product_info = $this->model_catalog_product->getProduct($order_product_info['product_id']);

				if ($product_info) {
					$option_data = array();

					$order_options = $this->model_account_order->getOrderOptions($order_product_info['order_id'], $order_product_id);

					foreach ($order_options as $order_option) {
						if ($order_option['type'] == 'select' || $order_option['type'] == 'radio' || $order_option['type'] == 'image') {
							$option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'checkbox') {
							$option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
							$option_data[$order_option['product_option_id']] = $order_option['value'];
						} elseif ($order_option['type'] == 'file') {
							$option_data[$order_option['product_option_id']] = $this->encryption->encrypt($this->config->get('config_encryption'), $order_option['value']);
						}
					}

					$this->cart->add($order_product_info['product_id'], $order_product_info['quantity'], $option_data);

					$this->session->data['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_info['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
				} else {
					$this->session->data['error'] = sprintf($this->language->get('error_reorder'), $order_product_info['name']);
				}
			}
		}

		$this->response->redirect($this->url->link('account/order/info', 'order_id=' . $order_id));
	}
}