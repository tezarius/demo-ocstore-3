<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountLogout extends ControllerStartupTezarius
{
	public function index()
    {
		if ($this->customer->isLogged())
		{
			$this->customer->logout();
			$this->response->redirect($this->url->link('account/logout', '', true));
		}

		$this->load->language('account/logout');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

		$this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        ],[
            'text' => $this->language->get('text_logout'),
            'href' => $this->url->link('account/logout', '', true)
        ]];

		$this->data['continue'] = $this->url->link('common/home');

        $this->renderCommon();
		$this->response->setOutput($this->load->view('common/success', $this->data));
	}
}
