<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountPassword extends ControllerStartupTezarius
{
	private $error = array();
	public function index()
    {
		if( !$this->customer->isLogged() )
		{
			$this->session->data['redirect'] = $this->url->link('account/password', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/password');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

		if( get($this->request->server,'REQUEST_METHOD') == 'POST' && $this->validate())
		{
			$this->load->model('account/customer');

            $CounterpartsID = $this->customer->getId();
            $login = rcv('login',$this->customer->getLogin());
            $passwd = rcv('password',$this->customer->getPasswd());

            if( $login==$this->customer->getLogin() && $passwd==$this->customer->getPasswd() )
            {
                $this->session->data['flashers'][] = [
                    'type' => 'success',
                    'text' => $this->language->get('text_without_changes')
                ];
                $this->response->redirect($this->url->link('account/account', '', true));
            }

			$res = $this->model_account_customer->editPassword($CounterpartsID,$login,$passwd);
			///dd($res);
			if( get($res,'isError') )
			{
			    $this->flashers[] = [
                    'type' => 'error',
                    'text' => get($res,'mess')
                ];
                ///dd($this->flashers);
            }
			else
			{
                $this->customer->setHashAuth($login,$passwd);
                ///dd([$login,$passwd,$hashAuth]);
                $this->session->data['flashers'][] = [
                    'type' => 'success',
                    'text' => $this->language->get('text_success')
                ];
                $this->response->redirect($this->url->link('account/account', '', true));
            }
		}

		$this->breadcrumbs = [[
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        ],[
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/password', '', true)
        ]];


		$this->data['error_login'] = get($this->error,'login');
		$this->data['error_password'] = get($this->error,'password');
        $this->data['error_confirm'] = get($this->error,'confirm');
        $this->data['error_oldpass'] = get($this->error,'oldpass');

        $this->data['login'] = rcv('login');
        $this->data['password'] = rcv('password');
        $this->data['confirm'] = rcv('confirm');
        $this->data['oldpass'] = rcv('oldpass');

        $this->data['action'] = $this->url->link('account/password', '', true);
        $this->data['back']   = $this->url->link('account/account', '', true);

        ///dd([$this->customer->getHashAuth(),$this->customer->getData(),$this->customer,$this->session->data]);

        $this->renderCommon();
		$this->response->setOutput($this->load->view('account/password', $this->data));
	}

	protected function validate()
    {
        $login = rcv('login');
        $login_l = utf8_strlen($login);
        if( $login_l > 32 ) $this->error['login'] = $this->language->get('error_login');

        if( rcv('oldpass') != $this->customer->getPasswd() )
        {
            ///$this->error['oldpass'] = $this->language->get('error_oldpass')." | ".rcv('oldpass')." | ".$this->customer->getPasswd()." | ".$this->customer->getHashAuth();
        }

        $password = html_entity_decode(rcv('password'), ENT_QUOTES, 'UTF-8');
        $password_l = utf8_strlen($password);
        if( $password_l>0 && ($password_l < 8 || $password_l > 40) ) $this->error['password'] = $this->language->get('error_password');

        if( rcv('confirm') != rcv('password') )
        {
            $this->error['confirm'] = $this->language->get('error_confirm');
        }

		return !$this->error;
	}
}