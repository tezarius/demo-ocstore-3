<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountAccount extends ControllerStartupTezarius
{
	public function index()
    {
        ///dd([$this->customer->isLogged(),$this->customer,$this->session->data]);
		if( !$this->customer->isLogged() )
        {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}
		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->setRobots('noindex,follow');

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        ]];


		$this->data['edit']     = $this->url->link('account/edit', '', true);
		$this->data['password'] = $this->url->link('account/password', '', true);
		$this->data['address']  = $this->url->link('account/address', '', true);

		/*/
		$this->data['credit_cards'] = [];
		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');
		foreach( $files as $file )
        {
			$code = basename($file, '.php');
			
			if( $this->config->get('payment_'.$code.'_status') && $this->config->get('payment_'.$code.'_card') )
            {
				$this->load->language('extension/credit_card/' . $code, 'extension');

				$this->data['credit_cards'][] = [
					'name' => $this->language->get('extension')->get('heading_title'),
					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				];
			}
		}
		//*/
		
		$this->data['wishlist'] = $this->url->link('account/wishlist');
		$this->data['garage']   = $this->url->link('account/garage');

		$this->data['order']    = $this->url->link('account/order', '', true);
		$this->data['download'] = $this->url->link('account/download', '', true);


        $this->data['reward'] = ( $this->config->get('total_reward_status') )
            ? $this->url->link('account/reward', '', true)
            : ''
        ;

		
		$this->data['return']      = $this->url->link('account/return', '', true);
		$this->data['transaction'] = $this->url->link('account/transaction', '', true);
		$this->data['newsletter']  = $this->url->link('account/newsletter', '', true);
		$this->data['recurring']   = $this->url->link('account/recurring', '', true);
		

		$this->load->model('account/customer');
		$affiliate_info = $this->model_account_customer->getAffiliate($this->customer->getId());
		if( !$affiliate_info )
        {
			$this->data['affiliate'] = $this->url->link('account/affiliate/add', '', true);
            $this->data['tracking']  = '';
        }
        else
        {
            $this->data['affiliate'] = $this->url->link('account/affiliate/edit', '', true);
            $this->data['tracking']  = $this->url->link('account/tracking', '', true);
        }

        $this->render('account/account');
	}

	public function country()
    {
		$json = [];

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		if( $country_info )
        {
			$this->load->model('localisation/zone');

			$json = [
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			];
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
