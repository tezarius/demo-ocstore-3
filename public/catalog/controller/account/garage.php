<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerAccountGarage extends ControllerStartupTezarius
{
    private $error = [];
    public function __construct($registry)
    {
        parent::__construct($registry);
        if( !$this->customer->isLogged() )
        {
            $this->session->data['redirect'] = $this->url->link('account/garage/add', '', true);
            $this->response->redirect($this->url->link('account/login', '', true));
        }
    }

    public function index()
    {
        $this->load->language('account/garage');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->setRobots('noindex,follow');
        $this->document->addScript('catalog/view/javascript/garage.js');

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_garage'),
            'href' => $this->url->link('garage', '', true)
        ]];

        $this->load->model('account/garage');
        $res = $this->model_account_garage->vehicles();
        ///pp($res);
        $this->data['cars'] = $res;
        ///$this->data['blankURI_AfterMarket'] = $this->url->link('catalog/aftermarket/tree',"type=TTT&mark=MRM&model=MDL&car=CCC",true);
        $this->data['blankURI_AfterMarket'] = $this->url->link('catalog/aftermarket/tree',"type=TTT&car=CCC&garage=1",true);

        $this->render('account/garage/list');
    }
	public function add()
    {
		$this->load->language('account/garage');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->setRobots('noindex,follow');
        $this->document->addScript('catalog/view/javascript/garage.js');
        ///dd($this->data);

        $this->load->model('catalog/aftermarket');
        $this->data['types'] = $this->model_catalog_aftermarket->types();

        if( buttonPressed('btnGarageAdd') && $this->validate() )
        {
            ///dd($this->data);
            $type   = rcv('type');
            $vehicle = (int) rcv('vehicle');
            $auto   = rcv('auto');
            $engine = rcv('engine');
            $year   = (int) rcv('year');
            $vin    = rcv('vin');
            $note   = rcv('note');
            ///dd($this->request->post);

            $this->load->model('account/garage');
            $res = $this->model_account_garage->add($auto,$engine,$vin,$year,$note,$vehicle,$type);
            ///dd($res);
            if( get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get($res,'mess'),
                ];
            }
            else
            {
                $this->session->data['flashers'][] = [
                    'type' => 'success',
                    'text' => 'Автомобиль успешно добавлен.',
                ];
                $this->response->redirect($this->url->link('account/garage'));
            }
        }

		$this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_garage'),
            'href' => $this->url->link('garage', '', true)
        ],[
            'text' => $this->language->get('text_garage_add'),
            'href' => $this->url->link('garage/add', '', true)
        ]];

        $this->data['error_auto'] = get($this->error,'auto');
        $this->data['error_vin']  = get($this->error,'vin');
        $this->data['error_year'] = get($this->error,'year');

        $this->data['auto'] = rcv('auto');
        $this->data['vin']  = rcv('vin');
        $this->data['year'] = rcv('year');
        $this->data['year_max'] = date('Y');
        $this->data['year_min'] = 1950;

        // Captcha
        $this->data['captcha'] = ( $this->config->get('captcha_'.$this->config->get('config_captcha').'_status') && in_array('garage-add',(array)$this->config->get('config_captcha_page')) )
            ? $this->load->controller('extension/captcha/'.$this->config->get('config_captcha'), $this->error)
            :''
        ;

        $this->data['action'] = $this->url->link('account/garage/add', '', true);

        $this->render('account/garage/add');
	}
	public function del()
    {
        $id = rcv('id');
        $note = rcv('note');
        $message = [
            'type' => 'success',
            'text' => "Успешно [{$id}] {$note}",
        ];
        $this->jEcho($message);
    }

    private function validate()
    {
        $auto = rcv('auto');
        $auto_l = utf8_strlen($auto);
        if( $auto_l < 3 ) $this->error['auto'] = $this->language->get('error_auto');

        $vin = rcv('vin');
        $vin_l = utf8_strlen($vin);
        if( $vin_l < 5 || $vin_l > 17 ) $this->error['vin'] = $this->language->get('error_vin');

        $year = (int) rcv('year');
        if( $year < 1950 || $year > date('Y') ) $this->error['year'] = $this->language->get('error_year');

        // Captcha
        if( $this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('register', (array)$this->config->get('config_captcha_page')) )
        {
            $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');
            if( $captcha ) $this->error['captcha'] = $captcha;
        }

        return !$this->error;
    }
}
