<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCatalogAftermarket extends ControllerStartupTezarius
{
    private $types;
    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->language('catalog/aftermarket');
        $this->types = [
            'P' => 'Легковые',
            'C' => 'Грузовые',
            'M' => 'Мототехника',
        ];
    }

    public function index()
    {
        $this->types();
    }
    public function types()
    {
        $this->load->model('catalog/aftermarket');
        $types = $this->model_catalog_aftermarket->types();
        if( isAjax() )
        {
            $json = [];
            $json['types'] = $types;
            $this->jEcho($json);
        }

        $this->document->addScript('catalog/view/libraries/@iconfu/svg-inject/dist/svg-inject.min.js');

        $this->data['types'] = $types;

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_aftermarket_types'),
            'href' => $this->url->link('catalog/aftermarket', '', true)
        ]];

        $this->render('catalog/aftermarket/types');
    }
    public function marks()
    {
        $type = rcv('type');
        $this->load->model('catalog/aftermarket');
        $marks = $this->model_catalog_aftermarket->marks($type);
        if( isAjax() )
        {
            $json = [];
            $json['marks'] = $marks;
            $this->jEcho($json);
        }
        $this->data['marks'] = $marks;
        $this->data['blankURI'] = $this->url->link('catalog/aftermarket/models',"type={$type}&mark=",true);

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => get($this->types,$type),
            'href' => $this->url->link('catalog/aftermarket','',true)
        ],[
            'text' => $this->language->get('text_aftermarket_marks'),
            'href' => $this->url->link('catalog/aftermarket/marks',"type={$type}",true)
        ]];

        $this->render('catalog/aftermarket/marks');
    }
    public function models()
    {
        $type = rcv('type');
        $mark = rcv('mark');
        $this->load->model('catalog/aftermarket');

        $models = $this->model_catalog_aftermarket->models($type,$mark);
        if( isAjax() )
        {
            $json = [];
            $json['models'] = $models;
            $this->jEcho($json);
        }

        $_mark = $this->model_catalog_aftermarket->mark($type,$mark);
        $markName = get($_mark,'manuName');

        $this->data['models'] = $models;
        $this->data['blankURI'] = $this->url->link('catalog/aftermarket/vehicles',"type={$type}&mark={$mark}&model=",true);

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => get($this->types,$type),
            'href' => $this->url->link('catalog/aftermarket','',true)
        ],[
            'text' => "$markName",
            'href' => $this->url->link('catalog/aftermarket/marks',"type={$type}",true)
        ],[
            'text' => $this->language->get('text_aftermarket_models'),
            'href' => $this->url->link('catalog/aftermarket/models',"type={$type}&mark={$mark}",true)
        ]];

        $this->render('catalog/aftermarket/models');
    }
    public function vehicles()
    {
        $type = rcv('type');
        $mark = rcv('mark');
        $model = rcv('model');
        $this->load->model('catalog/aftermarket');
        $vehicles = $this->model_catalog_aftermarket->vehicles($type,$mark,$model);
        if( isAjax() )
        {
            $json = [];
            $json['vehicles'] = $vehicles;
            $this->jEcho($json);
        }
        $this->data['vehicles'] = $vehicles;
        $this->data['blankURI'] = $this->url->link('catalog/aftermarket/tree',"type={$type}&mark={$mark}&model={$model}&car=",true);

        $_model = $this->model_catalog_aftermarket->model($type,$model);
        $markName = get($_model,'manuName');
        $modelName = get($_model,'modelName');

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => get($this->types,$type),
            'href' => $this->url->link('catalog/aftermarket','',true)
        ],[
            'text' => $markName,
            'href' => $this->url->link('catalog/aftermarket/marks',"type={$type}",true)
        ],[
            'text' => $modelName,
            'href' => $this->url->link('catalog/aftermarket/models',"type={$type}&mark={$mark}",true)
        ],[
            'text' => $this->language->get('text_aftermarket_vehicles'),
            'href' => $this->url->link('catalog/aftermarket/vehicles',"type={$type}&mark={$mark}&model={$model}",true)
        ]];

        $this->render('catalog/aftermarket/vehicles');
    }
    public function tree()
    {
        $type = rcv('type');
        $mark = rcv('mark');
        $model = rcv('model');
        $car = rcv('car');

        $this->load->model('catalog/aftermarket');
        $res = $this->model_catalog_aftermarket->tree($type,$car);
        /*/ /// parentNodeId // assemblyGroupNodeId
        usort($tree, function($a,$b) {
            $a = $a['parentNodeId'];
            $b = $b['parentNodeId'];
            return ($a == $b) ? 0 : ( ($a < $b) ? -1 : 1 );
        });
        //*/
        $tree = buildTree($res, 'parentNodeId', 'assemblyGroupNodeId');
        if( isAjax() )
        {
            $json = [];
            $json['tree'] = ( rcv('simple') ) ? $res : $tree;
            $this->jEcho($json);
        }

        $_vehicle = $this->model_catalog_aftermarket->vehicle($type,$car);
        ///dd($_vehicle);
        $this->data['heading_title'] = get($_vehicle,'fulldescription');

        $garage = rcv('garage');
        if( $garage )
        {
            $this->breadcrumbs = [[
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            ],[
                'text' => $this->language->get('text_garage_return'),
                'href' => $this->url->link('account/garage','',true)
            ],[
                'text' => $this->language->get('text_aftermarket_tree'),
                'href' => $this->url->link('catalog/aftermarket/tree',"type={$type}&car={$car}&garage={$garage}",true)
            ]];
            $blankURI = $this->url->link('catalog/aftermarket/details',"type={$type}&car={$car}&garage={$garage}&node=",true);
        }
        else
        {
            $markName = get($_vehicle,'manuName');
            $modelName = get($_vehicle,'modelName');
            $vehicleName = get($_vehicle,'description');

            $this->breadcrumbs = [[
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            ],[
                'text' => get($this->types,$type),
                'href' => $this->url->link('catalog/aftermarket','',true)
            ],[
                'text' => $markName,
                'href' => $this->url->link('catalog/aftermarket/marks',"type={$type}",true)
            ],[
                'text' => $modelName,
                'href' => $this->url->link('catalog/aftermarket/models',"type={$type}&mark={$mark}",true)
            ],[
                'text' => $vehicleName,
                'href' => $this->url->link('catalog/aftermarket/vehicles',"type={$type}&mark={$mark}&model={$model}",true)
            ],[
                'text' => $this->language->get('text_aftermarket_tree'),
                'href' => $this->url->link('catalog/aftermarket/tree',"type={$type}&mark={$mark}&model={$model}&car={$car}",true)
            ]];
            $blankURI = $this->url->link('catalog/aftermarket/details',"type={$type}&mark={$mark}&model={$model}&car={$car}&node=",true);
        }

        $blankURI = html_entity_decode($blankURI);
        $treeHTML = $this->buildTreeHTML($tree,[
            'uri'=>$blankURI,
            'nid'=>'assemblyGroupNodeId',
            ///
            'type'=>$type,
            'car'=>$car,
        ]);
        ///$treeHTML = html_entity_decode($treeHTML);
        ///die($treeHTML);
        $this->data['treeHTML'] = $treeHTML;
        $this->render('catalog/aftermarket/tree');
    }
    public function details()
    {
        $type = rcv('type');
        $mark = rcv('mark');
        $model = rcv('model');
        $car  = rcv('car');
        $node = rcv('node');

        $this->load->model('catalog/aftermarket');

        $_vehicle = $this->model_catalog_aftermarket->vehicle($type,$car);
        $vehicle = get($_vehicle,'fulldescription');

        /// 32532;P
        $this->load->model('price');
        $code = "$node;$type";
        $res = $this->model_price->searchByAuto($car,$code,$vehicle);
        ///dd($res);
        $offers = [];
        if( $IDSearchCode = get($res,'IDSearchCode'))
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $StockID = $this->customer->getStockIDGlobal();
            $PriceLevelID = $this->customer->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'ByCarNode';
            $SearchValue = $IDSearchCode;
            $SortData = '';
            ///pp([$recentlyRequest,$StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData]);
            $offers = $this->model_price->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( get(get($offers,0,[]),'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get(get($offers,0,[]),'mess'),
                ];
                $offers = [];
            }
            else if( !empty($offers) )
            {
                ///dd(['!empty',$offers]);
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => get($res,'mess'),
            ];
        }

        $res = $this->model_catalog_aftermarket->details($type,$car,$node);
        ///dd($res);
        ///$this->data['details'] = $res;
        ///$this->data['blankURI'] = $this->url->link('price/offers',"number=NNNN&brand=BBBB",true);

        $detailsCatalog = $this->load->view('catalog/aftermarket/details/catalog',[
            'details' => $res,
            'blankURI' => $this->url->link('price/offers',"number=NNNN&brand=BBBB",true),
        ]);
        $detailsOffers = $this->load->view('catalog/aftermarket/details/offers',[
            'offers' => $offers,
            'column_left' => TRUE,
        ]);
        if( isAjax() )
        {
            if( rcv('inner') )
            {
                jEcho([
                    'inner' => 1,
                    'details' => $detailsOffers.$detailsCatalog,
                    ///'detailsCatalog' => $detailsCatalog,
                    ///'detailsOffers' => $detailsOffers
                ]);
            }
            else
            {
                jEcho(['detailsCatalog'=>$res,'detailsOffers'=>$res,]);
            }
        }
        $this->data['detailsCatalog'] = $detailsCatalog;
        $this->data['detailsOffers'] = $detailsOffers;


        $garage = rcv('garage');
        if( $garage )
        {
            $this->breadcrumbs = [[
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            ],[
                'text' => $this->language->get('text_garage_return'),
                'href' => $this->url->link('account/garage','',true)
            ],[
                'text' => $this->language->get('text_aftermarket_tree'),
                'href' => $this->url->link('catalog/aftermarket/tree',"type={$type}&car={$car}&garage={$garage}",true)
            ],[
                'text' => $this->language->get('text_aftermarket_details'),
                'href' => $this->url->link('catalog/aftermarket/details',"type={$type}&car={$car}&garage={$garage}&node={$node}",true)
            ]];
        }
        else
        {
            $this->breadcrumbs = [[
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            ],[
                'text' => get($this->types,$type),
                'href' => $this->url->link('catalog/aftermarket','',true)
            ],[
                'text' => $this->language->get('text_aftermarket_marks'),
                'href' => $this->url->link('catalog/aftermarket/marks',"type={$type}",true)
            ],[
                'text' => $this->language->get('text_aftermarket_models'),
                'href' => $this->url->link('catalog/aftermarket/models',"type={$type}&mark={$mark}",true)
            ],[
                'text' => $this->language->get('text_aftermarket_vehicles'),
                'href' => $this->url->link('catalog/aftermarket/vehicles',"type={$type}&mark={$mark}&model={$model}",true)
            ],[
                'text' => $this->language->get('text_aftermarket_tree'),
                'href' => $this->url->link('catalog/aftermarket/tree',"type={$type}&mark={$mark}&model={$model}&car={$car}",true)
            ],[
                'text' => $this->language->get('text_aftermarket_details'),
                'href' => $this->url->link('catalog/aftermarket/details',"type={$type}&mark={$mark}&model={$model}&car={$car}&node={$node}",true)
            ]];
        }

        $this->render('catalog/aftermarket/details');
    }

    public function buildTreeHTML($items,$params=[])
    {
        $this->document->addScript('catalog/view/javascript/tree.js');
        $this->document->addStyle('catalog/view/stylesheet/tree.css');

        $dom = new \DOMDocument();

        $ul  = $dom->appendChild( new \DOMElement( "ul" ) );
        $ul->setAttribute( "id", "tz-tree" );
        ///$ul->setAttribute( "class", "ul-treefree ul-dropfree" );

        $counter = 0; /// Вместо ID
        $placement = function($items,$root,$params,&$counter) use (&$placement)
        {
            ///++$counter;
            foreach( $items AS $f )
            {
                ///++$counter;

                $li = $root->appendChild( new \DOMElement( "li" ) );

                $text = get($f,'assemblyGroupName');

                if( $children = get($f,'children') )
                {
                    $dID = ++$counter;
                    ///$dID = md5($text);

                    /// Название
                    $a = $li->appendChild( new \DOMElement( "a" ) );
                    $a->appendChild( new \DOMText( $text ) );
                    $a->setAttribute("id","a{$dID}");
                    $a->setAttribute("data-group","$dID");
                    ///$a->setAttribute("onClick","yulsun.catalogs.tree('$dID',event)");

                    /// Рекурсия
                    $ul = $li->appendChild( new \DOMElement( "ul" ) );
                    $ul->setAttribute('id',$dID);
                    $ul->setAttribute('class',"subgroup subgroup$dID");

                    ///++$counter;
                    $placement($children,$ul,$params,$counter);
                }
                else
                {
                    /// Название
                    $a = $li->appendChild( new \DOMElement( "a" ) );
                    $a->appendChild( new \DOMText( $text ) );

                    /// Действие
                    $node = get($f,get($params,'nid'));
                    $uri = get($params,'uri').$node;
                    $a->setAttribute("href","$uri");

                    /// Накидываем
                    $a->setAttribute("class",'pricing');
                    $a->setAttribute("data-type",get($params,'type'));
                    $a->setAttribute("data-car",get($params,'car'));
                    $a->setAttribute("data-node",$node);
                }
            }
        };
        $placement($items,$ul,$params,$counter);

        $html = $dom->saveHTML();
        return $html;
    }
}
