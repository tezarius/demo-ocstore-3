<?php

class ControllerExtensionPaymentStoreCash extends Controller
{
    public function index()
    {
        $data['continue'] = $this->url->link('checkout/success');
        return $this->load->view('extension/payment/store_cash', $data);
    }

    public function confirm()
    {
        if( $this->session->data['payment_method']['code'] == 'store_cash' )
        {
            $this->load->model('checkout/order');
            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_store_cash_order_status_id'));
        }
    }
}