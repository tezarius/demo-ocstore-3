<?php


class ControllerExtensionTotalCancel extends ControllerStartupTezarius
{
	public function index()
    {
        if( get($this->session->data,'reward') || get($this->session->data,'coupon') )
        {
            $this->load->language('extension/total/cancel');

            $data['heading_title'] = $this->language->get('heading_title');

            return $this->load->view('extension/total/cancel', $data);
        }

        return '';
	}

	public function all()
    {
        $this->load->language('extension/total/cancel');
        $json = [];
        ///
        $ShopID = (int) $this->getStockID();
        $TabID = $this->getBasketTabActive();
        $UserID = (int) $this->customer->getId();
        $PriceLID = (int) $this->customer->getPriceLID();
        ///
        $this->load->model('checkout/cart');
        $res = $this->model_checkout_cart->removeBasketDiscounts($ShopID,$TabID,$UserID,$PriceLID);
        ///
        ///$json['$res'] = $res;
        if( get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
            $status = 0;
            $redirect = '';
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => $this->language->get('text_success')
            ];
            $status = 1;
            $redirect = $this->url->link('checkout/cart');

            unset($this->session->data['reward']);
            unset($this->session->data['coupon']);
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;
        ///
        ///$this->flashers[] = $message;
        ///$json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);
        ///
        $this->jEcho($json);
    }
}
