<?php


class ControllerExtensionTotalReward extends ControllerStartupTezarius
{
    private $field = 'BonusAccess';
	public function index()
    {
		$points = $this->customer->getRewardPoints();

		$points_total = 0;
		foreach( $this->cart->getProducts() AS $product )
            $points_total += ( get($product,$this->field) )
                ? $product[$this->field]
                : $product['total'] - $product['qty'] * 1
            ;

        ///$points = 999;
        ///$points_total = 99999999;
		if( $points && $points_total && (int)get($this->session->data,'reward')!==(int)$points && $this->config->get('total_reward_status') )
		{
			$this->load->language('extension/total/reward');

			$data['heading_title'] = sprintf($this->language->get('heading_title'), $points);
			$data['entry_reward'] = sprintf($this->language->get('entry_reward'), $points_total);

            $data['reward'] = get($this->session->data,'reward',$points);

			return $this->load->view('extension/total/reward', $data);
		}

		return '';
	}

	public function reward()
    {
        $this->load->language('extension/total/reward');
		$json = [];
		///
        $ShopID = (int) $this->getStockID();
        $TabID = $this->getBasketTabActive();
        $UserID = (int) $this->customer->getId();
        $PriceLID = (int) $this->customer->getPriceLID();
        $CardID = (int) $this->customer->getDiscountCardID();
        ///
        $this->load->model('checkout/cart');
        $res = $this->model_checkout_cart->applyRewardPoints($CardID,$ShopID,$TabID,$UserID,$PriceLID);
        ///
        ///$json['$res'] = $res;
        if( get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
            $status = 0;
            $redirect = '';
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => $this->language->get('text_success')
            ];
            $status = 1;
            $redirect = $this->url->link('checkout/cart');

            $reward = rcv('reward');
            $this->session->data['reward'] = abs($reward);
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;
        ///
        ///$this->flashers[] = $message;
        ///$json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);
        ///
		$this->jEcho($json);
	}
}
