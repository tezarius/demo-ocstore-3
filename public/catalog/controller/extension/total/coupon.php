<?php


class ControllerExtensionTotalCoupon extends ControllerStartupTezarius
{
	public function index()
    {
        $view = '';
		if( !get($this->session->data,'coupon') && $this->config->get('total_coupon_status') )
		{
			$this->load->language('extension/total/coupon');

            $data['coupon'] = get($this->session->data,'coupon');

            $view = $this->load->view('extension/total/coupon', $data);
		}
		return $view;
	}

	public function coupon()
    {
        $this->load->language('extension/total/coupon');
        $json = [];
        ///
        $PromoName = rcv('coupon');
        $ShopID = (int) $this->getStockID();
        $TabID = $this->getBasketTabActive();
        $UserID = (int) $this->customer->getId();
        $PriceLID = (int) $this->customer->getPriceLID();
        $CardID = (int) $this->customer->getDiscountCardID();
        ///
        $this->load->model('checkout/cart');
        $res = $this->model_checkout_cart->applyCoupon($PromoName,$CardID,$ShopID,$TabID,$UserID,$PriceLID);
        ///
        ///$json['$res'] = $res;
        if( get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
            $status = 0;
            $redirect = '';
            unset($this->session->data['coupon']);
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => $this->language->get('text_success')
            ];
            $status = 1;
            $redirect = $this->url->link('checkout/cart');
            $this->session->data['coupon'] = $PromoName;
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;
        ///
        ///$this->flashers[] = $message;
        ///$json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);
        ///
        $this->jEcho($json);
    }
	public function coupon3()
    {
		$this->load->language('extension/total/coupon');

		$json = array();

		$this->load->model('extension/total/coupon');

        $coupon = rcv('coupon');

		$coupon_info = $this->model_extension_total_coupon->getCoupon($coupon);

		if (empty($this->request->post['coupon'])) {
			$json['error'] = $this->language->get('error_empty');

			unset($this->session->data['coupon']);
		} elseif ($coupon_info) {
			$this->session->data['coupon'] = $this->request->post['coupon'];

			$this->session->data['success'] = $this->language->get('text_success');

			$json['redirect'] = $this->url->link('checkout/cart');
		} else {
			$json['error'] = $this->language->get('error_coupon');
		}

		$this->jEcho($json);
	}
}
