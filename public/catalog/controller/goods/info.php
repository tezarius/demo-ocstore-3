<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerGoodsInfo extends ControllerStartupTezarius
{
    public function index()
    {
        $this->load->language('goods/info');

        $hash = rcv('hash');
        $arr = arrDecryption($hash);

        $artbrID = get($arr,'artbrID');
        $brandID = get($arr,'brandID');
        $brand   = get($arr,'brand');
        $code    = get($arr,'code');
        $GoodsID = get($arr,'goodsID');
        $offer   = get($arr,'offer');
        ///pp([$artbrID,$brandID,$brand,$code,$GoodsID]);

        $goodsLink = $this->url->link('price/offers',"hash={$hash}");
        $this->data['offersLink'] = $goodsLink;

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => "$brand / $code",
            'href' => $goodsLink
        ]];


        $this->load->model('goods/info');
        $product_info = $this->model_goods_info->getAll($artbrID,$brandID,$brand,$code,$GoodsID);
        ///pp($product_info);

        $title = get($product_info,'meta_title',get($product_info,'name'));
        $this->document->setTitle($title);

        $heading = get($product_info,'meta_h1',get($product_info,'name'));
        $this->data['heading_title'] = $heading;
        $this->data['review_status'] = $this->config->get('config_review_status');
        $this->data['review_guest']  = ( $this->config->get('config_review_guest') || $this->customer->isLogged() );
        $this->data['tab_review']    = sprintf($this->language->get('tab_review'), (int)get($product_info,'reviews'));

        $this->document->setDescription(get($product_info,'meta_description'));
        $this->document->setKeywords(get($product_info,'meta_keyword'));
        $this->document->addLink($goodsLink,'canonical');

        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $this->document->addScript('catalog/view/javascript/items.js');


        $this->data['image'] = get($product_info,'image');
        $this->data['images'] = get($product_info,'images');

        $this->data['price'] = get($product_info,'price');
        $this->data['special'] = get($product_info,'special');

        $this->data['settings_global'] = get($product_info,'settings_global');
        $this->data['settings_local'] = get($product_info,'settings_local');

        $this->data['offer'] = $offer;
        $this->data['text_minimum'] = sprintf($this->language->get('text_minimum'),get($offer,'step'));
        $this->data['basketTabID'] = $this->getBasketTabActive();

        $arrApplyGlobal = get($product_info,'apply_global');
        $arrApplyGlobalFilter = [ 'marks' => [], 'models' => [] ];
        foreach( $arrApplyGlobal AS &$app )
        {
            ///array_push($arrApplyGlobalFilter['marks'],[]);
            ///array_push($arrApplyGlobalFilter['models'],[]);

            $make = $app['make'];
            $make = strtoupper($make);
            $makeCode = crc32($make);

            $model = $app['model'];
            $model = str_replace(''.$app['constructionType'],'',''.$model);
            $modelCode = crc32($model);

            $arrApplyGlobalFilter['marks'][$makeCode] = $make;
            $arrApplyGlobalFilter['models'][$makeCode][$modelCode] = $model;

            $app['make'] = $make;
            $app['makeCode'] = $makeCode;
            $app['model'] = $model;
            $app['modelCode'] = $modelCode;
        }
        $this->data['apply_global'] = $arrApplyGlobal;
        $this->data['apply_global_filter'] = $arrApplyGlobalFilter;
        ///dd([$arrApplyGlobalFilter,$arrApplyGlobal]);


        $this->render('goods/info');
    }
}
