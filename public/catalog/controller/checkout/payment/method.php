<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCheckoutPaymentMethod extends ControllerStartupTezarius
{
	public function index()
    {
		$this->load->language('checkout/checkout');

		$payAddr = get($this->session->data,'payment_address');
		///dd([$payAddr,$this->session->data]);
		if( $payAddr )
		{
			// Totals
			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			$sort_order = [];
			$this->load->model('setting/extension');
			$results = $this->model_setting_extension->getExtensions('total');
			foreach( $results as $key => $value ) $sort_order[$key] = $this->config->get('total_'.$value['code'].'_sort_order');
			array_multisort($sort_order, SORT_ASC, $results);
			foreach( $results as $result ) if( $this->config->get('total_'.$result['code'].'_status') )
            {
                $this->load->model('extension/total/'.$result['code']);
                // We have to put the totals in an array so that they pass by reference.
                $this->{'model_extension_total_'.$result['code']}->getTotal($total_data);
            }

			// Payment Methods
			$method_data = [];
			$this->load->model('setting/extension');
			$results = $this->model_setting_extension->getExtensions('payment');
			///pp($results);
			$recurring = $this->cart->hasRecurringProducts(); /// Тут всегда пока FALSE
			foreach( $results as $result )
			{
                ///pp(['payment_'.$result['code'].'_status',$this->config->get('payment_'.$result['code'].'_status')]);
			    if( $this->config->get('payment_'.$result['code'].'_status') )
                {
                    $this->load->model('extension/payment/'.$result['code']);
                    $method = $this->{'model_extension_payment_'.$result['code']}->getMethod($payAddr,$total);
                    ///pp($method);
                    if( $method )
                    {
                        if( $recurring )
                        {
                            if(
                                property_exists($this->{'model_extension_payment_'.$result['code']},'recurringPayments')
                                && $this->{'model_extension_payment_'.$result['code']}->recurringPayments()
                            ){
                                $method_data[$result['code']] = $method;
                            }
                        }
                        else $method_data[$result['code']] = $method;

                    }
                }
            }
			$sort_order = []; foreach( $method_data as $key => $value ) $sort_order[$key] = $value['sort_order'];
			array_multisort($sort_order, SORT_ASC, $method_data);
			$this->session->data['payment_methods'] = $method_data;
            ///dd($method_data);
		}

		$payMethods = get($this->session->data,'payment_methods',[]);
        $this->data['payment_methods'] = $payMethods;
        $this->data['error_warning'] = ( empty($payMethods) )? sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact')) : '';


        $this->data['code']    = get(get($this->session->data,'payment_method',[]),'code');
        $this->data['comment'] = get($this->session->data,'comment');
		$this->data['scripts'] = $this->document->getScripts();

        $this->data['text_agree'] = '';
		if( $this->config->get('config_checkout_id') )
		{
			$this->load->model('catalog/information');
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));
			if( $information_info ) $this->data['text_agree'] = sprintf(
			    $this->language->get('text_agree'),
                $this->url->link('information/information/agree','information_id='.$this->config->get('config_checkout_id'),true),
                $information_info['title'], $information_info['title']
            );
		}

        $this->data['agree'] = get($this->session->data,'agree');

		$this->render('checkout/payment/method',FALSE);
	}

	public function save()
    {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
        /*/
		$products = $this->cart->getProducts();
		foreach( $products as $product )
		{
			$product_total = 0;
			foreach( $products as $product_2 ) if ($product_2['product_id'] == $product['product_id']) $product_total += $product_2['qty'];
			if( $product['part'] > $product_total )
			{
				$json['redirect'] = $this->url->link('checkout/cart');
				break;
			}
		}
		//*/

		if (!isset($this->request->post['payment_method'])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		} elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		}

		if( $this->config->get('config_checkout_id') )
		{
			$this->load->model('catalog/information');
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));
			if( $information_info && !isset($this->request->post['agree']) )
			{
				$json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
		}

		if( !$json )
		{
			$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];
			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		$this->jEcho($json);
	}
}
