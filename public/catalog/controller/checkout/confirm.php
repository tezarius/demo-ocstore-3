<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCheckoutConfirm extends ControllerStartupTezarius
{
	public function index()
    {
		$redirect = '';

		if( $this->cart->hasShipping() )
		{
			// Validate if shipping address has been set.
			if (!isset($this->session->data['shipping_address'])) {
				$redirect = $this->url->link('checkout/checkout', '', true);
			}

			// Validate if shipping method has been set.
			if (!isset($this->session->data['shipping_method'])) {
				$redirect = $this->url->link('checkout/checkout', '', true);
			}
		}
		else
		    {
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
		}

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$redirect = $this->url->link('checkout/checkout', '', true);
		}

		// Validate if payment method has been set.
		if (!isset($this->session->data['payment_method'])) {
			$redirect = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$redirect = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
        /*/
        $products = $this->cart->getProducts();
        foreach( $products as $product )
        {
            $product_total = 0;
            foreach( $products as $product_2 ) if ($product_2['product_id'] == $product['product_id']) $product_total += $product_2['qty'];
            if( $product['part'] > $product_total )
            {
                $redirect = $this->url->link('checkout/cart');
                break;
            }
        }
        //*/


		if( !$redirect )
		{
			$order_data = [];

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);
			$this->load->model('setting/extension');
			$sort_order = [];
			$results = $this->model_setting_extension->getExtensions('total');
			foreach ($results as $key => $value) $sort_order[$key] = $this->config->get('total_'.$value['code'].'_sort_order');
			array_multisort($sort_order, SORT_ASC, $results);
			foreach ($results as $result) {
				if ($this->config->get('total_'.$result['code'].'_status')) {
					$this->load->model('extension/total/'.$result['code']);
					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_'.$result['code']}->getTotal($total_data);
				}
			}

			$sort_order = []; foreach ($totals as $key => $value) $sort_order[$key] = $value['sort_order'];
			array_multisort($sort_order, SORT_ASC, $totals);

			$order_data['totals'] = $totals;

			$this->load->language('checkout/checkout');

			$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$order_data['store_id'] = $this->config->get('config_store_id');
			$order_data['store_name'] = $this->config->get('config_name');

			if ($order_data['store_id']) {
				$order_data['store_url'] = $this->config->get('config_url');
			} else {
				if ($this->request->server['HTTPS']) {
					$order_data['store_url'] = HTTPS_SERVER;
				} else {
					$order_data['store_url'] = HTTP_SERVER;
				}
			}
			
			$this->load->model('account/customer');

			if( $this->customer->isLogged() )
			{
				///$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
				$customer_info = $this->customer->getCustomer();

				$order_data['customer_id']       = $this->customer->getId();
				$order_data['customer_group_id'] = get($customer_info,'customer_group_id');
				$order_data['firstname']         = get($customer_info,'firstname');
				$order_data['lastname']          = get($customer_info,'lastname');
				$order_data['email']             = get($customer_info,'email');
				$order_data['telephone']         = get($customer_info,'telephone');
				$order_data['custom_field']      = json_decode(get($customer_info,'custom_field'), true);
			}
			elseif( isset($this->session->data['guest']) )
            {
				$order_data['customer_id']       = 0;
				$order_data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
				$order_data['firstname']         = $this->session->data['guest']['firstname'];
				$order_data['lastname']          = $this->session->data['guest']['lastname'];
				$order_data['email']             = $this->session->data['guest']['email'];
				$order_data['telephone']         = $this->session->data['guest']['telephone'];
				$order_data['custom_field']      = $this->session->data['guest']['custom_field'];
			}

			$order_data['payment_firstname']      = $this->session->data['payment_address']['firstname'];
			$order_data['payment_lastname']       = $this->session->data['payment_address']['lastname'];
			$order_data['payment_company']        = $this->session->data['payment_address']['company'];
			$order_data['payment_address_1']      = $this->session->data['payment_address']['address_1'];
			$order_data['payment_address_2']      = $this->session->data['payment_address']['address_2'];
			$order_data['payment_city']           = $this->session->data['payment_address']['city'];
			$order_data['payment_postcode']       = $this->session->data['payment_address']['postcode'];
			$order_data['payment_zone']           = $this->session->data['payment_address']['zone'];
			$order_data['payment_zone_id']        = $this->session->data['payment_address']['zone_id'];
			$order_data['payment_country']        = $this->session->data['payment_address']['country'];
			$order_data['payment_country_id']     = $this->session->data['payment_address']['country_id'];
			$order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
			$order_data['payment_custom_field']   = (isset($this->session->data['payment_address']['custom_field']) ? $this->session->data['payment_address']['custom_field'] : array());

			if (isset($this->session->data['payment_method']['title'])) {
				$order_data['payment_method'] = $this->session->data['payment_method']['title'];
			} else {
				$order_data['payment_method'] = '';
			}

			if (isset($this->session->data['payment_method']['code'])) {
				$order_data['payment_code'] = $this->session->data['payment_method']['code'];
			} else {
				$order_data['payment_code'] = '';
			}

			if( $this->cart->hasShipping() )
			{
				$order_data['shipping_firstname']      = $this->session->data['shipping_address']['firstname'];
				$order_data['shipping_lastname']       = $this->session->data['shipping_address']['lastname'];
				$order_data['shipping_company']        = $this->session->data['shipping_address']['company'];
				$order_data['shipping_address_1']      = $this->session->data['shipping_address']['address_1'];
				$order_data['shipping_address_2']      = $this->session->data['shipping_address']['address_2'];
				$order_data['shipping_city']           = $this->session->data['shipping_address']['city'];
				$order_data['shipping_postcode']       = $this->session->data['shipping_address']['postcode'];
				$order_data['shipping_zone']           = $this->session->data['shipping_address']['zone'];
				$order_data['shipping_zone_id']        = $this->session->data['shipping_address']['zone_id'];
				$order_data['shipping_country']        = $this->session->data['shipping_address']['country'];
				$order_data['shipping_country_id']     = $this->session->data['shipping_address']['country_id'];
				$order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
				$order_data['shipping_custom_field']   = (isset($this->session->data['shipping_address']['custom_field']) ? $this->session->data['shipping_address']['custom_field'] : array());

				if (isset($this->session->data['shipping_method']['title'])) {
					$order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
				} else {
					$order_data['shipping_method'] = '';
				}

				if (isset($this->session->data['shipping_method']['code'])) {
					$order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
				} else {
					$order_data['shipping_code'] = '';
				}
			}
			else
			{
				$order_data['shipping_firstname'] = '';
				$order_data['shipping_lastname'] = '';
				$order_data['shipping_company'] = '';
				$order_data['shipping_address_1'] = '';
				$order_data['shipping_address_2'] = '';
				$order_data['shipping_city'] = '';
				$order_data['shipping_postcode'] = '';
				$order_data['shipping_zone'] = '';
				$order_data['shipping_zone_id'] = '';
				$order_data['shipping_country'] = '';
				$order_data['shipping_country_id'] = '';
				$order_data['shipping_address_format'] = '';
				$order_data['shipping_custom_field'] = array();
				$order_data['shipping_method'] = '';
				$order_data['shipping_code'] = '';
			}

			$order_data['products'] = array();

			foreach( $this->cart->getProducts() AS $product )
			{
				$option_data = array();

				foreach( get($product,'option',[]) AS $option )
				{
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'option_id'               => $option['option_id'],
						'option_value_id'         => $option['option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				$order_data['products'][] = array(
					'product_id' => get($product,'id'),
					'name'       => get($product,'name'),
					'model'      => get($product,'code_display ')." ".get($product,'brand'),
					'option'     => $option_data,
					'download'   => get($product,'download'),
					'quantity'   => get($product,'qty'),
					'subtract'   => get($product,'subtract'),
					'price'      => get($product,'cost'),
					'total'      => get($product,'total'),
					'tax'        => $this->tax->getTax(get($product,'cost'),get($product,'tax_class_id')),
					'reward'     => get($product,'dscntBonus')
				);
				///dd($product);
			}

			// Gift Voucher
			$order_data['vouchers'] = [];
			if( !empty($this->session->data['vouchers']) ) foreach( $this->session->data['vouchers'] AS $voucher )
            {
                $order_data['vouchers'][] = array(
                    'description'      => $voucher['description'],
                    'code'             => token(10),
                    'to_name'          => $voucher['to_name'],
                    'to_email'         => $voucher['to_email'],
                    'from_name'        => $voucher['from_name'],
                    'from_email'       => $voucher['from_email'],
                    'voucher_theme_id' => $voucher['voucher_theme_id'],
                    'message'          => $voucher['message'],
                    'amount'           => $voucher['amount']
                );
            }

			$order_data['comment'] = $this->session->data['comment'];
			$order_data['total'] = $total_data['total'];

			if( isset($this->request->cookie['tracking']) )
			{
				$order_data['tracking'] = $this->request->cookie['tracking'];

				$subtotal = $this->cart->getSubTotal();

				// Affiliate
				$affiliate_info = $this->model_account_customer->getAffiliateByTracking($this->request->cookie['tracking']);

				if ($affiliate_info) {
					$order_data['affiliate_id'] = $affiliate_info['customer_id'];
					$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
				} else {
					$order_data['affiliate_id'] = 0;
					$order_data['commission'] = 0;
				}

				// Marketing
				$this->load->model('checkout/marketing');

				$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

				if ($marketing_info) {
					$order_data['marketing_id'] = $marketing_info['marketing_id'];
				} else {
					$order_data['marketing_id'] = 0;
				}
			}
			else
			{
				$order_data['affiliate_id'] = 0;
				$order_data['commission'] = 0;
				$order_data['marketing_id'] = 0;
				$order_data['tracking'] = '';
			}

			$order_data['language_id'] = $this->config->get('config_language_id');
			$order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
			$order_data['currency_code'] = $this->session->data['currency'];
			$order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
			$order_data['ip'] = ip();
            $order_data['forwarded_ip'] = ip(TRUE);
            $order_data['user_agent'] = get($this->request->server,'HTTP_USER_AGENT');
            $order_data['accept_language'] = get($this->request->server,'HTTP_ACCEPT_LANGUAGE');

			$this->load->model('checkout/order');

			///$this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);
            $order_data = array_merge_recursive($order_data,[
                'ShopID' => $this->customer->getStockId(),
                'UserID' => $this->customer->getId(),
                'TabID' => $this->getBasketTabActive(),
                'FirmID' => $this->customer->getFirmId(),
                'PriceLID' => $this->customer->getPriceLID(),

                'CarsCustomerID' => 0,
                'CustomersSourceID' => 0,
                'ManagerID' => $this->customer->getManagerID(),

                'isDelivery' => 0,
                'addressDelivery' => '',
                'isDeliveryPartly' => 0,

                'CardID' => $this->customer->getDiscountCardID(),
                'note' => $order_data['comment'],

                '' => '',
            ]);
			$res = $this->model_checkout_order->create($order_data);
			///dd($res);
            if( get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get($res,'mess')
                ];
            }
            elseif( $_id = get($res,'jrDocsID') )
            {
                $this->flashers[] = [
                    'type' => 'success',
                    'text' => sprintf($this->language->get('checkout_create_success'),$_id)
                ];
                $this->session->data['order_id'] = $_id;
            }
            else
            {
                $this->flashers[] = [
                    'type' => 'warning',
                    'text' => $this->language->get('checkout_create_warning')
                ];
            }
            $this->data['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);


			$this->data['products'] = array();
			$this->load->model('tool/upload');
			///dd($this->cart->getProducts());
			foreach( $this->cart->getProducts() as $product )
			{
				$option_data = array();
				foreach( get($product,'option',[]) AS $option )
				{
					if( $option['type'] != 'file' ) $value = $option['value'];
					else
					{
						$upload_info = (array) $this->model_tool_upload->getUploadByCode($option['value']);
						$value = get($upload_info,'name');
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20).'..' : $value)
					);
				}

				$recurring = '';
				if( $_recurring = get($product,'recurring') )
				{
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($_recurring['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($_recurring['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $_recurring['trial_cycle'], $frequencies[$_recurring['trial_frequency']], $_recurring['trial_duration']).' ';
					}

					if ($_recurring['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($_recurring['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $_recurring['cycle'], $frequencies[$_recurring['frequency']], $_recurring['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($_recurring['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $_recurring['cycle'], $frequencies[$_recurring['frequency']], $_recurring['duration']);
					}
				}

				$this->data['products'][] = array(
					'cart_id'    => get($product,'tabID'),
					'product_id' => get($product,'id'),
					'name'       => get($product,'name'),
					'model'      => get($product,'model'),
					'option'     => $option_data,
					'recurring'  => $recurring,
					'quantity'   => get($product,'qty')*1,
					'subtract'   => get($product,'subtract'),
					'price'      => $this->currency->format($this->tax->calculate(get($product,'cost',0)*1,get($product,'tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']),
					'total'      => $this->currency->format($this->tax->calculate(get($product,'total',0)*1,get($product,'tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']),
					'href'       => $this->url->link('price/offers','number='.$product['code'],'brand='.$product['brand'])
				);
				///dd($product);
			}

			// Gift Voucher
			$this->data['vouchers'] = array();
			if ( !empty($this->session->data['vouchers']) ) foreach( $this->session->data['vouchers'] as $voucher )
			{
                $this->data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency'])
                );
            }


			$this->data['totals'] = array();
			foreach( $order_data['totals'] as $total )
			{
				$this->data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}

			///dd([$this->session->data['payment_method']['code']]);
			$this->data['payment'] = $this->load->controller('extension/payment/'.$this->session->data['payment_method']['code']);
		} 
		else $this->data['redirect'] = $redirect;
		

		$this->render('checkout/confirm',FALSE);
	}
}
