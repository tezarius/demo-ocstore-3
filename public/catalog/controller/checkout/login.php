<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCheckoutLogin extends ControllerStartupTezarius
{
	public function index()
    {
		$this->load->language('checkout/checkout');

		$data['checkout_guest'] = ($this->config->get('config_checkout_guest') && !$this->config->get('config_customer_price') && !$this->cart->hasDownload());

        $data['account'] = get($this->session->data,'account');

		$data['forgotten'] = $this->url->link('account/forgotten', '', true);

		$this->response->setOutput($this->load->view('checkout/login', $data));
	}

	public function save()
    {
		$this->load->language('checkout/checkout');

		$json = [];

		if( $this->customer->isLogged() ) $json['redirect'] = $this->url->link('checkout/checkout', '', true);

        ///
		if(
		    ( !$this->cart->hasProducts() && empty($this->session->data['vouchers']) )
            ||
            ( !$this->cart->hasStock() && !$this->config->get('config_stock_checkout') )
        ){
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		if( !$json )
		{
			$this->load->model('account/customer');

			$email = rcv('email');
			$password = rcv('password');

			// Check how many login attempts have been made.
			$login_info = $this->model_account_customer->getLoginAttempts($email);
			if( $login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified']) )
			{
				$json['error']['warning'] = $this->language->get('error_attempts');
			}

			// Check if customer has been approved.
			$customer_info = $this->model_account_customer->getCustomerByEmail($email);
			if( $customer_info && !$customer_info['status'] )
			{
				$json['error']['warning'] = $this->language->get('error_approved');
			}

			if( !isset($json['error']) )
			{
				if( !$this->customer->login($email,$password) )
				{
					$json['error']['warning'] = $this->language->get('error_login');
					$this->model_account_customer->addLoginAttempt($email);
				}
				else {
					$this->model_account_customer->deleteLoginAttempts($email);
				}
			}
		}

		if( !$json )
		{
			// Unset guest
			unset($this->session->data['guest']);

			// Default Shipping Address
			$this->load->model('account/address');

			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			// Wishlist
			if( isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist']) )
			{
				$this->load->model('account/wishlist');
				foreach( $this->session->data['wishlist'] as $key => $product_id )
				{
					$this->model_account_wishlist->addWishlist($product_id);
					unset($this->session->data['wishlist'][$key]);
				}
			}

			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		$this->jEcho($json);
	}
}
