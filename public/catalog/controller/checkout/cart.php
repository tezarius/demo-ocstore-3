<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerCheckoutCart extends ControllerStartupTezarius
{
    public function index()
    {
        $this->load->language('checkout/cart');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->setRobots('noindex,follow');

        $this->breadcrumbs = [[
            'href' => $this->url->link('common/home'),
            'text' => $this->language->get('text_home')
        ],[
            'href' => $this->url->link('checkout/cart'),
            'text' => $this->language->get('heading_title')
        ]];

        ///
        if( $this->cart->hasProducts() || !empty($this->session->data['vouchers'] ))
        {
            //*/
            if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => $this->language->get('error_stock'),
                ];
            }
            if( $this->config->get('config_customer_price') && !$this->customer->isLogged() )
            {
                $this->flashers[] = [
                    'type' => 'info',
                    'text' => sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'))
                ];
            }
            //*/

            $this->data['action'] = $this->url->link('checkout/cart/edit', '', true);

            if( $this->config->get('config_cart_weight') )
            {
                $this->data['weight'] = $this->weight->format(
                    $this->cart->getWeight(),
                    $this->config->get('config_weight_class_id'),
                    $this->language->get('decimal_point'),
                    $this->language->get('thousand_point')
                );
            } 
            else $this->data['weight'] = '';


            $tabActive = $this->getBasketTabActive();
            $tabs = $this->cart->getTabs();
            ///dd($tabs);
            /// Проверям не удалена ли активная корзина
            $isExist = 0; foreach( $tabs AS $t ) if( (int)get($t,'tabID')===$tabActive ){ $isExist = 1; break; }
            if( !$isExist )
            {
                /// Делаем активной по последней модифицированной
                $LastUpdTimeSave = 0;
                foreach( $tabs AS $t )
                {
                    $LastUpdTime = date('YmdHis',strtotime(get($t,'LastUpdTime')));
                    if( $LastUpdTime > $LastUpdTimeSave )
                    {
                        $LastUpdTimeSave = $LastUpdTime;
                        $tabActive = get($t,'tabID');
                    }
                }
                $this->setBasketTabActive($tabActive);
            }
            $this->data['tabsList'] = $tabs;
            $this->data['tabActive'] = $tabActive;


            /*/
            $ByRecord = 0;
            $ShopID = (int) $this->getStockID();
            $UserID = (int) $this->customer->getId();
            $SessID = $this->session->getId();
            ///$products = $this->cart->loadBasket($ShopID,$tabActive,$UserID,$SessID,$ByRecord);
            //*/
            $products = $this->cart->getProducts();
            ///dd($products);
            if( 23==32 )
            {
                $this->load->model('tool/image');
                $this->load->model('tool/upload');
                foreach( $products as $product )
                {
                    $product_total = 0;

                    foreach( $products as $product_2 )
                    {
                        if( $product_2['product_id'] == $product['product_id'] )
                        {
                            $product_total += $product_2['qty'];
                        }
                    }

                    if ($product['minimum'] > $product_total) {
                        $this->data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
                    }

                    if ($product['image']) {
                        $image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_'.$this->config->get('config_theme').'_image_cart_width'), $this->config->get('theme_'.$this->config->get('config_theme').'_image_cart_height'));
                    } else {
                        $image = '';
                    }

                    $option_data = array();

                    foreach ($product['option'] as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $value = $upload_info['name'];
                            } else {
                                $value = '';
                            }
                        }

                        $option_data[] = array(
                            'name'  => $option['name'],
                            'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20).'..' : $value)
                        );
                    }

                    // Display prices
                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));

                        $price = $this->currency->format($unit_price, $this->session->data['currency']);
                        $total = $this->currency->format($unit_price * $product['qty'], $this->session->data['currency']);
                    }
                    else {
                        $price = false;
                        $total = false;
                    }

                    $recurring = '';
                    if( $product['recurring'] )
                    {
                        $frequencies = array(
                            'day'        => $this->language->get('text_day'),
                            'week'       => $this->language->get('text_week'),
                            'semi_month' => $this->language->get('text_semi_month'),
                            'month'      => $this->language->get('text_month'),
                            'year'       => $this->language->get('text_year')
                        );

                        if ($product['recurring']['trial']) {
                            $recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['qty'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']).' ';
                        }

                        if ($product['recurring']['duration']) {
                            $recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['qty'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                        } else {
                            $recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['qty'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
                        }
                    }

                    $this->data['products'][] = array(
                        'cart_id'   => $product['cart_id'],
                        'thumb'     => $image,
                        'name'      => $product['name'],
                        'model'     => $product['model'],
                        'option'    => $option_data,
                        'recurring' => $recurring,
                        'quantity'  => $product['qty'],
                        'stock'     => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
                        'reward'    => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
                        'price'     => $price,
                        'total'     => $total,
                        'href'      => $this->url->link('product/product', 'product_id='.$product['product_id'])
                    );
                }
            }
            $this->data['products'] = $products;


            // Totals
            $totals = array();
            $taxes = $this->cart->getTaxes();
            $total = 0;
            // Because __call can not keep var references so we put them into an array.
            $total_data = array(
                'totals' => &$totals,
                'taxes'  => &$taxes,
                'total'  => &$total
            );
            // Display prices
            if( $this->customer->isLogged() || !$this->config->get('config_customer_price') )
            {
                $sort_order = array();
                $this->load->model('setting/extension'); /// Как оригинал, так, для порядку
                $results = $this->model_setting_extension->getExtensions('total');
                foreach( $results as $key => $value ) $sort_order[$key] = $this->config->get('total_'.$value['code'].'_sort_order');
                array_multisort($sort_order, SORT_ASC, $results);
                foreach( $results as $result )
                {
                    if( $this->config->get('total_'.$result['code'].'_status') )
                    {
                        $this->load->model('extension/total/'.$result['code']);
                        // We have to put the totals in an array so that they pass by reference.
                        $this->{'model_extension_total_'.$result['code']}->getTotal($total_data);
                    }
                }
                $sort_order = array(); foreach( $totals as $key => $value ) $sort_order[$key] = $value['sort_order'];
                array_multisort($sort_order, SORT_ASC, $totals);
            }
            ///dd($totals);


            $this->data['totals'] = array();
            foreach( $totals as $total )
            {
                $this->data['totals'][] = array(
                    'title' => $total['title'],
                    'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
                );
            }

            // Gift Voucher
            $this->data['vouchers'] = array();
            if( !empty($this->session->data['vouchers'])) {
                foreach ($this->session->data['vouchers'] as $key => $voucher) {
                    $this->data['vouchers'][] = array(
                        'key'         => $key,
                        'description' => $voucher['description'],
                        'amount'      => $this->currency->format($voucher['amount'], $this->session->data['currency']),
                        'remove'      => $this->url->link('checkout/cart', 'remove='.$key)
                    );
                }
            }


            $this->data['continue'] = $this->url->link('common/home');
            $this->data['checkout'] = $this->url->link('checkout/checkout', '', true);

            $this->data['modules'] = array();
            $files = glob(DIR_APPLICATION.'/controller/extension/total/*.php');
            if( $files ) foreach( $files as $file )
            {
                ///if( 'reward'==basename($file, '.php') ) pp($this->cart->getProducts());
                $result = $this->load->controller('extension/total/'.basename($file, '.php'));
                if( $result ) $this->data['modules'][] = $result;

            }

            $view = 'checkout/cart';
        }
        else
        {
            $this->data['text_error'] = $this->language->get('text_empty');
            $this->data['continue'] = $this->url->link('common/home');

            unset($this->session->data['success']);

            $view = 'error/not_found';
        }
        $this->render($view);
    }
    public function add()
    {
        $this->load->language('checkout/cart');

        $ans = [];

        ///dd([$this->request->post]);
        $ShopID = (int) $this->getStockID();
        $UserID = (int) $this->customer->getId();
        $SessID = $this->session->getId();
        $PriceLID = (int) $this->customer->getPriceLID();

        $tabActive = $this->getBasketTabActive();
        $tabs = $this->cart->getTabs($ShopID,$UserID,$SessID);
        $LastUpdTimeSave = $LastUpdTabID = 0; $TabExist = FALSE;
        foreach( $tabs AS $t )
        {
            $LastUpdTime = date('YmdHis',strtotime(get($t,'LastUpdTime')));
            if( $LastUpdTime > $LastUpdTimeSave )
            {
                $LastUpdTimeSave = $LastUpdTime;
                $LastUpdTabID = get($t,'tabID');
            }
            $TabID = get($t,'tabID');
            if( $TabID==$tabActive ) $TabExist = TRUE;
        }
        $tabActive = ( !$TabExist ) ? $tabActive : $LastUpdTabID;

        $ans['tabActive'] = $tabActive;
        $ans['tabs'] = $tabs;
        $this->setBasketTabActive($tabActive);

        $rbg = (int) rcv('rbg');
        $tzp = rcv('tzp');
        $qty = (int) rcv('qty');
        $std = (int) rcv('std');
        $pld = (int) rcv('pld');

        $res = $this->cart->add($rbg,$tzp,$qty,$std,$pld,$ShopID,$UserID,$PriceLID,$SessID,$tabActive);
        /// $res['isError'] = '1'; $res['mess'] = 'test error message';

        if( get($res,'isError') )
        {
            $this->flashers[] = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
        }
        if( $_id = get($res,'AddCartID') )
        {
            $ans['cartID'] = $_id;
            $this->flashers[] = [
                'type' => 'success',
                'text' => sprintf($this->language->get('basket_add_success'),$this->url->link('checkout/cart'))
            ];
        }
        $ans['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);

        $this->jEcho($ans);
    }
    public function update()
    {
        $this->load->language('checkout/cart');

        $json = [];

        $id = (int) rcv('id');
        $qty = (int) rcv('qty');
        $note = rcv('note');

        $ShopID = (int) $this->getStockID();
        $UserID = (int) $this->customer->getId();
        $SessID = $this->session->getId();
        $PriceLID = (int) $this->customer->getPriceLID();

        $TabID = $this->getBasketTabActive();

        // Update
        $res = $this->cart->update($id,$ShopID,$TabID,$UserID,$SessID,$qty,$note,$PriceLID);
        $json['$res'] = $res;
        if( get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
            $status = 0;
            $redirect = '';
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => $this->language->get('basket_update')
            ];
            $status = 1;
            $redirect = $this->url->link('checkout/cart');
            /// Если не существует заметки, значит меняли кол-во, значит сбрасываем скидку
            if( !$note ) unset($this->session->data['reward']);
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;

        $this->flashers[] = $message;
        $json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);

        $this->jEcho($json);
    }
    public function remove()
    {
        $this->load->language('checkout/cart');

        $json = array();

        // Remove
        $key = rcv('key');
        if( $key )
        {
            $res = $this->cart->remove($key);
            if( get($res,'isError') )
            {
                $message = [
                    'type' => 'error',
                    'text' => get($res,'mess')
                ];
                $json['status'] = 0;
            }
            else
            {
                $message = [
                    'type' => 'success',
                    'text' => $this->language->get('text_remove')
                ];
                $json['status'] = 1;
            }
            $this->flashers[] = $message;

            $json['message'] = $message;
            $json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);


            unset($this->session->data['vouchers'][$key]);
            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['reward']);

            // Totals
            $totals = array();
            $taxes = $this->cart->getTaxes();
            $total = 0;
            // Because __call can not keep var references so we put them into an array.
            $total_data = array(
                'totals' => &$totals,
                'taxes'  => &$taxes,
                'total'  => &$total
            );
            // Display prices
            if( $this->customer->isLogged() || !$this->config->get('config_customer_price') )
            {
                $sort_order = array();

                $this->load->model('setting/extension');
                $results = $this->model_setting_extension->getExtensions('total');
                foreach( $results as $key => $value ) $sort_order[$key] = $this->config->get('total_'.$value['code'].'_sort_order');
                array_multisort($sort_order, SORT_ASC, $results);
                foreach( $results as $result )
                {
                    if( $this->config->get('total_'.$result['code'].'_status') )
                    {
                        $this->load->model('extension/total/'.$result['code']);
                        // We have to put the totals in an array so that they pass by reference.
                        $this->{'model_extension_total_'.$result['code']}->getTotal($total_data);
                    }
                }

                $sort_order = array(); foreach( $totals as $key => $value ) $sort_order[$key] = $value['sort_order'];
                array_multisort($sort_order, SORT_ASC, $totals);
            }

            $json['total'] = sprintf(
                $this->language->get('text_items'),
                ///$this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0),
                $this->cart->countProducts() + count( get($this->session->data,'vouchers',[]) ),
                $this->currency->format($total,get($this->session->data,'currency'))
            );
        }

        $this->jEcho($json);
    }
    public function create()
    {
        $this->load->language('checkout/cart');

        $status = 0;

        $tabActive = $this->getBasketTabActive();

        $ShopID = (int) $this->getStockID();
        $UserID = (int) $this->customer->getId();
        $SessID = $this->session->getId();
        $tabs = $this->cart->loadTabs($ShopID,$UserID,$SessID);

        foreach( $tabs AS $t )
        {
            if( (int) get($t,'tabID')===$tabActive )
            {
                ++$tabActive;
                $status = 1;
                break;
            }
        }
        $this->setBasketTabActive($tabActive);

        $this->jEcho(['status'=>$status,'TabID'=>$tabActive,'message'=>$this->language->get('basket_create')]);
    }
    public function change()
    {
        $this->load->language('checkout/cart');

        $status = 0;
        $redirect = '';

        $tabChange = (int) rcv('key');

        $ShopID = (int) $this->getStockID();
        $UserID = (int) $this->customer->getId();
        $SessID = $this->session->getId();
        $tabs = $this->cart->loadTabs($ShopID,$UserID,$SessID);

        foreach( $tabs AS $t )
        {
            if( (int) get($t,'tabID')===$tabChange )
            {
                $status = 1;
                $this->setBasketTabActive($tabChange);
                $redirect = $this->url->link('checkout/cart');
                break;
            }
        }

        $this->jEcho(['status'=>$status,'change'=>[$_POST,$_GET],'redirect'=>$redirect]);
    }
    public function clear()
    {
        $this->load->language('checkout/cart');

        $json = [];

        $status = 0;
        $redirect = '';

        $res = $this->cart->clear();
        $json['$res'] = $res;
        if( get($res,'isError') )
        {
            $message = [
                'type' => 'error',
                'text' => get($res,'mess')
            ];
        }
        else
        {
            $message = [
                'type' => 'success',
                'text' => $this->language->get('basket_clear')
            ];
            $status = 1;
            $redirect = $this->url->link('checkout/cart');
        }
        $json['status'] = $status;
        $json['message'] = $message;
        $json['redirect'] = $redirect;

        $this->flashers[] = $message;
        $json['flash'] = $this->load->controller('common/flash',['flashers'=>$this->flashers]);

        $this->jEcho($json);
    }
    public function tabs()
    {
        ///
    }
}
