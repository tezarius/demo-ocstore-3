<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerVinRequest extends ControllerStartupTezarius
{
    private $error = [];
    public function index()
    {
        $this->load->language('vin/request');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->setRobots('noindex,follow');

        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
        $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

        $StockID = $this->officeChoosed();

        $this->data = $this->request->post;
        $this->data['StockID'] = $StockID;
        ///dd($this->data);

        $this->load->model('account/vin');
        $this->load->model('account/customer');
        if( $StockID && get($this->request->server,'REQUEST_METHOD')=='POST' && $this->validate() )
        {
            ///dd($this->data);
            $CustomerUID = $this->customer->getId();
            if( !$CustomerUID )
            {
                $name = rcv('name');
                $phone = rcv('phone');
                $res = $this->model_account_customer->verifyExistence('',$phone,'');
                if( get($res,'isError') )
                {
                    $this->flashers[] = [
                        'type' => 'success',
                        'text' => get($res,'mess'),
                    ];
                    $CustomerUID = get($res,'CounterpartsID');
                }
                else
                {
                    $res = $this->model_account_vin->checkUserExist($StockID,$name,$phone);
                    ///pp($res);
                    if( get($res,'isError') )
                    {
                        $this->flashers[] = [
                            'type' => 'error',
                            'text' => get($res,'mess'),
                        ];
                    }
                    else
                    {
                        $CustomerUID = get($res,'AnyID');
                    }
                }

            }
            if( $CustomerUID )
            {
                $vin  = rcv('vin');
                $note = rcv('note');
                $CarID     = 0;
                $ManagerID = $this->customer->getManagerID();
                $SourceID  = $this->customer->getSourceID();
                $res = $this->model_account_vin->request($CustomerUID,$CarID,$vin,$note,$StockID,$ManagerID,$SourceID);
                ///dd($res);
                if( get($res,'isError') )
                {
                    $this->flashers[] = [
                        'type' => 'error',
                        'text' => get($res,'mess'),
                    ];
                }
                else
                {
                    $this->session->data['flashers'][] = [
                        'type' => 'success',
                        'text' => 'Ваш запрос успешно добавлен. Ожидайте ответа от менеджера. Спасибо!',
                    ];
                    $this->response->redirect($this->url->link('common/home'));
                }
            }
            else
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => 'Что-то пошло не так :( [tzvr]',
                ];
            }
        }

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $this->language->get('text_vin_request'),
            'href' => $this->url->link('vin/request', '', true)
        ]];

        $this->data['error_vin']   = get($this->error,'vin');
        $this->data['error_name']  = get($this->error,'name');
        $this->data['error_phone'] = get($this->error,'phone');
        $this->data['error_note']  = get($this->error,'note');

        if( $_err = get($this->error,'agree') ) $this->flashers[] = [
            'type' => 'error',
            'text' => $_err,
        ];

        $this->data['action'] = $this->url->link('vin/request','',true);

        $this->data['vin']   = rcv('vin');
        $this->data['name']  = rcv('name');
        $this->data['phone'] = rcv('phone');
        $this->data['note']  = rcv('note');


        // Captcha
        if( $this->config->get('captcha_'.$this->config->get('config_captcha').'_status') && in_array('register',(array)$this->config->get('config_captcha_page')) )
        {
            $this->data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
        }
        else $this->data['captcha'] = '';


        $this->data['text_agree'] = '';
        if( $this->config->get('config_account_id') )
        {
            $this->load->model('catalog/information');
            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));
            if( $information_info )
            {
                $this->data['text_agree'] = sprintf($this->language->get('text_agree'),
                    $this->url->link('information/information/agree', 'information_id='.$this->config->get('config_account_id'), true),
                    $information_info['title'],
                    $information_info['title']
                );
            }
        }
        $this->data['agree'] = rcv('agree');

        ///dd($this->flashers);
        $this->render('vin/request');
    }

    private function validate()
    {
        $note = rcv('note');
        $note_l = utf8_strlen($note);
        if( $note_l < 3 ) $this->error['note'] = $this->language->get('error_note');

        $vin = rcv('vin');
        $vin_l = utf8_strlen($vin);
        if( $vin_l < 5 || $vin_l > 17 ) $this->error['vin'] = $this->language->get('error_vin');

        $name = rcv('name');
        $name_l = utf8_strlen($name);
        if( $name_l < 1 || $name_l > 32 ) $this->error['name'] = $this->language->get('error_name');

        $phone = rcv('phone');
        $phone = preg_replace("/[^0-9]/", '', $phone);
        if( utf8_strlen($phone) < 11 )
        {
            $this->error['phone'] = $this->language->get('error_phone');
        }

        // Captcha
        if( $this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('vin-request', (array)$this->config->get('config_captcha_page')) )
        {
            $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');
            if( $captcha ) $this->error['captcha'] = $captcha;

        }

        // Agree to terms
        if( $this->config->get('config_account_id') )
        {
            $this->load->model('catalog/information');

            $information_info = $this->model_catalog_information->getInformation($this->config->get('config_account_id'));

            if( $information_info && !isset($this->request->post['agree']) )
            {
                $this->error['agree'] = sprintf($this->language->get('error_agree'), $information_info['title']);
            }
        }

        return !$this->error;
    }
}
