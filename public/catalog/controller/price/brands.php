<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerPriceBrands extends ControllerStartupTezarius
{
    public function index()
    {
        $this->load->language('product/product');
        $this->load->language('price/brands');

        $number = rcv('number');

        $this->data['offersLink'] = $this->url->link('price/offers',"number=$number");

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => "Бренды для $number",
            'href' => $this->url->link('price/brands',"&number=$number")
        ],[
            'text' => '<i class="fa fa-refresh"></i> Обновить',
            'href' => $this->url->link('price/brands',"&number=$number&reload=1")
        ]];


        $isReLoad = rcv('reload');
        ///$isReLoad =  buttonPressed('btnBrandReload');
        $this->load->model('price');
        $brands = $this->model_price->getBrands($number);
        if( empty($brands) || $isReLoad )
        {
            $res = $this->model_price->getBrandsKey($number);
            if( get($res,'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get($res,'mess'),
                ];
            }
            else
            {
                $IDSearchCode = get($res,'IDSearchCode');
                $this->model_price->getBrandsRemote($IDSearchCode);
                $brands = $this->model_price->getBrands($number);
            }
        }
        $this->data['brands'] = $brands;

        $view = ( count($brands)>0 ) ? 'price/brands' : 'error/not_found';

        $this->document->setTitle('test_title');
        $this->document->setDescription('test_description');
        $this->document->setKeywords('test_keywords');
        $this->document->addLink($this->url->link('price/brands', "number=$number"), 'canonical');
        ///$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        ///$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');


        $this->data['action'] = $this->url->link('price/brand','',true);

        $this->render($view);
    }
}
