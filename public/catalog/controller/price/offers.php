<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerPriceOffers extends ControllerStartupTezarius
{
    public function index()
    {
        ///$this->load->language('price/offers');

        $number = rcv('number'); /// $number = $this->request->get['number'];
        $this->data['offersLink'] = $this->url->link('price/offers',"number=$number");

        $brand = rcv('brand'); /// $brand = $this->request->get['brand'];

        $view = 'price/offers';
        if( $StockID = $this->officeChoosed() )
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->load->model('price');
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Check Price Cash
            $recentlyRequest = FALSE; /// Недавно был запрос ?
            $cash = get($this->session->data,'priceCash');
            ///dd($cash);
            $queryStr = get($cash,'QUERY_STR');
            if( $queryStr && $queryStr == crc32("{$number}{$brand}") )
            {
                $lastQuery = (int) get($cash,'LAST_TIME'); /// Первый раз 0
                $timePassedS = time() - $lastQuery; /// Первый раз - Дохера секунд
                $timePassedM = $timePassedS / 60;   /// Первый раз - Дохера минут
                ///dd([$cash,$timePassedS,$timePassedM]);
                if( $timePassedM < 5 ) $recentlyRequest = TRUE; /// Меньше 5-ти минут - значит недавно был запрос
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if( !$recentlyRequest )
            {
                $res = $this->model_price->getSearchID($number,$brand);
                if( get($res,'isError') )
                {
                    $this->flashers[] = [
                        'type' => 'error',
                        'text' => get($res,'mess'),
                    ];
                    $IDSearchCode = 0;
                } /// $this->session->data['error'] || $this->session->data['success']
                else $IDSearchCode = get($res,'IDSearchCode','IDSearchCode');
                ///$this->data['IDSearchCode'] = [$IDSearchCode,$res];

                $this->model_price->webServicesLoad($IDSearchCode);

                /// Create Price Cash
                $this->session->data['priceCash'] = [
                    'SEARCH_ID' => $IDSearchCode,
                    'QUERY_STR' => crc32("{$number}{$brand}"),
                    'LAST_TIME' => time(),
                ];
            }
            else $IDSearchCode = get($cash,'SEARCH_ID');
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $PriceLevelID = $this->customer->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'ByCodeBrand';
            $SearchValue = $IDSearchCode;
            $SortData = '';
            ///pp([$recentlyRequest,$StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData]);
            $offers = $this->model_price->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( get(get($offers,0,[]),'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get(get($offers,0,[]),'mess'),
                ];
                $offers = [];
            }
            else array_walk($offers, function(&$row)
            {
                $row['more'] = $this->url->link('goods/info',"hash=".arrEncryption([
                        'artbrID' => (int) get($row,'id_rbArticles'),
                        'brandID' => (int) get($row,'id_rbBrands'),
                        'brand'   => get($row,'brand'),
                        'code'    => get($row,'code'),
                        'goodsID' => (int) get($row,'GoodsID'),
                        ///
                        'offer' => [
                            'qty'  => (int) get($row,'qty')*1,
                            'step' => (int) get($row,'cost_for_qty'),
                            'rbg'  => (int) get($row,'GoodsID'),
                            'tzp'  => get($row,'tzp'),
                            'std'  => (int) get($row,'id_rbStock'),
                            'pld'  => (int) get($row,'id_rbStockStoragePlace'),
                            'reward' => $this->customer->getRewardPoints(),
                        ],
                    ])
                );
            });
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            $offers = [];
        }

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => "Список брендов для $number",
            'href' => $this->url->link('price/brands',"&number=$number")
        ],[
            'text' => "$number / $brand (".count($offers)." всего)",
            'href' => $this->url->link('price/offers',"number=$number","brand=$brand")
        ]];

        //dd($offers);
        $this->data['offers'] = $offers;
        $this->data['basketTabID'] = $this->getBasketTabActive();

        $this->document->setTitle('test_title');
        $this->document->setDescription('test_description');
        $this->document->setKeywords('test_keywords');
        $this->document->addLink($this->url->link('price/offers',"number=$number","brand=$brand"), 'canonical');
        ///$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        ///$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

        $this->renderCommon();
        $this->response->setOutput($this->load->view($view,$this->data));
    }
}
