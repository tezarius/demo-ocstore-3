<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt


class ControllerPriceGoods extends ControllerStartupTezarius
{
    public function index()
    {
        ///$this->load->language('price/offers');

        $hash = rcv('hash');
        $arr = arrDecryption($hash);
        $id = get($arr,'id');
        $name = get($arr,'name');

        $goodsLink = $this->url->link('goods/info',"hash={$hash}");
        $this->data['offersLink'] = $goodsLink;

        $this->breadcrumbs = [[
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        ],[
            'text' => $name,
            'href' => $goodsLink
        ]];
        ///$this->data['offers'] = [$number,$brand];


        $view = 'price/offers';
        if( $StockID = $this->officeChoosed() )
        {
            $this->load->model('price');
            $PriceLevelID = $this->customer->getPriceLID();
            $StockTypeFilter = 'StockAll';
            $SearchFilter = 'ByWebCategory';
            $SearchValue = $id;
            $SortData = '';
            ///pp([$StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData]);
            $offers = $this->model_price->priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData);
            ///dd($offers);
            if( get(get($offers,0,[]),'isError') )
            {
                $this->flashers[] = [
                    'type' => 'error',
                    'text' => get(get($offers,0,[]),'mess'),
                ];
                $offers = [];
            }
            else array_walk($offers, function(&$row)
            {
                $row['more'] = $this->url->link('goods/info',"hash=".arrEncryption([
                        'artbrID' => (int) get($row,'id_rbArticles'),
                        'brandID' => (int) get($row,'id_rbBrands'),
                        'brand'   => get($row,'brand'),
                        'code'    => get($row,'code'),
                        'goodsID' => (int) get($row,'GoodsID'),
                        ///
                        'offer' => [
                            'price' => nl2br(get($row,'cost_display')),
                            'qty'  => (int) get($row,'qty')*1,
                            'step' => (int) get($row,'cost_for_qty'),
                            'rbg'  => (int) get($row,'GoodsID'),
                            'tzp'  => get($row,'tzp'),
                            'std'  => (int) get($row,'id_rbStock'),
                            'pld'  => (int) get($row,'id_rbStockStoragePlace'),
                            'reward' => $this->customer->getRewardPoints(),
                        ],
                    ])
                );
            });
        }
        else
        {
            $offers = [];
        }
        //dd($offers);
        $this->data['offers'] = $offers;
        $this->data['basketTabID'] = $this->getBasketTabActive();

        $this->document->setTitle('test_title');
        $this->document->setDescription('test_description');
        $this->document->setKeywords('test_keywords');
        $this->document->addLink($goodsLink,'canonical');
        ///$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        ///$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

        $this->renderCommon();
        $this->response->setOutput($this->load->view($view,$this->data));
    }
}
