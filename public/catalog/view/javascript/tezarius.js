//"use strict";
// Class definition

console.log("%cОстановитесь!", "font-family:Whitney,Helvetica,Arial,sans-serif;font-size:50px;font-weight:bold;text-transform:uppercase;color:#ffa834;-webkit-text-stroke:2px #26b;");
console.log("\n");
console.log("%cЭта функция браузера предназначена для разработчиков. Если кто-то сказал вам скопировать и вставить что-то здесь, " +
    "чтобы увеличить вдвое ваш баланс, или взломать чужой аккаунт, это мошенники. Выполнив эти действия, вы предоставите им доступ только к своему аккакнту.",
    "font-family:Whitney,Helvetica,Arial,sans-serif;font-size:25px;");
console.log("\n");

// Common Offices Function
let FTSOffices = function(){
    let topMenuID; /// ID меню выбора офиса
    let $TopMenu;  /// Объект меню
    let CurrentID; /// Текущий выбранный офис
    /// Private Function
    let common = function(){
        topMenuID = '#top-offices-links';
        console.log('| .:: TSOffices common ::. |');
        $TopMenu = $(topMenuID);
        CurrentID = $TopMenu.data('current')*1;
    };
    let current = function(){
        return CurrentID;
    };
    let selected = function(ths){
        let $ths = $(ths);
        let $ico = $ths.children('i.fa-check');

        if( !$ico.is(":hidden") ) return ths;
        console.log('| .:: TSOffices selected ::. |',ths);

        //
        let remote = function(){
            let officeID = $ths.data('office-id');
            let _data = new FormData();
            _data.append('office',officeID);
            $.ajax({
                url: 'index.php?route=common/offices_list/selected',
                type: 'post',
                dataType: 'json',
                data: _data,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    ///$ths.button('loading');
                },
                complete: function() {
                    ///$ths.button('reset');
                },
                success: function(json) {
                    console.log('|.:: TSBasket add JSON ::.|',json);

                    if( json.redirect ){
                        location.href = json.redirect;
                        return false;
                    }
                    /// Скорее всего ниже провалиться не должны, так как всегда будет редирект - ДОЛЖЕН БЫТЬ

                    if( json.error ){
                        $ths.parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                    }
                    if( json.success ){
                        alert(json['success']);
                        $ths.parent().find('input').val(json['code']);
                    }

                    /// /// ///
                    $TopMenu.find('ul li i.fa-check').hide(); ///$ths.parents(topMenuID).find('ul li i.fa-check').hide();
                    $ico.show();
                    //
                    let address = $ths.children('span').text();
                    $TopMenu.find('.btn-link span').text(address);
                    /// /// ///

                    $ths.parent().find('.text-danger').remove();

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };

        if( current() ) Swal.fire({
            title: 'Сменить офис ?',
            text: "Вы автоматически разлогинитесь в текущем!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ) remote();
        });
        else remote();
    };
    let noSelected = function(stopWork){
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Для продолжения выберите офис!',
            footer: '<a href>Вам нужна помощь ?</a>'
        });
        if( stopWork ) return false;
    };
    /// Public Function
    return {
        init: function(){
            common();
        }
        ,current: current
        ,selected: selected
        ,noSelected: noSelected
    }
};
// Common Search Functions
let FTSSearch = function(){
    // Private Function
    let common = function(){
        $('#searchByNumber input[name=\'number\']').parent().find('button').on('click', function() {
            if( !TSOffices.current() ) TSOffices.noSelected(true);
            let value = $('header #searchByNumber input[name=\'number\']').val();
            if( !value || value.length===0 ) TSSearch.emptySearch(true);
            else{
                let url = $('base').attr('href') + 'index.php?route=price/brands';
                url += '&number=' + encodeURIComponent(value);
                location.href = url;
            }
        });
        $('#searchByNumber input[name=\'number\']').on('keydown', function(e) {
            if( !TSOffices.current() ) TSOffices.noSelected(true);
            if( e.keyCode === 13 ) $('header #searchByNumber input[name=\'number\']').parent().find('button').trigger('click');
        });
        /// https://github.com/bassjobsen/Bootstrap-3-Typeahead
        $.ajax({
            url: 'index.php?route=common/search/history',
            type: 'get',
            dataType: 'json',
            beforeSend: function() {},
            complete: function() {},
            success: function(json){
                var $input = $('#searchByNumber input');
                $input.typeahead({
                    source: json.history,
                    autoSelect: false,
                    selectOnBlur: false
                });
                $input.change(function() {
                    var current = $input.typeahead("getActive");
                    console.log('|.:: FTSSearch common change ::.|',current);
                    if( current ){
                        // Some item from your model is active!
                        if (current.name == $input.val()) {
                            // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
                            location.href = '/index.php?route=price/offers&number=' + current.code + '&brand=' + current.brand;
                        } else {
                            // This means it is only a partial match, you can either add a new item
                            // or take the active if you don't want new items
                        }
                    } else {
                        // Nothing is active so it is a new value (or maybe empty value)
                    }
                });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let emptySearch = function(stopWork){
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Введите номер!',
            footer: '<a href>Вам нужна помощь ?</a>'
        });
        if( stopWork ) return false;
    };
    ///let history = function(){};
    return {
        // Public Function
        init: function () {
            common();
        }
        ,emptySearch: emptySearch
        ///,history: history
    };
};
// Counter Plus Minus Functions
let FTSCounter = function(){
    // Private Function
    ///let $input;
    let common = function(){
        console.log('| .:: TSCounter common ::. |');
    };
    let plus = function(counterID,btn){
        console.log('| .:: TSCounter plus ::. |');
        let $input = $(counterID).find('input.counter');
        let value = $input.val() * 1;
        let step = $input.data('step') * 1;
        let qty = $input.data('qty') * 1;
        console.log('| .:: TSCounter plus ::. |',{
            counterID: counterID,
            input: $input,
            value: value,
            step: step,
            qty: qty
        });
        if( value===qty ){
            $(btn).prop('disable',true);
            $(btn).attr('disable',true);
            return;
        }
        else{
            $(btn).prop('disable',false);
            $(btn).attr('disable',false);
        }
        value += step;
        $input.val(value).trigger('change');
    };
    let minus = function(counterID,btn){
        console.log('| .:: TSCounter minus ::. |');
        let $input = $(counterID).find('input.counter');
        let value = $input.val() * 1;
        let step = $input.data('step') * 1;
        console.log('| .:: TSCounter minus ::. |',{
            counterID: counterID,
            input: $input,
            value: value,
            step: step
        });
        if( value===step ){
            $(btn).prop('disable',true);
            $(btn).attr('disable',true);
            return;
        }
        else{
            $(btn).prop('disable',false);
            $(btn).attr('disable',false);
        }
        value -= step;
        $input.val(value).trigger('change');
    };
    let get = function(counterID){
        return $(counterID).find('input.counter').val() * 1;
    };
    // Public Function
    return {
        init: function(){
            common();
        }
        ,plus: plus
        ,minus: minus
        ,get: get
    }
};
// Basket Add Remove Functions
let FTSBasket = function(){
    ///let btnClassActive = '';
    ///let btnClassDefault = '';
    // Private Function
    let common = function(){
        ///btnClassActive = 'btn-success';
        ///btnClassDefault = 'btn-default';
    };
    let add = function(counterID,btn){
        let $input = $(counterID).find('input.counter');
        let qty = $input.val() * 1;   /// Quantity
        let rbg = $input.data('rbg'); /// GoodsID
        let tzp = $input.data('tzp'); /// TZP
        let std = $input.data('std'); /// StockID
        let pld = $input.data('pld'); /// PlaceID
        console.log('| .:: TSBasket add ::. |',{
            counterID: counterID,
            input: $input,
            qty: qty,
            tzp: tzp
        });
        let _data = new FormData();
        _data.append('rbg',rbg);
        _data.append('tzp',tzp);
        _data.append('qty',qty);
        _data.append('std',std);
        _data.append('pld',pld);
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            dataType: 'json',
            data: _data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                console.log('|.:: TSBasket add JSON ::.|',json);

                if( json['flash'] ){
                    let idx = $input.data('row-root');
                    $(btn).parents('#row-root-'+idx).after('<div class="row">' + json['flash'] + '</div>');
                    ///console.log('|.:: TSBasket add JSON flash ::.|',idx,$(btn).parents('#row-root-'+idx),json['flash']);
                }
                if( json['cartID'] ){
                    $(btn).removeClass('btn-primary').addClass('btn-success').attr('type','button').attr('onclick','TSBasket.change(this,true)');
                }

                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    ///$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                ///$('html, body').animate({ scrollTop: 0 }, 'slow');

                $('#cart > ul').load('index.php?route=common/cart/info ul li');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let quantity = function(ths){
        let $ths = $(ths);
        let id = $ths.data('id');
        let qty = $ths.val();
        update(id,qty,'');
    };
    let timerNote;
    let note = function(counterID,ths){
        if( timerNote ) clearTimeout(timerNote);
        timerNote = setTimeout(function(){
            let $ths = $(ths);
            let id = $ths.data('id');
            let qty = $(counterID).find('input.counter').val();
            let note = $ths.val();
            update(id,qty,note);
        },1000);
    };
    let update = function(id,qty,note){
        console.log('| .:: QTY CHANGE ::. |');
        $.ajax({
            url: 'index.php?route=checkout/cart/update',
            type: 'post',
            data: 'id=' + id + '&qty=' + qty + '&note=' + note,
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                console.log('|.:: TSBasket update JSON ::.|',getURLVar('route'),json);
                if( json.status ) location.href = json.redirect;
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let remove = function(key,rowID){
        Swal.fire({
            title: 'Удалить позицию из корзины ?',
            text: "Вы всегда сможете узнать актуальные цены и повторно добавить позицию!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ){
                $.ajax({
                    url: 'index.php?route=checkout/cart/remove',
                    type: 'post',
                    data: 'key=' + key,
                    dataType: 'json',
                    beforeSend: function(){
                        $('#cart > button').button('loading');
                    },
                    complete: function(){
                        $('#cart > button').button('reset');
                    },
                    success: function(json){
                        console.log('|.:: TSBasket remove JSON ::.|',getURLVar('route'),json);

                        /// Решили пока н аредиректе остановится
                        ///if( json.status ) $(rowID).remove();

                        if( json['flash'] ) $('.breadcrumb').after(json['flash']);

                        if( json.message ){
                            Swal.fire({
                                type: json.message.type,
                                title: json.message.text,
                                ///text: json.message.text,
                            });
                        }
                        // Need to set timeout otherwise it wont update the total
                        setTimeout(function(){
                            ///$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                        }, 100);

                        if( getURLVar('route')==='checkout/cart' || getURLVar('route')==='checkout/checkout' ){
                            location.href = 'index.php?route=checkout/cart';
                        }
                        else $('#cart > ul').load('index.php?route=common/cart/info ul li');

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        });
    };
    let create = function(ths){
        $.ajax({
            url: 'index.php?route=checkout/cart/create',
            type: 'post',
            dataType: 'json',
            beforeSend: function() {
                $('#cart > button').button('loading');
            },
            complete: function() {
                $('#cart > button').button('reset');
            },
            success: function(json) {
                console.log('|.:: TSBasket create JSON ::.|',json);

                if( json.status ){
                    let $ths = $(ths);

                    let $root = $ths.parents('.tz-basket-tabs');
                    let $btns = $root.find('.tz-basket-tab');
                    let $last = $btns.last();
                    let $tpl = $root.find('.tz-basket-tab-tpl');

                    let classAcv = $tpl.data('class-active');
                    let classDef = $tpl.data('class-default');

                    $btns.removeClass(classAcv).addClass(classDef);

                    let $new = $tpl.clone().addClass(classAcv).data('tab-id',json['TabID']).show();
                    $last.after($new);
                    ///
                    $('#basket-active-content').html('<h1>'+json.message+'</h1>');

                    //json['TabID']
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let change = function(ths,askme){
        let $ths = $(ths);
        let $root = $ths.parents('.tz-basket-tabs');
        let $tpl = $root.find('.tz-basket-tab-tpl');
        let classAcv = $tpl.data('class-current');
        if( $ths.hasClass(classAcv) ) return false;
        ///
        let remote = function(){
            $.ajax({
                url: 'index.php?route=checkout/cart/change',
                type: 'post',
                dataType: 'json',
                data: 'key=' + $ths.data('tab-id'),
                beforeSend: function() {
                    ///console.log('|.:: TSBasket create JSON ::.|',$ths.data('tab-id'));
                    $('#cart > button').button('loading');
                },
                complete: function() {
                    $('#cart > button').button('reset');
                },
                success: function(json) {
                    console.log('|.:: TSBasket create JSON ::.|',json);

                    if( json.status ){
                        location.href = json.redirect;
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };
        if( askme ) Swal.fire({
            title: 'Перейти в корзину ?',
            text: 'При переходе вы потеряете текущий результат и выставленные фильтры',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ) remote();
        });
        else remote();

    };
    let clear = function(key){
        Swal.fire({
            title: 'Удалить текущую корзину ?',
            text: "Все позиции из корзины будут удалены!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да',
            cancelButtonText: 'Нет'
        }).then((result) => {
            if( result.value ){
            $.ajax({
                url: 'index.php?route=checkout/cart/clear',
                type: 'post',
                data: 'key=' + key,
                dataType: 'json',
                beforeSend: function(){
                    $('#cart > button').button('loading');
                },
                complete: function(){
                    $('#cart > button').button('reset');
                },
                success: function(json){
                    console.log('|.:: TSBasket clear JSON ::.|',getURLVar('route'),json);
                    if( json.status ) location.href = json.redirect;
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });
    };
    // Public Function
    return {
        init: function(){
            common();
        }
        ,add: add
        ,update: update
        ,remove: remove
        ,create: create
        ,change: change
        ,clear: clear
        ,quantity: quantity
        ,note: note
    }
};

// Created Objects
let TSOffices = new FTSOffices();
let TSSearch = new FTSSearch();
let TSCounter = new FTSCounter();
let TSBasket = new FTSBasket();

// Ready && Init Scetion
jQuery(document).ready(function(){
    TSOffices.init();
    TSSearch.init();
    TSCounter.init();
    TSBasket.init();

    ///$('.date').datetimepicker({language: '{{ datepicker }}', pickTime: false});
    ///$('.time').datetimepicker({language: '{{ datepicker }}', pickDate: false});
    ///$('.datetime').datetimepicker({language: '{{ datepicker }}', pickDate: true, pickTime: true});
});
///ready(function(){});