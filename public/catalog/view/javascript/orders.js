//"use strict";
// Class definition
// Common Search Functions
let FTSOrders = function(){
    // Private Function
    let common = function(){
        $('.period-date').datetimepicker({
            todayBtn: "linked",
            language: "ru",
            autoclose: true,
            todayHighlight: true,
            dateFormat: 'yyyy.mm.dd'
        });
        let filterRootID = '#orders-filters';
        $(filterRootID+' .dropdown-menu li a').click(function(){

            let $this = $(this);
            let code = $this.data('filter');
            $this.parents(filterRootID).find('.filter').hide();
            $('#orders__' + code + '.filter').show();

            $this.parents(filterRootID).find('.filter-name').text($this.text());
            $this.parents(filterRootID).find('.filter-name').val($this.text());
            $this.parents(filterRootID).find('.filter-code').val(code);
        });
    };
    let history = function(id,ths){
        $.ajax({
            url: 'index.php?route=account/order/history',
            type: 'post',
            data: 'id=' + id,
            dataType: 'json',
            beforeSend: function() {
                ///$(ths).button('loading');
            },
            complete: function() {
                ///$(ths).button('reset');
            },
            success: function(json) {
                console.log('|.:: TSOrders history JSON ::.|',getURLVar('route'),json);
                ///if( json.status ) location.href = json.redirect;
                if( json['flash'] ) $('.breadcrumb').after(json['flash']);
                if( json.message ){
                    Swal.fire({
                        type: json.message.type,
                        title: json.message.text,
                        ///text: json.message.text,
                    });
                }
                if( json['status'] ){
                    Swal.fire({
                        ///type: json.message.type,
                        html: json.html,
                        ///text: json.message.text,
                    });
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };
    let withdraw = function(id){
        Swal.fire({
            title: 'Отправить запросна снятие ?',
            text: "Укажите причину!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it',
            cancelButtonText: 'No, do it later',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                console.log('|.:: TSOrders withdraw preConfirm ::.|',getURLVar('route'),data);
                /// https://learn.javascript.ru/fetch
                return fetch('index.php?route=account/order/withdraw', {
                    method: 'POST',
                    ///headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({id:id,note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    Swal.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((body) => {
            ///console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                Swal.fire({
                    type: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                });
            }
        });
    };
    let delivery = function(id){
        Swal.fire({
            title: 'Запросить сроки доставки ?',
            text: "Можете оставить ваш комментарий",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                console.log('|.:: TSOrders delivery preConfirm ::.|',getURLVar('route'),data);
                /// https://learn.javascript.ru/fetch
                return fetch('index.php?route=account/order/delivery', {
                    method: 'POST',
                    ///headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({id:id,note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    Swal.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((body) => {
            ///console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                Swal.fire({
                    type: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                });
            }
        });
    };
    let towork = function(id){
        Swal.fire({
            title: 'Отправка позиции заказа в работу ?',
            text: "Можете оставить ваш комментарий",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Продолжить',
            cancelButtonText: 'Отмена',
            showLoaderOnConfirm: true,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            preConfirm: (data) => {
                console.log('|.:: TSOrders towork preConfirm ::.|',getURLVar('route'),data);
                /// https://learn.javascript.ru/fetch
                return fetch('index.php?route=account/order/towork', {
                    method: 'POST',
                    ///headers:{'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
                    body: JSON.stringify({id:id,note:data}),
                }).then(response => {
                    ///console.log('| .:: SWEET ALERT PRE ::. |',response);
                    if( !response.ok ) throw new Error(response.statusText);
                    return response.json();
                }).catch(error => {
                    Swal.showValidationMessage(`Request failed: ${error}`);
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((body) => {
            ///console.log('| .:: SWEET ALERT THN ::. |',body);
            if( body.value ){
                Swal.fire({
                    type: body.value.type,
                    title: body.value.text,
                    ///text: json.message.text,
                });
            }
        });
    };
    ///let history = function(){};
    return {
        // Public Function
        init: function () {
            common();
        }
        ,history: history
        ,withdraw: withdraw
        ,delivery: delivery
        ,towork: towork
        ///,history: history
    };
};

// Created Objects
let TSOrders = new FTSOrders();

// Ready && Init Scetion
jQuery(document).ready(function(){
    TSOrders.init();
});
///ready(function(){});