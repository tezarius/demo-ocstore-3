//"use strict";
// Class definition
// Common Search Functions
let FTSItems = function(){
    let $applyTable;
    let $applyFilterRoot;
    let applyFilterSetMake;
    let applyFilterSetModel;
    let applyFilterClassDefault = 'btn-default';
    let applyFilterClassActive = 'btn-primary';
    // Private Function
    let common = function(){
        applyFilterSetMake = '';
        applyFilterSetModel = '';
        $applyTable = $('table#applicability');
    };
    let applyFilterMake = function(ths){
        let $ths = $(ths);
        if( $ths.hasClass(applyFilterClassDefault) ){
            $ths.parents('.btn-group').find('.'+applyFilterClassActive).removeClass(applyFilterClassActive).addClass(applyFilterClassDefault);
            $ths.removeClass(applyFilterClassDefault).addClass(applyFilterClassActive);
            applyFilterSetMake = $ths.data('code');

            $ths.parents('#tab-applicability').find('.btn-group.apply-filter-models').hide();
        }
        else{
            $ths.removeClass(applyFilterClassActive).addClass(applyFilterClassDefault);
            applyFilterSetMake = '';
        }
        applyFilterSetModel = '';
        $ths.parents('#tab-applicability').find('.btn-group.apply-filter-models button').removeClass(applyFilterClassActive).addClass(applyFilterClassDefault);
        applyFilterActiveModels($ths);
        $ths.parents('#tab-applicability').find('.alert').remove();
    };
    let applyFilterModel = function(ths){
        let $ths = $(ths);
        if( $ths.hasClass(applyFilterClassDefault) ){
            $ths.parents('.btn-group').find('.'+applyFilterClassActive).removeClass(applyFilterClassActive).addClass(applyFilterClassDefault);
            $ths.removeClass(applyFilterClassDefault).addClass(applyFilterClassActive);
            applyFilterSetModel = $ths.data('code');
        }
        else{
            $ths.removeClass(applyFilterClassActive).addClass(applyFilterClassDefault);
            applyFilterSetModel = '';
        }
        applyFilterActiveTable();
    };
    let applyFilterActiveModels = function($ths){
        $ths.parents('#tab-applicability').find('.btn-group.apply-filter-models').hide();
        if( applyFilterSetMake ) $ths.parents('#tab-applicability').find('#apply-filter-models-'+applyFilterSetMake).show();
        applyFilterActiveTable();
    };
    let applyFilterActiveTable = function(){
        let $head = $applyTable.find('thead');
        if( applyFilterSetMake || applyFilterSetModel ) $head.show(); else $head.hide();

        $applyTable.find('tbody tr').hide();

        let $findByMake = $applyTable.find('tr[data-make-code="'+applyFilterSetMake+'"]');
        let $findByModel = $applyTable.find('tr[data-model-code="'+applyFilterSetModel+'"]');

        ///console.log(applyFilterSetMake,applyFilterSetModel);
        if( applyFilterSetMake && !applyFilterSetModel ) $findByMake.show();
        else if( applyFilterSetModel ) $findByModel.show();
    };
    return {
        // Public Function
        init: function () {
            common();
        }
        ,apply: {
            filter: {
                make: applyFilterMake,
                model: applyFilterModel,
            }
        }
        ///,history: history
    };
};

// Created Objects
let TSItems = new FTSItems();

// Ready && Init Scetion
jQuery(document).ready(function(){
    TSItems.init();
});
///ready(function(){});