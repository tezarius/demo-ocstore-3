<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Убрать скидки по корзине';

// Text
$_['text_success']  = 'Баллы успешно отменены!';


$_['button_cancel_discounts'] = 'Отменить все применненные бонусы';