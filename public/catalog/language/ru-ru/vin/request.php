<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'ВИН запрос';

// Text
$_['text_vin_request']  = 'ВИН-Запрос';
$_['text_agree']    = 'Я прочитал и согласен с условиями <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_vin']   = 'Тема запроса';
$_['entry_name']  = 'Имя';
$_['entry_phone'] = 'Телефон';
$_['entry_note']  = 'Подробно';
// Place Holders
$_['holder_vin']   = 'VIN / Номер кузова';
$_['holder_name']  = 'Как к вам обращаться';
$_['holder_phone'] = 'Вам перезвонят';
$_['holder_note']  = 'Список деталей и другие заметки';

// Error
$_['error_vin']   = 'Поле (VIN /Номер кузова) должно содержать от 5 до 17 символов!';
$_['error_name']  = 'Имя должно содержать от 1 до 32 символов!';
$_['error_phone'] = 'Номер телефона должен содержать от 11 цифр!';
$_['error_note']  = 'Опишите проблему более подробно';
$_['error_agree'] = 'Вы должны прочитать и согласится с %s!';