<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Неоригинальный каталог';
$_['title_aftermarket_details_offers'] = 'Результат по текущем прайсам';
$_['title_aftermarket_details_catalog'] = 'Глобальный запрос цен';

// Text
$_['text_aftermarket_types']    = 'Типы';
$_['text_aftermarket_marks']    = 'Марки';
$_['text_aftermarket_models']   = 'Модели';
$_['text_aftermarket_vehicles'] = 'Модификации';
$_['text_aftermarket_tree']     = 'Дерево узлов';
$_['text_aftermarket_details']  = 'Список запчастей';
$_['text_garage_return']        = 'Вернуться в гараж';

// Entry
$_['entry_type']    = 'Тип';

// Place Holders
$_['holder_type']    = 'Выберите тип авто';

// Error
$_['error_type']    = 'Поле должно быть равно C/P/M латинскими буквами!';
