<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title'] = 'Гараж';

// Text
$_['text_auto_catalog'] = 'Выбор по каталогу';
$_['text_auto_cart']    = 'Карточка авто/мото';
$_['text_garage']       = 'Гараж';
$_['text_garage_add']   = 'Добавить транспорт';
$_['text_garage_aftermarket'] = 'Каталоги заменители';

// Entry
$_['entry_type']    = 'Тип';
$_['entry_mark']    = 'Марка';
$_['entry_model']   = 'Модель';
$_['entry_vehicle'] = 'Модификация';
$_['entry_auto']    = 'Наименование авто/техники';
$_['entry_engine']  = 'Двигатель';
$_['entry_year']    = 'Год';
$_['entry_vin']     = 'VIN / Номер кузова';
$_['entry_note']    = 'Комментарий';
$_['entry_node']    = 'Узел';

// Place Holders
$_['holder_type']    = 'Выберите тип авто';
$_['holder_mark']    = 'Выберите мартку';
$_['holder_model']   = 'Выберите модел';
$_['holder_vehicle'] = 'Введите модификацию';
$_['holder_auto']    = 'Можно будет редактировать';
$_['holder_engine']  = 'Объем двигателя в см3';
$_['holder_year']    = 'Год выпуска';
$_['holder_vin']     = 'Если не определился, нужно ввести';
$_['holder_note']    = 'При необходимости введите';

// Error
$_['error_type']    = 'Поле должно быть равно C/P/M латинскими буквами!';
$_['error_mark']    = 'Марка должна содержать от 3 символов!';
$_['error_model']   = 'Модель должна содержать от 3 символов!';
$_['error_vehicle'] = 'Модификайия должна содержать от 3 символов!';
$_['error_auto']    = 'Наименование должно содержать от 3 символов!';
$_['error_vin']     = 'Поле (VIN /Номер кузова) должно содержать от 5 до 17 символов!';
$_['error_year']    = 'Введите корректный год';
