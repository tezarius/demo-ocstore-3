<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']         = 'История заказов';
$_['heading_history_title'] = 'История статусов';


// Text
$_['text_account']          = 'Личный кабинет';
$_['text_order']            = 'Заказ';
$_['text_order_detail']     = 'Детали заказа';
$_['text_invoice_no']       = '№ Счета:';
$_['text_account_order_id'] = '№ заказа:';
$_['text_date_added']       = 'Дата добавления:';
$_['text_shipping_address'] = 'Адрес доставки';
$_['text_shipping_method']  = 'Способ доставки:';
$_['text_payment_address']  = 'Платёжный адрес';
$_['text_payment_method']   = 'Способ оплаты:';
$_['text_comment']          = 'Комментарий к заказу';
$_['text_history']          = 'История заказа';
$_['text_success']          = 'Товары из заказа <a href="%s">%s</a> успешно добавлены <a href="%s">в вашу корзину</a>!';
$_['text_empty']            = 'Вы еще не совершали покупок!';
$_['text_error']            = 'Запрошенный заказ не найден!';

$_['filter_name_current']    = 'Текущие';
$_['filter_holder_current']  = 'Текущие заказы';
$_['filter_name_period']     = 'За период';
$_['filter_name_number']     = 'По номеру';
$_['filter_name_search']     = 'Мультипоиск';

$_['order_history']          = 'История позиции заказа';

$_['order_withdraw']         = 'Запрос на снятие';
$_['order_delivery']         = 'Запрос срока доставки';
$_['order_towork']           = 'Отправка позиции заказа в работу';

$_['order_withdraw_success'] = 'Запрос на снятие отправлен';
$_['order_delivery_success'] = 'Запрос срока доставки отправлен';
$_['order_towork_success']   = 'Позиция принята в работу';


// Column
$_['column_order_id']       = '№ Заказа';
$_['column_product']        = 'Количество Товаров';
$_['column_customer']       = 'Покупатель';
$_['column_name']           = 'Название товара';
$_['column_model']          = 'Код товара';
$_['column_quantity']       = 'Количество';
$_['column_price']          = 'Цена';
$_['column_total']          = 'Всего';
$_['column_action']         = 'Действие';
$_['column_date_added']     = 'Дата добавления';
$_['column_status']         = 'Статус';
$_['column_comment']        = 'Комментарий';

$_['column_parts']          = 'Деталь';
$_['column_qty']            = 'Кол-во';
$_['column_history_date']   = 'Дата';
$_['column_history_status'] = 'Статус';
$_['column_history_note']   = 'Заметка';
$_['column_last_note']      = 'Заметка';


// Error
$_['error_reorder']         = '%s в настоящее время не доступны для заказана.';