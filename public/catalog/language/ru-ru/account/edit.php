<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']      = 'Учетная запись';

// Text
$_['text_account']       = 'Личный кабинет';
$_['text_edit']          = 'Редактировать информацию';
$_['text_your_details']  = 'Контактные данные';
$_['text_firm_details']  = 'Карточка организации <small>*(правка доступа только один раз)</small>';
$_['text_success']       = 'Ваша учетная запись была успешно обновлена!';

// Entry
$_['entry_firstname']    = 'Имя';
$_['entry_lastname']     = 'Фамилия';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Телефон';
$_['entry_fax']          = 'Факс';

$_['entry_patronymic'] = 'Отчество';
$_['entry_org_name']   = 'Организация';
$_['entry_org_inn']    = 'ИНН';
$_['entry_org_kpp']    = 'КРР';
$_['entry_org_ogrn']   = 'ОГРН';
$_['entry_org_addr']   = 'Адрес';

// Error
$_['error_exists']       = 'Такой E-Mail уже зарегистрирован!';
$_['error_firstname']    = 'Имя должно содержать от 1 до 32 символов!';
$_['error_lastname']     = 'Фамилия должна содержать от 1 до 32 символов!';
$_['error_email']        = 'E-Mail адрес введен неверно!';
$_['error_telephone']    = 'Номер телефона должен содержать от 3 до 32 символов!';
$_['error_custom_field'] = '%s необходим!';

$_['error_patronymic'] = 'Отчество должно содержать от 1 до 32 символов!';
$_['error_org_name']   = 'Некорректное название организации'; /// Не меньше 4 символов
$_['error_org_inn']    = 'Некорректный ИНН'; /// Не меньше 10 символов
$_['error_org_addr']   = 'Некорректный адрес'; /// Не менее 5 символов