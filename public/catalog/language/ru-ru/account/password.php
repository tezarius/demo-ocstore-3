<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Heading
$_['heading_title']  = 'Правка логина с паролем';

// Text
$_['text_account']   = 'Личный кабинет';
$_['text_password']  = 'Ваш пароль';
$_['legend_login']   = 'Ваш логин';
$_['text_success']   = 'Данные успешно изменены!';
$_['text_without_changes'] = 'Данные остались прежние!';

// Entry
$_['entry_login']    = 'Логин';
$_['entry_password'] = 'Пароль';
$_['entry_confirm']  = 'Подтвердите пароль';
$_['entry_oldpass']  = 'Старый пароль';

// Error
$_['error_login']    = 'Логин должен содержать от 1 до 32 символов!';
$_['error_password'] = 'Пароль должен содержать от 8 до 20 символов!';
$_['error_oldpass']  = 'Неверный старый пароль';
$_['error_confirm']  = 'Пароли не совпадают!';