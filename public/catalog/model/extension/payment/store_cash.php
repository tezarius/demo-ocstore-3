<?php

class ModelExtensionPaymentStoreCash extends ModelSettingTezarius
{
	public function getMethod($address,$total)
    {
		$this->load->language('extension/payment/store_cash');
		$method_data = [];

        ///$status = ($total <= 0.00);
        $status = TRUE;

		if( $status )
		{
			$method_data = [
				'code'       => 'store_cash',
				'title'      => $this->language->get('text_title'),
				'terms'      => '',
				'sort_order' => $this->config->get('payment_store_cash_sort_order')
			];
		}

		return $method_data;
	}
}