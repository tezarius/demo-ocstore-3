<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ModelGoodsInfo extends ModelSettingTezarius
{
    public function getAll($artbrID,$brandID,$brand,$code,$GoodsID)
    {
        $images = [];
        $images_global = $this->imagesGlobal($brandID,$brand,$code);
        $images_local = $this->imagesLocal($artbrID);
        foreach( $images_global AS $img )
        {
            array_push($images,[
                'name' => get($img,'DataSupplierArticleNumber').' / '.get($img,'description'),
                'path' => get($img,'url'),
                'note' => get($img,'Description'),
            ]);
        }
        foreach( $images_local AS $img )
        {
            array_push($images,[
                'name' => get($img,'id_rbArticles').' / '.get($img,'rbBrands_id_rbBrands'),
                'path' => get($img,'name'),
                'note' => get($img,'note'),
            ]);
        }
        $image = array_shift($images);

        $settingsLocal = $this->settingsLocal($GoodsID);
        $settingsLocal['table'] = $this->settingsLocalTable($GoodsID);

        $ans = [
            'images_global' => $images_global,
            'images_local'  => $images_local,
            'images'        => $images,
            'image'         => $image,
            'name'          => "$brand / $code",

            'settings_global' => $this->settingsGlobal($brandID,$brand,$code),
            'settings_local'  => $settingsLocal,

            'apply_global' => $this->applyGlobal($brandID,$brand,$code),
            'apply_local'  => $this->applyLocal(),

            'brand_info' => $this->brandInfo($brandID),
        ];
        return $ans;
    }

    /**
     * Получить фото детали из глобального автосправочника
     * https://lk.tezarius.ru/ru/public-api/show/36
     * @param $brandID
     * @param $brand
     * @param $code
     * @return array
     */
    public function imagesGlobal($brandID,$brand,$code)
    {
        $query = $this->dbt->query("call pCars('getArticlePhoto',JSON_OBJECT('BrandID',{$brandID},'BrandName','{$brand}','code','{$code}'),@Code,@Error)");
        $this->dbt->clear();
        return $query->rows;
    }
    /**
     * Получить список путей фото товара из вашей базы данных
     * https://lk.tezarius.ru/ru/public-api/show/34
     * @param $artbrID
     * @return array
     */
    public function imagesLocal($artbrID)
    {
        $query = $this->dbt->query("CALL pRB_get('rbPhoto',JSON_OBJECT('filter','PhotoByArtId','id',{$artbrID}),'ru',1,0,1,'')");
        $this->dbt->clear();
        return $query->rows;
    }

    /**
     * Получить информацию о детали из глобального автосправочника, размеры, свойства и тд
     * https://lk.tezarius.ru/ru/public-api/show/35
     * @param $brandID
     * @param $brand
     * @param $code
     * @return array
     */
    public function settingsGlobal($brandID,$brand,$code)
    {
        $query = $this->dbt->query("call pCars('getArticleInfo',JSON_OBJECT('brandID',{$brandID},'brand','{$brand}','code','{$code}'),@Code,@Error)");
        $this->dbt->clear();
        return $query->rows;
    }
    /**
     * Получить карточку товара вашего наличия, свойства, примечание и тд
     * https://lk.tezarius.ru/ru/public-api/show/38
     * @param $GoodsID
     * @return array
     */
    public function settingsLocal($GoodsID)
    {
        $query = $this->dbt->query("CALL pRB_get('rbGoods',JSON_OBJECT('filter','id','id',{$GoodsID}),'ru',1,0,1,'')");
        $this->dbt->clear();
        return $query->row;
    }
    public function settingsLocalTable($GoodsID)
    {
        $query = $this->dbt->query("CALL pRB_get('rbGoodsProperties',JSON_OBJECT('filter','ByGoodsId','id',{$GoodsID}),'ru',1,0,1,'')");
        $this->dbt->clear();
        return $query->rows;
    }

    /**
     * Получить инфо о применяемости запчасти к авто из глобального автосправочника
     * https://lk.tezarius.ru/ru/public-api/show/37
     * @param $brandID
     * @param $brand
     * @param $code
     * @return array
     */
    public function applyGlobal($brandID,$brand,$code)
    {
        $query = $this->dbt->query("call pCars('getArticleApplicability',JSON_OBJECT('brandID',{$brandID},'brand','{$brand}','code','{$code}'),@Code,@Error)");
        $this->dbt->clear();
        return $query->rows;
    }
    public function applyLocal(){ return NULL; }

    /**
     * Информация о бренде
     * https://lk.tezarius.ru/ru/public-api/show/39
     * @param $brandID
     * @return array
     */
    public function brandInfo($brandID)
    {
        $query = $this->dbt->query("CALL pANY('BrandInfo',JSON_OBJECT('ID',{$brandID}),'ru')");
        $this->dbt->clear();
        return $query->row;
    }
}