<?php

class ModelAccountCustomer extends ModelSettingTezarius
{
    /// Адаптированны
    public function verifyExistence($login,$phone,$email)
    {
        $sql = "call pANY_www('CustomerExistChecking',JSON_OBJECT('phone','{$phone}','login','{$login}','email','{$email}'),'ru')";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }
    public function addCustomer($data)
    {
        $StockID = (int) $data['StockID'];
        $name_f = $this->dbt->escape($data['firstname']);
        $name_i = $this->dbt->escape($data['lastname']);
        $login  = $this->dbt->escape($data['email']);
        $pass   = $this->dbt->escape($data['password']);
        $phone  = $this->dbt->escape($data['telephone']);

        $jsRegData = "'name_f','{$name_f}','name_i','{$name_i}','email','{$login}','phone','{$phone}','id_rbStock','{$StockID}','login','{$login}','pass','{$pass}'";
        $sql = "CALL `pRB_change`('ins','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,$jsRegData),'ru',1)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();

        ///$query->row['sql'] = $sql;
        ///$query->row['data'] = $data;
        return $query->row;
	}
	public function editPassword($CounterpartsID,$login,$password)
    {
        $sql = "call `pRB_change`('upd','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,'isAuthorizationDataUpd',1,'login','$login','pass','{$password}','CntptsId',{$CounterpartsID}),'ru',1)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }
	public function editCustomer($CustomerUID, $StockID, $data)
    {
        $name_i = $this->dbt->escape(get($data,'firstname'));
        $name_f = $this->dbt->escape(get($data,'lastname'));
        $name_o = $this->dbt->escape(get($data,'patronymic'));
        $email  = $this->dbt->escape(get($data,'email'));
        $phone  = $this->dbt->escape(get($data,'telephone'));

        $org  = $this->dbt->escape(get($data,'org_name'));
        $inn  = $this->dbt->escape(get($data,'org_inn'));
        $kpp  = $this->dbt->escape(get($data,'org_kpp'));
        $ogrn = $this->dbt->escape(get($data,'org_ogrn'));
        $addr = $this->dbt->escape(get($data,'org_addr'));


        $sql = "call `pRB_change`('upd','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,
            /* Контактные данные */
            'name_f','{$name_f}', 'name_i','{$name_i}','name_o','{$name_o}', 'email','{$email}','phone','{$phone}','id_rbStock',{$StockID},
            /* Карточка организации, правка доступа только один раз */
            'name_full','{$org}','inn','{$inn}','kpp','{$kpp}','ogrn','{$ogrn}','address','{$addr}',
            /* Код клиента */
            'CntptsId',{$CustomerUID}
            ),'ru',1)";
        ///dd($sql);
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }
	public function historySearch($UserID)
    {
        $sql = "call pSearch_ArtCode('MyHistory',JSON_OBJECT('isWebsiteQuery',1,'id_rbCounterparts',{$UserID}))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->rows;
    }
    public function getNewsletter()
    {
        $UserID = $this->customer->getId();
        $sql = "CALL pRB_get('rbOrderStates',JSON_OBJECT('isWebsiteQuery',1,'id_rbCounterparts',{$UserID}),'ru',0,1,0,'')";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->rows;

    }
    public function editNewsletter($newsletter)
    {
        $UserID = $this->customer->getId();
        $ArrayStates = implode(',',$newsletter);
        $sql = "CALL pANY_www('SetOrderStatesInform',JSON_OBJECT('CouterpartsID',{$UserID},'ArrayStates','{$ArrayStates}'),'ru')";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }



	/// Родные
	public function editAddressId($customer_id, $address_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}
	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}
	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}
	public function getCustomerByCode($code) {
		$query = $this->db->query("SELECT customer_id, firstname, lastname, email FROM `" . DB_PREFIX . "customer` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}
	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

		return $query->row;
	}
	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}
	public function addTransaction($customer_id, $description, $amount = '', $order_id = 0) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_transaction SET customer_id = '" . (int)$customer_id . "', order_id = '" . (float)$order_id . "', description = '" . $this->db->escape($description) . "', amount = '" . (float)$amount . "', date_added = NOW()");
	}
	public function deleteTransactionByOrderId($order_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");
	}
	public function getTransactionTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(amount) AS total FROM " . DB_PREFIX . "customer_transaction WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}
	public function getTotalTransactionsByOrderId($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_transaction WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}
	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}
	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}
	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}
	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}
	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
	public function addAffiliate($customer_id, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "customer_affiliate SET `customer_id` = '" . (int)$customer_id . "', `company` = '" . $this->db->escape($data['company']) . "', `website` = '" . $this->db->escape($data['website']) . "', `tracking` = '" . $this->db->escape(token(64)) . "', `commission` = '" . (float)$this->config->get('config_affiliate_commission') . "', `tax` = '" . $this->db->escape($data['tax']) . "', `payment` = '" . $this->db->escape($data['payment']) . "', `cheque` = '" . $this->db->escape($data['cheque']) . "', `paypal` = '" . $this->db->escape($data['paypal']) . "', `bank_name` = '" . $this->db->escape($data['bank_name']) . "', `bank_branch_number` = '" . $this->db->escape($data['bank_branch_number']) . "', `bank_swift_code` = '" . $this->db->escape($data['bank_swift_code']) . "', `bank_account_name` = '" . $this->db->escape($data['bank_account_name']) . "', `bank_account_number` = '" . $this->db->escape($data['bank_account_number']) . "', `status` = '" . (int)!$this->config->get('config_affiliate_approval') . "'");
		
		if ($this->config->get('config_affiliate_approval')) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "customer_approval` SET customer_id = '" . (int)$customer_id . "', type = 'affiliate', date_added = NOW()");
		}		
	}
	public function editAffiliate($customer_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer_affiliate SET `company` = '" . $this->db->escape($data['company']) . "', `website` = '" . $this->db->escape($data['website']) . "', `commission` = '" . (float)$this->config->get('config_affiliate_commission') . "', `tax` = '" . $this->db->escape($data['tax']) . "', `payment` = '" . $this->db->escape($data['payment']) . "', `cheque` = '" . $this->db->escape($data['cheque']) . "', `paypal` = '" . $this->db->escape($data['paypal']) . "', `bank_name` = '" . $this->db->escape($data['bank_name']) . "', `bank_branch_number` = '" . $this->db->escape($data['bank_branch_number']) . "', `bank_swift_code` = '" . $this->db->escape($data['bank_swift_code']) . "', `bank_account_name` = '" . $this->db->escape($data['bank_account_name']) . "', `bank_account_number` = '" . $this->db->escape($data['bank_account_number']) . "' WHERE `customer_id` = '" . (int)$customer_id . "'");
	}
	public function getAffiliate($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_affiliate` WHERE `customer_id` = '" . (int)$customer_id . "'");

		return $query->row;
	}
	public function getAffiliateByTracking($tracking) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_affiliate` WHERE `tracking` = '" . $this->db->escape($tracking) . "'");

		return $query->row;
	}			
}