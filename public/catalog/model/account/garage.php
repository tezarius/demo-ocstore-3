<?php

class ModelAccountGarage extends ModelSettingTezarius
{
    public function vehicles()
    {
        $UserID = $this->customer->getId();
        $sql = "call `pRB_get`('rbCarsCustomer',JSON_OBJECT('filter','ByCounterpartsID','id',{$UserID}),'ru',1,0,1,'')";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->rows;
    }
	public function add($auto,$engine,$vin,$year,$note,$carID,$type)
    {
        $UserID = $this->customer->getId();
        $sql = "call `pRB_change`('ins','rbCarsCustomer',0,JSON_OBJECT("
            ."'isWebsiteQuery',1,'id_rbCounterparts',{$UserID},'name','{$auto}','engine','{$engine}','vin','{$vin}',"
            ."'year','{$year}','note','{$note}','id_out',{$carID},'CarTypeOut','{$type}'),'ru',1)"
        ;
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
    public function del()
    {
        $sql = "";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
}