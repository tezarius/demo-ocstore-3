<?php

class ModelAccountVin extends ModelSettingTezarius
{
	public function checkUserExist($ShopID,$name,$phone)
    {
        $sql = "CALL pRB_change('ins','rbCustomers',0,JSON_OBJECT('isWebsiteQuery',1,'isRequestCustomer',1,'name_f','{$name}','phone','{$phone}','id_rbStock',{$ShopID}),'ru',1)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }
	public function request($UserID,$CarID,$vin,$note,$ShopID,$ManagerID,$SourceID)
    {
        $sql = "CALL pANY('RequestAdd',JSON_OBJECT('id_rbCounterparts',{$UserID},'carId_out',{$CarID},'subject','$vin','body','{$note}',
        'id_rbStock',{$ShopID},'id_rbUsers_manager',{$ManagerID},'id_rbCustomersSource',{$SourceID}),'ru')";
        ///dd($sql);
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }
}