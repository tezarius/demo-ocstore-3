<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ModelPrice extends ModelSettingTezarius
{
    /** STEP 1 */
    public function getBrands($number)
    {
        $query = $this->dbt->query("CALL pBrands_by_code('$number')");
        $this->dbt->clear();
        return $query->rows;
    }
    /** STEP 1.2 */
    public function getBrandsKey($code)
    {
        $StockID = (int) $this->customer->getStockID();
        $CounterpartID = (int) $this->customer->getId();

        $sql = "call pSearch_ArtCode('brandNew',JSON_OBJECT('code','{$code}','id_rbStock',{$StockID},'id_rbCounterparts',{$CounterpartID}))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
    /** STEP 1.3 */
    public function getBrandsRemote($LogSearchID)
    {
        $ans = [];

        $host = DBT_HOSTNAME;
        $project = TEZ_PROJECT_ID;
        $target_url = "https://{$host}/services/in/suppliers/search.php?BrandsRequest=1&IDProjects={$project}&IDSearchCode={$LogSearchID}";
        $ans['uri'] = $target_url;

        $ch = curl_init($target_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($ch);
        curl_close($ch);

        $ans['res'] = $result;
        return $ans;
    }



    /** STEP 2.1 */
    public function getSearchID($code,$brand)
    {
        $StockID = (int) $this->customer->getStockID();
        $CounterpartID = (int) $this->customer->getId();
        $ip = ip();

        $sql = "call pSearch_ArtCode('new',JSON_OBJECT('isWebsiteQuery',1,'ip','$ip','code','$code','brand','$brand','source','website','id_rbStock',$StockID,'id_rbCounterparts',$CounterpartID))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }
    /** STEP 2.2 */
    public function webServicesLoad($LogSearchID)
    {
        $ans = [];

        $host = DBT_HOSTNAME;
        $project = TEZ_PROJECT_ID;
        $target_url = "https://{$host}/services/in/suppliers/search.php?IDProjects={$project}&IDSearchCode={$LogSearchID}";
        $ans['uri'] = $target_url;

        $ch = curl_init($target_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $result = curl_exec($ch);
        curl_close($ch);

        $ans['res'] = $result;
        return $ans;
    }
    /** STEP 2.3 */
    public function priceLoad($StockID,$PriceLevelID,$StockTypeFilter,$SearchFilter,$SearchValue,$SortData)
    {
        $StockID = (int) $StockID;
        $sql = "call pPrice(JSON_OBJECT(
            'isWebSearch',1,
            'StockDestinationID',$StockID,
            'PriceLevelID',$PriceLevelID,
            'StockTypeFilter','$StockTypeFilter',
            'SearchFilter','$SearchFilter',
            'SearchFilterValue','$SearchValue',
            'SortData','$SortData',
            'limit',1000,
            'limitPos',1
        ),'ru')";
        ///dd($sql);
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->rows['sql'] = $sql;
        return $query->rows;
    }


    public function searchByAuto($car,$code,$vehicle)
    {
        $UserUD = $this->customer->getId();
        $ShopID = $this->customer->getStockID();
        $ip = ip();
        $sql = "call pSearch_ArtCode('new',JSON_OBJECT('isWebsiteQuery',1,'ip','{$ip}','code','{$code}','brandID',{$car},'brand','{$vehicle}',"
            ."'TypeData','ByCarNode','source','website','id_rbStock',{$ShopID},'id_rbCounterparts',{$UserUD}))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        ///$query->row['sql'] = $sql;
        return $query->row;
    }
}