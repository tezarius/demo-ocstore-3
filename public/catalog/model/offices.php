<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ModelOffices extends ModelSettingTezarius
{
    /** Get Offices List for Header Menu */
    public function getList()
    {
        $query = $this->dbt->query("CALL `pRB_get`('rbStock',JSON_OBJECT('filter','login'),'ru',1,0,1,'')");
        $this->dbt->clear();
        return $query->rows;
    }
}