<?php

class ModelCheckoutCart extends ModelSettingTezarius
{
    public function getRewardPoints()
    {
        /// Пока метод тут:
        /// public/system/library/cart/customer.php
        /// Он там был изначально
    }
    /// Логичнее поместить сюда: public/catalog/model/extension/total/reward.php
    public function applyRewardPoints($CardID,$ShopID,$TabID,$UserID,$PriceLID)
    {
        $sql = "call pCart('DiscountSet',JSON_OBJECT('DiscontType','bonus','DiscontValue','','DiscontCardID',{$CardID},'isWebCart',1,'ShopID',{$ShopID},
        'CounterPartsID',{$UserID},'tabID',{$TabID},'PriceLevelID',{$PriceLID}))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
    /// Логичнее поместить сюда: public/catalog/model/extension/total/coupon.php
    public function applyCoupon($PromoName,$CardID,$ShopID,$TabID,$UserID,$PriceLID)
    {
        $sql = "call pCart('DiscountSet',JSON_OBJECT('DiscontType','promo','DiscontValue','{$PromoName}','DiscontCardID',{$CardID},'isWebCart',1,'ShopID',{$ShopID},
        'CounterPartsID',{$UserID},'tabID',{$TabID},'PriceLevelID',{$PriceLID}))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
    public function removeBasketDiscounts($ShopID,$TabID,$UserID,$PriceLID)
    {
        $sql = "call pCart('DiscountSet',JSON_OBJECT('DiscontType','skip','isWebCart',1,'ShopID',{$ShopID},'CounterPartsID',{$UserID},'tabID',{$TabID},'PriceLevelID',{$PriceLID}))";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
}