<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ModelSettingTezarius extends Model
{
    public function __construct($registry){
        parent::__construct($registry);
        $this->dbt->query("SET NAMES utf8");
        $this->dbt->query("SET CHARACTER SET utf8");
    }
}