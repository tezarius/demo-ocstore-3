<?php

class ModelCatalogAftermarket extends ModelSettingTezarius
{
    public function types()
    {
        return [[
            'code' => 'P',
            'name' => 'Легковые',
            'image' => '/catalog/view/image/catalog/aftermarket/type/P.svg',
            'href' => '/index.php?route=catalog/aftermarket/marks&type=P',
        ],[
            'code' => 'C',
            'name' => 'Грузовые',
            'image' => '/catalog/view/image/catalog/aftermarket/type/C.svg',
            'href' => '/index.php?route=catalog/aftermarket/marks&type=C',
        ],[
            'code' => 'M',
            'name' => 'Мототехника',
            'image' => '/catalog/view/image/catalog/aftermarket/type/M.svg',
            'href' => '/index.php?route=catalog/aftermarket/marks&type=M',
        ]];
    }
    public function marks($type)
    {
        $sql = "CALL pCars('getMakes',JSON_OBJECT('Type','{$type}'),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->rows;
    }
    public function mark($type,$mark)
    {
        $sql = "CALL pCars('getCarsInfo',JSON_OBJECT('Step','marka','Type','$type','id',{$mark}),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
    public function models($type,$mark)
    {
        $sql = "CALL pCars('getModels',JSON_OBJECT('Type','{$type}','ManuID',{$mark}),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->rows;
    }
    public function model($type,$model)
    {
        $sql = "CALL pCars('getCarsInfo',JSON_OBJECT('Step','model','Type','$type','id',$model),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }
    public function vehicles($type,$mark,$model)
    {
        $sql = "CALL pCars('getCars',JSON_OBJECT('Type','{$type}','ManuID',{$mark},'ModelID',{$model}),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->rows;
    }
    public function vehicle($type,$car)
    {
        $sql = "CALL pCars('getCarsInfo',JSON_OBJECT('Step','car','Type','$type','id',$car),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->row;
    }

    public function tree($type,$car)
    {
        $sql = "CALL pCars('getTree',JSON_OBJECT('Type','{$type}','CarID',{$car}),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->rows;
    }
    public function details($type,$car,$node)
    {
        $sql = "call pCars('getArticlesByNode',JSON_OBJECT('Type','{$type}','CarID',{$car},'NodeID',{$node}),@Code,@Error)";
        $query = $this->dbt->query($sql);
        $this->dbt->clear();
        return $query->rows;
    }
}