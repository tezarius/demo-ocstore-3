<?php
// Heading
$_['heading_title'] 				 = '[xds] Карусель товаров';

// Text
$_['text_extension'] 				 = 'Расширения';
$_['text_success']   				 = 'Настройки успешно изменены!';
$_['text_edit']     			   = 'Настройки модуля';

// Entry
$_['entry_name']      			 = 'Название модуля';
$_['entry_title']     			 = 'Заголовок модуля';
$_['entry_module_type']			 = 'Тип модуля';

$_['entry_latest']					 = 'Новинки';
$_['entry_special']					 = 'Акции';
$_['entry_bestseller']			 = 'Хиты продаж';
$_['entry_viewed']					 = 'Вы смотрели';
$_['entry_featured']				 = 'Пользовательский набор';
$_['entry_pagination']			 = 'Отображать пагинацию';
$_['entry_resolution']			 = 'Разрешение';
$_['entry_r_count_items']		 = 'Кол-во товаров';
$_['entry_product_items']		 = 'Отображать товаров';
$_['entry_hide_products']		 = 'Исключить товары, которых нет в наличии';
$_['entry_shufle']					 = 'Премешивать товары';
$_['entry_autuplay']		 		 = 'Автопрокрутка';
$_['entry_autuplay_speed']	 = 'Скорость автопрокрутки';
$_['entry_add_buttons']			 = 'Дополнительеные кнопки у товаров';

$_['entry_wishlist']			 	 = 'В закладки';
$_['entry_compare']			   	 = 'В сравнение';
$_['entry_fastorder']			 	 = 'Быстрый заказ';
$_['entry_qwiev']			 		 	 = 'Быстрый просмотр';


$_['entry_product']   			 = 'Пользовательский набор товаров';
$_['entry_limit']    				 = 'Лимит отображаемых товаров';
$_['entry_width']    				 = 'Ширина изображения';
$_['entry_height']   				 = 'Высота изображения';
$_['entry_status']   				 = 'Статус модуля';

$_['text_enabled']    			 = 'Вкл.';
$_['text_disabled']    			 = 'Выкл.';
$_['text_yes']    					 = 'Да';
$_['text_no']    						 = 'Нет';

// Help
$_['help_product']   				 = 'Эти товары будут отображаться, если выбран тип модуля <Пользовательский набор>';

// Error
$_['error_permission'] 			 = 'У Вас нет прав для управления данным модулем!';
$_['error_name']     				 = 'Название модуля должно содержать от 3 до 64 символов!';
$_['error_width']    				 = 'Введите ширину изображения!';
$_['error_height']   				 = 'Введите высоту изображения!';

