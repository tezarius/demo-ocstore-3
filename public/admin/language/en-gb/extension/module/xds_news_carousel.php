<?php
// Heading
$_['heading_title'] 				 = '[xds] News Carousel';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified featured module!';
$_['text_edit']        = 'Edit Featured Module';

// Entry
$_['entry_name']      			 = 'Module name';
$_['entry_title']     			 = 'Module title';
$_['entry_module_type']			 = 'Module type';

$_['entry_latest']					 = 'Latest news';
$_['entry_featured']				 = 'Custom set';
$_['entry_pagination']			 = 'Display pagination';
$_['entry_resolution']			 = 'Resolution';
$_['entry_r_count_items']		 = 'Number of records';
$_['entry_product_items']		 = 'Display entries';


$_['entry_autuplay']		 		 = 'Autoplay';
$_['entry_autuplay_speed']	 = 'Autoplay speed';


$_['entry_article']   			 = 'Custom News Set';
$_['entry_limit']    				 = 'Display Limit';
$_['entry_width']    				 = 'Image width';
$_['entry_height']   				 = 'Image height';
$_['entry_status']   				 = 'Status';

$_['text_enabled']    			 = 'On';
$_['text_disabled']    			 = 'Off';
$_['text_yes']    					 = 'Yes';
$_['text_no']    						 = 'No';

// Help
$_['help_article']   				 = 'This news will be displayed if the module type <Custom set> is selected.';
$_['ocstore_alert']   			 = 'This module uses the assembly functionality <a href="" target="_blank">ocStore</a> and is available only for it!';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify featured module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';
