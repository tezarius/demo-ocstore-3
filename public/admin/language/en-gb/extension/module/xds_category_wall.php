<?php
// Heading
$_['heading_title'] 				 = '[xds] Category list';

// Text
$_['text_extension'] 				 = 'Extensions';
$_['text_success']   				 = 'Settings changed successfully!';
$_['text_edit']     			   = 'Module settings';

// Entry
$_['entry_name']      			 = 'Name';
$_['entry_title']     			 = 'title';
$_['entry_status']   				 = 'Status';

$_['entry_category']   			 = 'Categories';
$_['entry_limit']   			   = 'Limit of displayed subcategories';
$_['entry_width']   			   = 'Height of the image';
$_['entry_height']   			   = 'Width of the image';


$_['text_enabled']    			 = 'On';
$_['text_disabled']    			 = 'Off';
$_['text_yes']    					 = 'Yes';
$_['text_no']    						 = 'No';


// help
$_['help_category']    			 = 'Start typing a category name';
$_['help_limit']    			   = 'Set to 0 if you do not want to display subcategories';

// Error
$_['error_permission'] 			 = 'You do not have rights to manage this module!';
$_['error_name']     				 = 'The name of the module must contain from 3 to 64 characters!';
$_['error_width']    				 = 'Enter the width of the image!';
$_['error_height']   				 = 'Enter the height of the image!';
