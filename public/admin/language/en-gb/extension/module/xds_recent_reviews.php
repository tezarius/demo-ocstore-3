<?php
// Heading
$_['heading_title']    = '[xds] Recent Reviews';

// Text
$_['text_extension']   = 'Modules';
$_['text_success']     = 'Success: You have modified "Recent Reviews" module!';
$_['text_edit']        = 'Edit';

// Entry
$_['entry_name']       	 = 'Module Name';
$_['entry_status']     	 = 'Status';
$_['entry_title']     	 = 'Module Title';
$_['entry_limit']        = 'Number of reviews displayed';
$_['entry_sort']         = 'Sort';
$_['entry_sort_date']    = 'Date';
$_['entry_sort_random']  = 'Random';
$_['entry_rating']       = 'Show only reviews with ratings higher than';
$_['entry_image_size']   = 'Product image size';

$_['entry_pagination']			 = 'Show paginatin';
$_['entry_resolution']			 = 'Resolution';
$_['entry_r_count_items']		 = 'Count reviews';
$_['entry_product_items']		 = 'Reviews';

$_['entry_shufle']					 = 'Shufle reviews';
$_['entry_autuplay']		 		 = 'Autoplay';
$_['entry_autuplay_speed']	 = 'Autoplay speed';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify "Recent Reviews" module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
