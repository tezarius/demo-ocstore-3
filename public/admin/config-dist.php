<?php
// HTTP
define('HTTP_SERVER', 'http://site.name/admin/');
define('HTTP_CATALOG', 'http://site.name/');

// HTTPS
define('HTTPS_SERVER', 'http://site.name/admin/');
define('HTTPS_CATALOG', 'http://site.name/');

// DIR
define('DIR_APPLICATION', '/path/to/site/public/admin/');
define('DIR_SYSTEM', '/path/to/site/public/system/');
define('DIR_IMAGE', '/path/to/site/public/image/');
define('DIR_STORAGE', '/path/to/site/storage/');
define('DIR_CATALOG', '/path/to/site/public/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', '127.0.0.1');
define('DB_USERNAME', 'user');
define('DB_PASSWORD', 'pass');
define('DB_DATABASE', 'base');
define('DB_PORT', '3306');
define('DB_PREFIX', '');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
define('OPENCARTFORUM_SERVER', 'https://opencartforum.com/');
