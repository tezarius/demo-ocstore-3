<?php

class ControllerExtensionPaymentStoreCash extends Controller
{
	private $error = array();

	public function index()
    {
		$this->load->language('extension/payment/store_cash');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if( ($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() )
		{
			$this->model_setting_setting->editSetting('payment_store_cash', $this->request->post);
			///print'<pre>';print_r($res);print'</pre>';exit;

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = [[
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        ],[
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
        ],[
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/store_cash', 'user_token=' . $this->session->data['user_token'], true)
        ]];

		$data['action'] = $this->url->link('extension/payment/store_cash', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

		if (isset($this->request->post['store_cash_order_status_id'])) {
			$data['store_cash_order_status_id'] = $this->request->post['store_cash_order_status_id'];
		} else {
			$data['store_cash_order_status_id'] = $this->config->get('store_cash_order_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['payment_store_cash_status'])) {
			$data['payment_store_cash_status'] = $this->request->post['payment_store_cash_status'];
		} else {
			$data['payment_store_cash_status'] = $this->config->get('payment_store_cash_status');
		}

		if (isset($this->request->post['payment_store_cash_sort_order'])) {
			$data['payment_store_cash_sort_order'] = $this->request->post['payment_store_cash_sort_order'];
		} else {
			$data['payment_store_cash_sort_order'] = $this->config->get('payment_store_cash_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/store_cash', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/store_cash')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}