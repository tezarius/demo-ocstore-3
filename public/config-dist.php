<?php
// HTTP
define('HTTP_SERVER', 'http://site.name/');

// HTTPS
define('HTTPS_SERVER', 'http://site.name/');

// DIR
define('DIR_APPLICATION', '/path/to/site/public/catalog/');
define('DIR_SYSTEM', '/path/to/site/public/system/');
define('DIR_IMAGE', '/path/to/site/public/image/');
define('DIR_STORAGE', '/path/to/site/storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB LOCAL
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', '127.0.0.1');
define('DB_USERNAME', 'user');
define('DB_PASSWORD', 'pass');
define('DB_DATABASE', 'base');
define('DB_PORT', '3306');
define('DB_PREFIX', '');

// DB TEZARIUS
define('DBT_DRIVER', 'mysqli');
define('DBT_HOSTNAME', 'XX.tezarius.ru');
define('DBT_USERNAME', 'user');
define('DBT_PASSWORD', 'pass');
define('DBT_DATABASE', 'base');
define('DBT_PORT', '3306');
define('DBT_PREFIX', '');
define('TEZ_PROJECT_ID', 0000);